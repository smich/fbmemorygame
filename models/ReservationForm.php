<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ReservationForm extends CFormModel
{
    public $route;
    public $customRoute;
    public $departureDate;
    public $departureTime;
    public $name;
    public $email;
    public $tel;
    public $passengers;
    public $kids;
    public $flightInfo;
    public $comments;
    public $verifyCode;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('route, departureDate, departureTime, name, email, tel, passengers', 'required'),
            array('comments, customRoute, flightInfo', 'safe'),
            // departureDate has to be a valid date
            array('departureDate', 'date', 'format'=>'dd/MM/yyyy'),
            // Custom route is required only when the user requests it!
            array('customRoute', 'isCustomRouteRequired'),
            // passengers have to be an int with max value 20
            array('passengers', 'numerical', 'integerOnly'=>true, 'max'=>20, 'min'=>4, 'message'=>'Provide a valid number of {attribute} (4 to 20)'),
            array('kids', 'numerical', 'integerOnly'=>true, 'max'=>19, 'message'=>'Provide a valid number of {attribute} (0 to 19)'),
            array('kids', 'isLessThanPassengers'),
            // email has to be a valid email address
            array('email', 'email'),
            // Telephone number validation
            array('tel', 'match', 'pattern'=>'/^([+]?[0-9 ]+)$/', 'message'=>'Invalid telephone number. You should write in the following example format: +30 213 029 1852'),
            // verifyCode needs to be entered correctly
            array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
        );
    }

    public function isLessThanPassengers()
    {
        return ($this->kids>0 && ($this->kids < $this->passengers));
    }
    
    public function isCustomRouteRequired()
    {
        return ($this->route==1 && empty($this->customRoute));
    }
    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'verifyCode'=>'Verification Code',
            'tel'=>'Telephone number',
            'passengers'=>'Total number of passengers',
            'kids'=>'Children up to 4 years old',
            'flightInfo'=>'Flight Info'
        );
    }
   
    /**
     * Check if this reservation is for a night route
     */
    public function isNightRoute()
    {
        $departureDt = explode(':',$this->departureTime);
        $hour = (int)$departureDt[0];
        return (($hour>=1 && $hour<=7) || $hour==24)?true:false;
    }
 
    /**
     * Prices
     * @return type
     */
    public static function optsPrices()
    {
        return array(
            '1'  =>'',
            'R1' =>'16',
            'R2' =>'18',
            'R3' =>'16',
            'R4' =>'18');
    }
    
    /**
     * Bus routes 
     * @return type
     */
    public static function optsRoutes()
    {
        return array(
            '0'  => 'Select route',
            '1'  => 'I want to pick my route',
            'R1' =>'Athens airport El. Venizelos to Athens City Center',
            'R2' =>'Athens airport El. Venizelos to Port of Pireaus, Rafina, Lavrio, Coastal Area (Including Palaio faliro, Vouliagmeni and Glyfada)',
            'R3' =>'Athens City Center to Athens airport El. Venizelos',
            'R4' =>'Coastal Area and Ports to Athens airport El. Venizelos'
        );
    }
    
    /**
     * Timetable
     * @return type
     */
    public static function optsTimetable()
    {
        $timetable = array(
            'R1' => array(
                '02:40','10:20','11:45','13:00','15:00','17:50','18:30','20:40','22:00','23:40'
            ),
            'R3' => array(
                '12:10','14:00','16:30','19:50','21:20','23:00'
            )
        );
        $timetable['R2'] = $timetable['R1'];
        $timetable['R4'] = $timetable['R3'];
        
        return $timetable;
    }

    /**
     * Bus departure hours 
     * @return type
     */
    public static function optsDepartureHours()
    {
        return array(
            '01'=>'01',
            '02'=>'02',
            '03'=>'03',
            '04'=>'04',
            '05'=>'05',
            '06'=>'06',
            '07'=>'07',
            '08'=>'08',
            '09'=>'09',
            '10'=>'10',
            '11'=>'11',
            '12'=>'12',
            '13'=>'13',
            '14'=>'14',
            '15'=>'15',
            '16'=>'16',
            '17'=>'17',
            '18'=>'18',
            '29'=>'29',
            '20'=>'20',
            '21'=>'21',
            '22'=>'22',
            '23'=>'23',
            '24'=>'24'
        );
    }

    /**
     * Bus departure minutes
     * @return type
     */
    public static function optsDepartureMinutes()
    {
        return array(
            '00'=>'00',
            '05'=>'05',
            '10'=>'10',
            '15'=>'15',
            '20'=>'20',
            '25'=>'25',
            '30'=>'30',
            '35'=>'35',
            '40'=>'40',
            '45'=>'45',
            '50'=>'50',
            '55'=>'55',
            '60'=>'60'
        );
    }
}
