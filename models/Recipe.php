<?php

/**
 * This is the model class for table "recipe".
 *
 * The followings are the available columns in table 'recipe':
 * @property integer $id
 * @property integer $mem_id
 * @property string $title
 * @property string $ingredients
 * @property string $description
 * @property string $status
 * @property create_dt $status
 * @property update_dt $status
 * @property rating_a int (0-5)
 * @property rating_b int (0-5)
 */
class Recipe extends CActiveRecord {
    /**
     * User message status
     */

    const STATUS_PENDING = 'pending';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_REJECTED = 'rejected';
    const THANKYOU_MESSAGE = 'Ευχαριστούμε που μοιράστηκες μαζί μας την "έξυπνη" συνταγή σου!';

    public $ingredientItems = array();
    public $discloseName;

    /**
     * Returns the static model of the specified AR class.
     * @return Recipe the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'recipe';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('mem_id', 'required'),
            array('id, mem_id, rating_a,', 'numerical', 'integerOnly' => true),
            array('rating_a,', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 5),
            array('description', 'length', 'max' => 1024),
            array('status', 'length', 'max' => 8),
            array('title,ingredients,description,status,rating_a,rating_b', 'safe'),
            array('title,ingredients,description,status', 'required', 'on' => 'update'),
            // The following rule is used by search().
            array('id, mem_id, title, ingredients, description, status, rating_a, rating_b', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'member' => array(self::BELONGS_TO, 'Member', 'mem_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'mem_id' => 'ID Χρήστη',
            'title' => 'Όνομα συνταγής',
            'ingredients' => 'Συστατικά συνταγής',
            'description' => 'Εκτέλεση συνταγής',
            'status' => 'Κατάσταση',
            'create_dt' => 'Ημ/νία δημιουργίας',
            'update_dt' => 'Ημ/νία ενημέρωσης',
            'rating_a' => 'Βαθμολογία κ. Δρίσκα',
            'rating_b' => 'Βαθμολογία κ. Δημοσθενόπουλου',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('mem_id', $this->mem_id);
        $criteria->compare('ingredients', $this->ingredients, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('create_dt', $this->create_dt, true);
        $criteria->compare('update_dt', $this->update_dt, true);
        $criteria->compare('rating_a', $this->rating_a, true);
        $criteria->compare('rating_b', $this->rating_b, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'id DESC')
        ));
    }

    // ---------------------------
    // PROTECTED FUNCTIONS
    // ---------------------------

    protected function afterFind() {
        // DeSerialize ingredients
        $this->ingredientItems = CJSON::decode($this->ingredients);
        $this->ingredients = implode('  ,  ', $this->ingredientItems);
    }

    protected function beforeSave() {
        // Serialize ingredients
        $this->ingredients = CJSON::encode($this->ingredientItems);

        // If the user has no participation code associated to them, create one now
        if (empty($this->member->code)) {
            $this->member->code = $this->member->generateParticipationCode();
        }

        $this->member->disclose_name = $this->discloseName;

        if($this->isNewRecord) {
            $this->create_dt = date('Y-m-d H:i:s');
        }
        else {
            $this->update_dt = date('Y-m-d H:i:s');
            if(empty($this->create_dt)) {
                $this->create_dt = date('Y-m-d H:i:s');
            }
        }

        $this->member->save();
        return parent::beforeSave();
    }

    // ---------------------------
    // HELPER FUNCTIONS
    // ---------------------------

    public function getStatusLabel()
    {
        switch($this->status)
        {
            case "pending":
                $status="Υπό έγκριση";
            break;
            case "accepted":
                $status="Εγκρίθηκε";
            break;
            case "rejected":
                $status="Απορρίφθηκε";
            break;
        }

        return $status;
    }

    public static function optsRating() {
        return array(
            0 => 0,
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
        );
    }

    public static function optsStatus() {
        return array(
            self::STATUS_PENDING => Yii::t("models-PhotoApp", "Υπο έγκριση"),
            self::STATUS_ACCEPTED => Yii::t("models-PhotoApp", "Εγκρίθηκε"),
            self::STATUS_REJECTED => Yii::t("models-PhotoApp", "Απορρίφθηκε"),
        );
    }

    public static function loadMessages($limit=30) {
        // Query params
        $qParams = array('order'=>'RAND( )', 'limit'=>$limit);

        // Get total number of messages
        $msgCount = self::model()->with('member')->countByAttributes(array('status'=>self::STATUS_ACCEPTED));

        // We need a random offset only if the total count is greater than the specified limit
        if($msgCount>$limit) {
            $validOffsetTop = $msgCount - $limit;
            $offset = rand(0,$validOffsetTop);
            $qParams['offset'] = $offset;
        }
        return self::model()->with('member')->findAllByAttributes(array('status'=>self::STATUS_ACCEPTED), $qParams);
    }

}