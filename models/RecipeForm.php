<?php

/**
 * RecipeForm class.
 * RecipeForm is the data structure for keeping
 * photo app form data.
 */
class RecipeForm extends CFormModel
{
    public $title;
    public $ingredients;
    public $description;
    public $discloseName;
    public $iagree;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('mem_id', 'userAlreadySubmitted'),
            array('title', 'required', 'message'=>'Ποιό είναι το όνομα στη συνταγή;'),
            array('ingredients', 'required', 'message'=>'Ποιά είναι τα συστατικά της συνταγής;'),
            array('description', 'required', 'message'=>'Πώς εκτελείται η συνταγή;'),
            array('iagree', 'required', 'requiredValue' => true, 'message' => 'Αποδεχτείτε του όρους χρήσης για να συνεχίσετε'),
        );
    }
    /**
     * Check if the user has already submit a recipe
     * @param  String $attributes The attribute name on which the validation rule is applied
     * @param  Array $params      Params that can be passed in the validation method
     * @return void
     */
    public function userAlreadySubmitted($attribute,$params)
    {
        // Check if the user has left already a message
        $memId = 1;
        $recipe = Recipe::model()->findByAttributes(array('mem_id'=>$memId));
        if($recipe) {
            $this->addError($attribute,
                Yii::t('models-RecipeForm',
                    "It looks like you have already submitted a recipe! <br/> This is your participation code: <b>{code}</b>",
                    array('{code}' => $recipe->member->code)
            ));
        }
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'title' => Yii::t('models-RecipeForm','Όνομα συνταγής'),
            'ingredients' => Yii::t('models-RecipeForm','Συστατικά συνταγής'),
            'description' => Yii::t('models-RecipeForm','Εκτέλεση συνταγής'),
            'discloseName' => Yii::t('models-RecipeForm','Εμφάνιση του ονόματός μου'),
        );
    }

    protected function beforeSave() {
        if($model->isNewRecord)
            $this->discloseName = !$this->discloseName;

        return parent::beforeSave();
    }
    
}