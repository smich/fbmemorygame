<?php

/**
 * This is the model class for table "member".
 *
 * The followings are the available columns in table 'member':
 * @property integer $id
 * @property string $fname
 * @property string $lname
 * @property string $email
 * @property string $active
 * @property string $disclose_name
 * @property string $external_id
 * @property string $code
 * @property string $permissions
 * @property string $photo
 * @property string $photo_url
 * @property string $create_dt
 * @property string $update_dt
 */
class Member extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Member the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('id', 'numerical', 'integerOnly' => true),
            array('fname, lname', 'length', 'max' => 45),
            array('email', 'length', 'max' => 125),
            array('email', 'email'),
            array('active', 'boolean'),
            array('disclose_name', 'boolean'),
            array('external_id, code', 'length', 'max' => 32),
            array('permissions', 'length', 'max' => 500),
            array('photo', 'length', 'max' => 250),
            array('photo_url', 'length', 'max' => 250),
            array('create_dt, update_dt, active, disclose_name', 'safe'),
            array('fname, lname, email', 'required', 'on' => 'update'),
            // The following rule is used by search().
            array('id, fname, lname, email, external_id, code, permissions, photo_url, create_dt, update_dt, disclose_name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'memoryResults' => array(self::HAS_MANY, 'MemoryApp', 'mem_id'),
            'hasMemoryResults' => array(self::STAT, 'MemoryApp', 'mem_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'disclose_name' => 'Εμφάνιση ονόματος',
            'active' => 'Ενεργός',
            'fname' => 'Όνομα',
            'lname' => 'Επώνυμο',
            'email' => 'Email',
            'external_id' => 'Σύνδεση μέσω Facebook',
            'code' => 'Κωδικός',
            'permissions' => 'Permissions',
            'photo_url' => 'Φωτογραφία',
            'create_dt' => 'Ημ/νία δημιουργίας',
            'update_dt' => 'Ημ/νία ενημέρωσης',
        );
    }

    /**
     * Generate an event participation code that is unique for each user
     *
     * @return String The participation code
     */
    public function generateParticipationCode() {
        $salt = 'aCo0lEv3nt';
        return substr(md5($this->id . $salt . date('Y-m-d H:i:s')), 1, 12);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('fname', $this->fname, true);
        $criteria->compare('lname', $this->lname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('external_id', $this->external_id, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('permissions', $this->permissions, true);
        $criteria->compare('photo_url', $this->photo_url, true);
        $criteria->compare('create_dt', $this->create_dt, true);
        $criteria->compare('update_dt', $this->update_dt, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'id DESC')
        ));
    }

    /**
     * Query a record by external provider user id
     *
     * @param string $id external user id at provider site
     * @return mixed the found MemberIntegration record or null if none was found
     */
    public function findByExternalId($id) {
        return $this->find('external_id=:id', array(
                    ':id' => $id,
                ));
    }

    /**
     * Query a record by external provider user id
     *
     * @param string $id external user id at provider site
     * @return mixed the found MemberIntegration record or null if none was found
     */
    public function findByEmail($email) {
        return $this->find('email=:email', array(
                    ':email' => $email,
                ));
    }

    /**
     * Wether a record for given external user data exists
     *
     * @param string $id external user id at provider site
     * @return bool wether a record exists
     */
    public function externalExists($id) {
        return $this->exists('external_id=:id', array(
                    ':id' => $id,
                ));
    }

    protected function beforeSave() {
        if (is_object($this->photo) && get_class($this->photo) == 'CUploadedFile') {
            // Remove from filename anything that is not a-z or 0-9
            $filename = preg_replace("/[^a-zA-Z0-9.-]/", "", $this->photo->name);
            $filename = date('ymd-His') . '-' . $filename;
            $this->photo->saveAs(param('imagePath') . $filename, false);

            YiiBase::import('application.lib.wideimage.WideImage');
            $size = 200;
            $finalFilename = "{$size}x{$size}-" . $filename;
            WideImage::load(param('imagePath') . $filename)
                ->resize($size, $size, 'outside')
                ->crop('center', 'center', $size, $size)
                ->saveToFile(param('imagePath') . $finalFilename);

            $this->photo = $filename;
            $this->photo_url = Yii::app()->createAbsoluteUrl(param('imageUrl') . $finalFilename);
        }

        if($this->isNewRecord) {
            $this->disclose_name = !$this->disclose_name;
            $this->create_dt = date('Y-m-d H:i:s');
        }
        else {
            $this->update_dt = date('Y-m-d H:i:s');
            if(empty($this->create_dt)) {
                $this->create_dt = date('Y-m-d H:i:s');
            }
        }

        return parent::beforeSave();
    }

}