<?php

class MemberForm extends CFormModel {

    public $fname;
    public $lname;
    public $email;
    public $photo;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('fname', 'required', 'message' => 'Εισάγετε το όνομά σας'),
            array('lname', 'required', 'message' => 'Εισάγετε το επώνυμό σας'),
            array('email', 'required', 'message' => 'Εισάγετε το email σας'),
            array('email', 'length', 'max' => 125),
            array('email', 'email'),
            array('photo', 'file', 'types' => 'jpg, gif, png'),
//            array('photo', 'required', 'message' => 'Ανεβάστε μια φωτογραφία σας'),
//            array('email', 'userExists'),
        );
    }

    /**
     * Check if the user already exists
     * @param  String $attributes The attribute name on which the validation rule is applied
     * @param  Array $params      Params that can be passed in the validation method
     * @return void
     */
    public function userExists() {
        return Member::model()->findByAttributes(array('email' => $this->email));
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'fname' => 'Όνομα',
            'lname' => 'Επώνυμο',
            'photo' => 'Φωτογραφία',
        );
    }

}