<?php

/**
 * PhotoAppForm class.
 * PhotoAppForm is the data structure for keeping
 * photo app form data.
 */
class PhotoAppForm extends CFormModel
{
    public $message;
    public $discloseName;
    public $iagree;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('mem_id', 'userAlreadySubmitted'),
            array('message', 'required', 'message'=>'Εισάγετε το μήνυμά σας'),
            array('iagree', 'required', 'requiredValue' => true, 'message' => 'Αποδεχτείτε του όρους χρήσης για να συνεχίσετε'),
        );
    }
    /**
     * Check if the user has already submit a message
     * @param  String $attributes The attribute name on which the validation rule is applied
     * @param  Array $params      Params that can be passed in the validation method
     * @return void
     */
    public function userAlreadySubmitted($attribute,$params)
    {
        // Check if the user has left already a message
        $memId = 1;
        $photoAppMsg = PhotoApp::model()->findByAttributes(array('mem_id'=>$memId));
        if($photoAppMsg) {
            $this->addError($attribute,
                Yii::t('models-PhotoAppForm',
                    "It looks like you have already submitted a message! <br/> This is your participation code: <b>{code}</b>",
                    array('{code}' => $photoAppMsg->member->code)
            ));
        }
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'message'=>'User message',
            'discloseName' => 'Εμφάνιση του ονόματός μου'
        );
    }

    protected function beforeSave() {
        if($model->isNewRecord)
            $this->discloseName = !$this->discloseName;

        return parent::beforeSave();
    }
    
}