<?php

/**
 * This is the model class for table "photo_app".
 *
 * The followings are the available columns in table 'photo_app':
 * @property integer $id
 * @property integer $mem_id
 * @property string $message
 * @property string $status
 */
class PhotoApp extends CActiveRecord {
    /**
     * User message status
     */

    const STATUS_PENDING = 'pending';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_REJECTED = 'rejected';
    const THANKYOU_MESSAGE = 'Ευχαριστούμε που μοιράστηκες μαζί μας το θετικό σου μήνυμα!';

    public $discloseName;

    /**
     * Returns the static model of the specified AR class.
     * @return PhotoApp the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'photo_app';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('mem_id', 'required'),
            array('id, mem_id', 'numerical', 'integerOnly' => true),
            array('status', 'length', 'max' => 8),
            array('mem_id, message', 'safe'),
                // array('id, mem_id, message, active, status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'member' => array(self::BELONGS_TO, 'Member', 'mem_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'mem_id' => 'ID Χρήστη',
            'message' => 'Μήνυμα',
            'status' => 'Κατάσταση',
            'create_dt' => 'Ημ/νία δημιουργίας',
            'update_dt' => 'Ημ/νία ενημέρωσης',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('mem_id', $this->mem_id);
        $criteria->compare('message', $this->message, true);
        $criteria->compare('status', $this->status, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'id DESC')
        ));
    }

    // ---------------------------
    // PROTECTED FUNCTIONS
    // ---------------------------

    protected function beforeSave() {
        // If the user has no participation code associated to them, create one now
        if (empty($this->member->code)) {
            $this->member->code = $this->member->generateParticipationCode();
        }

        $this->member->disclose_name = $this->discloseName;

        if($this->isNewRecord) {
            $this->create_dt = date('Y-m-d H:i:s');
        }
        else {
            $this->update_dt = date('Y-m-d H:i:s');
            if(empty($this->create_dt)) {
                $this->create_dt = date('Y-m-d H:i:s');
            }
        }

        $this->member->save();
        return parent::beforeSave();
    }

    // ---------------------------
    // HELPER FUNCTIONS
    // ---------------------------

    public function getStatusLabel()
    {
        switch($this->status)
        {
            case "pending":
                $status="Υπό έγκριση";
            break;
            case "accepted":
                $status="Εγκρίθηκε";
            break;
            case "rejected":
                $status="Απορρίφθηκε";
            break;
        }

        return $status;
    }

    public static function optsStatus() {
        return array(
            self::STATUS_PENDING => Yii::t("models-PhotoApp", "Υπο έγκριση"),
            self::STATUS_ACCEPTED => Yii::t("models-PhotoApp", "Εγκρίθηκε"),
            self::STATUS_REJECTED => Yii::t("models-PhotoApp", "Απορρίφθηκε"),
        );
    }

    public static function loadMessages($limit=30)
    {
        // Query params
        $qParams = array('order'=>'RAND( )', 'limit'=>$limit);

        // Get total number of messages
        $msgCount = self::model()->with('member')->countByAttributes(array('status'=>self::STATUS_ACCEPTED));

        // We need a random offset only if the total count is greater than the specified limit
        if($msgCount>$limit) {
            $validOffsetTop = $msgCount - $limit;
            $offset = rand(0,$validOffsetTop);
            $qParams['offset'] = $offset;
        }
        return self::model()->with('member')->findAllByAttributes(array('status'=>self::STATUS_ACCEPTED), $qParams);
    }

}