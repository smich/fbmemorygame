-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 15, 2012 at 10:33 AM
-- Server version: 5.5.27
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `podilato_hsnf`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `email` varchar(125) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `disclose_name` tinyint(1) DEFAULT '1',
  `external_id` varchar(32) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `permissions` varchar(500) DEFAULT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `photo_url` varchar(250) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=137 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `fname`, `lname`, `email`, `active`, `disclose_name`, `external_id`, `code`, `permissions`, `photo`, `photo_url`, `create_dt`, `update_dt`) VALUES
(17, 'Λεωνίδας', 'Tritis-QA', 'tritis-qa_jgfahit_tritis-qa@tfbnw.net', 1, 1, '100004545395264', 'ec798a76d6ab', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions"}', NULL, 'https://graph.facebook.com/100004545395264/picture', NULL, NULL),
(34, 'Διονύσης', 'QA', 'dummy342@webwonder.gr', 1, 1, '759869562', '3045f4d3772', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions"}', NULL, 'https://graph.facebook.com/759869562/picture', NULL, NULL),
(41, 'Μάριος', 'QA', 'dummy412@webwonder.gr', 1, 1, '1774732408', '3045f4d3772', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions"}', NULL, 'https://graph.facebook.com/1774732408/picture', NULL, NULL),
(48, 'Κώστας', 'QA', 'dummy248@webwonder.gr', 1, 1, '1074621122', '3045f4d3772', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions"}', NULL, 'https://graph.facebook.com/1074621122/picture', NULL, NULL),
(105, 'Dimitris', 'Rizoglou', 'drizoglou@aol.com', 1, 1, '1052894269', '044776ec2fea', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/1052894269/picture', NULL, NULL),
(111, 'Mina', 'Gioni', 'mina.gioni@gmail.com', 1, NULL, '1105844144', '6e0cc51a89ed', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/1105844144/picture', NULL, NULL),
(112, 'Diana', 'barbatsarou', 'diana.barbatsarou@havasww.com', 1, 1, NULL, '34daab270534', NULL, '121114-153228-diana2.jpg', 'http://www.podilatodromia.gr/uploads/200x200-121114-153228-diana2.jpg', NULL, NULL),
(114, 'Ίρμα', 'White', 'p5@webwonder.gr', 1, 1, '100002916757950', '2b117d75b00b', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/100002916757950/picture', NULL, NULL),
(115, 'Γιάννης', 'Πράσινος', 'dasd123@asd.gr', 1, 1, NULL, '70124b189f0b', NULL, '121114-161955-pattern154.gif', 'http://podilatodromia.gr/uploads/200x200-121114-161955-pattern154.gif', NULL, NULL),
(116, 'Giorgos', 'Papas', 'fotozoom@gmail.com', 1, 1, '100002242386088', '759d9e973300', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/100002242386088/picture', NULL, NULL),
(117, 'Christina', 'Sarris', 'christina.sarris@eurorscg.gr', 1, 1, '100000798488980', NULL, '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/100000798488980/picture', NULL, NULL),
(118, 'Giannis', 'Antoniou', 'johnant77@yahoo.gr', 1, 1, '767104291', 'd332edbf0044', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/767104291/picture', NULL, NULL),
(119, 'Katerina', 'Charterou', 'tinachart@yahoo.gr', 1, 1, '706792677', 'dbcfdb9ccd2a', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/706792677/picture', NULL, NULL),
(120, 'Tereza', 'Palavidi', 'tereza.palavidi@havasww.com', 1, 1, NULL, '3a1cbd8e0bf6', NULL, '121114-174345-3345281015028299207369351412869281212772003384850o.jpg', 'http://podilatodromia.gr/uploads/200x200-121114-174345-3345281015028299207369351412869281212772003384850o.jpg', NULL, NULL),
(121, 'Nikos', 'Nikolakis', 'p6@webwonder.gr', 1, 1, '100003015304596', '9dac28ed2553', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions"}', NULL, 'https://graph.facebook.com/100003015304596/picture', NULL, NULL),
(122, 'Tereza', 'Palavidi', 'teresa_moi9@hotmail.com', 1, NULL, '514128692', '7f9e1bafb48f', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/514128692/picture', NULL, NULL),
(123, 'Rinaki', 'Mpalantsinaki', 'eirinimir@yahoo.gr', 1, 1, '1209369664', 'f8a4ef24e335', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/1209369664/picture', NULL, NULL),
(124, 'Aimilia', 'Katsogianni', 'aimilia_kat@hotmail.com', 1, 1, '698452181', '3adee7528ef3', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/698452181/picture', NULL, NULL),
(125, 'christina', 'sarris', 'christina.sarris@havasww.com', 1, 1, NULL, '6637afc489a4', NULL, '121114-185652-CS2.JPG', 'http://podilatodromia.gr/uploads/200x200-121114-185652-CS2.JPG', NULL, NULL),
(126, 'Tetartis-QA', 'Tetartis-QA', 'tetartis-qa_nnrjuax_tetartis-qa@tfbnw.net', 1, 1, '100004517825948', NULL, '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/100004517825948/picture', NULL, NULL),
(127, 'stergios-qa', 'stergios-qa', 'stergios-qa_vtbijks_stergios-qa@tfbnw.net', 1, 1, '100004492320218', NULL, '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/100004492320218/picture', NULL, NULL),
(128, 'Tonia', 'Manthou', 'manthou@terraetaqua.gr', 1, NULL, '1136352043', 'f869d16146db', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/1136352043/picture', NULL, NULL),
(129, 'Manos', 'Palavidis', 'manos.palavidis@eurorscg.gr', 1, 1, '1215210880', 'e7b651bd678e', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/1215210880/picture', NULL, NULL),
(130, 'george', 'babakas', 'small-bak@hotmail.com', 1, 0, NULL, NULL, NULL, '121114-203137-moto08056.jpg', 'http://www.podilatodromia.gr/uploads/200x200-121114-203137-moto08056.jpg', NULL, NULL),
(131, 'Elena', 'Sidiroglou', 'esidirog@gmail.com', 1, 0, '672947621', NULL, '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/672947621/picture', NULL, NULL),
(132, 'Michael', 'Kokkinos', 'kokkinosmichael@gmail.com', 1, 0, '630592964', NULL, '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/630592964/picture', NULL, NULL),
(133, 'Χρύσα', 'Στάθη', 'gpstathi@otenet.gr', 1, 1, '100002589410518', 'bf4c452a7d0f', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","7":"bookmarked"}', NULL, 'https://graph.facebook.com/100002589410518/picture', NULL, NULL),
(134, 'Ελένη', 'Δημητρίου', 'e+test@lne.gr', 1, 1, NULL, 'abc13461d235', NULL, '121115-014218-1572031017949142655643080n.jpg', 'http://podilatodromia.gr/uploads/200x200-121115-014218-1572031017949142655643080n.jpg', NULL, NULL),
(135, 'Maria', 'Stavridou', 'msta168@yahoo.gr', 1, 1, '1111472411', 'f2814ce6b701', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/1111472411/picture', NULL, NULL),
(136, 'Giorgos', 'Tzoumis', 'koraki_g@yahoo.com', 1, NULL, '786270627', '0941b8e7f9a8', '{"0":"email","1":"user_work_history","2":"user_education_history","3":"user_location","4":"user_about_me","5":"installed","6":"status_update","7":"photo_upload","8":"video_upload","10":"create_note","11":"share_item","12":"publish_stream","13":"publish_actions","14":"bookmarked"}', NULL, 'https://graph.facebook.com/786270627/picture', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `photo_app`
--

DROP TABLE IF EXISTS `photo_app`;
CREATE TABLE IF NOT EXISTS `photo_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_id` int(11) DEFAULT NULL,
  `message` text,
  `status` enum('pending','accepted','rejected') DEFAULT 'accepted',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mem_id` (`mem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `photo_app`
--

INSERT INTO `photo_app` (`id`, `mem_id`, `message`, `status`) VALUES
(22, 17, 'Vestibulum sagittis nisi non neque gravida rutrum. Donec enim nunc, accumsan at congue eget, vestibulum ut lorem.', 'rejected'),
(39, 34, 'Nunc pharetra laoreet sapien, sit amet consectetur nunc rhoncus ut. Pellentesque mauris sem, fringilla vehicula lacinia id, posuere ut odio.', 'rejected'),
(46, 41, 'Nunc pharetra laoreet sapien, sit amet consectetur nunc rhoncus ut. Pellentesque mauris sem, fringilla vehicula lacinia id, posuere ut odio.', 'rejected'),
(53, 48, 'Nunc pharetra laoreet sapien, sit amet consectetur nunc rhoncus ut. Pellentesque mauris sem, fringilla vehicula lacinia id, posuere ut odio.', 'rejected'),
(70, 105, 'zcdfdsfsdfsdfdsf', 'rejected'),
(73, 111, 'Θα είμαστε εκεί!', 'accepted'),
(74, 112, 'Καλή μας επιτυχία!', 'accepted'),
(76, 114, 'Η καλή διατροφή σώζει ζωές!', 'accepted'),
(77, 115, 'Αισιοδοξία, η πιο μεγάλη δύναμη', 'accepted'),
(78, 118, 'καλή επιτυχία!!', 'accepted'),
(79, 116, 'Θα τρέξουμε μαζί σας!', 'accepted'),
(80, 119, 'Ο καθένας χαράζει το δικό του κύκλο ζωής, με ή χωρίς διαβήτη ', 'accepted'),
(81, 120, 'Με πεταλιές και μαγειρέματα να μοιραστεί το μήνυμα σε ολόκληρη την Ελλάδα!!!  ', 'accepted'),
(82, 121, 'Καλή τύχη με το εγχείρημά σας', 'accepted'),
(83, 123, 'Καλή δύναμη σε όλους!!!!!', 'accepted'),
(84, 124, 'Καλή επιτυχία !!!!!!', 'accepted'),
(85, 125, 'Στηρίζουμε την προσπάθεια, στηρίζουμε τον άνθρωπο!', 'accepted'),
(86, 128, '"Γλυκοπεταλιές" και "γλυκοσυνταγιές" για μια "γλυκιά" ζωή ...', 'accepted'),
(87, 129, 'Νικητής η ζωή!', 'accepted'),
(88, 133, 'Γλυκιά ορθοπεταλιά για την "Ενημέρωση και Πρόληψη" του Διαβήτη. Θα είμαστε εκεί!\r\nΚαλή επιτυχία.', 'accepted'),
(89, 134, 'Καλή επιτυχία!', 'accepted'),
(91, 135, 'και ομως ο διαβητης δεν ειναι ανικητος!!!!!!!!!!!!', 'accepted'),
(92, 136, 'Συγχαρητήρια, ωραία πρωτοβουλία!!', 'accepted');

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
CREATE TABLE IF NOT EXISTS `recipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `ingredients` text,
  `description` text,
  `status` enum('pending','accepted','rejected') DEFAULT 'pending',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mem_id` (`mem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`id`, `mem_id`, `title`, `ingredients`, `description`, `status`) VALUES
(20, 17, 'Titlos', '["Vestibulum ","sagittis","Lolemasdasd","asdasd"]', 'Nisi non neque gravida rutrum. Donec enim nun', 'rejected'),
(37, 34, 'Titlos', '["Vivamus","Fusce","Proin","vehicula","Nulla facilisi","porta lobortis","iaculis"]', 'Vivamus pellentesque facilisis ipsum vitae fe', 'rejected'),
(44, 41, 'Titlos', '["Vivamus","Fusce","Proin","vehicula","Nulla facilisi","porta lobortis","iaculis"]', 'Vivamus pellentesque facilisis ipsum vitae fe', 'rejected'),
(51, 48, 'Titlos', '["Morbi diam purus, sagittis et commodo ac, tincidunt quis lacus. Curabitur sollicitudin turpis vel risus euismod euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ligula sapien, tempus et dictum vel, porttitor at justo. Fusce eu feugiat magna.","Morbi diam purus","Donec ligula "]', 'Exc Morbi diam purus, sagittis et commodo ac, tin', 'rejected'),
(71, 122, 'Πολύσπορο ψωμί με αποξηρωμένα φρούτα ', '["300 \\u03b3\\u03c1. \\u03b1\\u03bb\\u03b5\\u03cd\\u03c1\\u03b9 \\u03bf\\u03bb\\u03b9\\u03ba\\u03ae\\u03c2","120 ml \\u03bd\\u03b5\\u03c1\\u03cc","15 \\u03b3\\u03c1. \\u03bc\\u03b1\\u03b3\\u03b9\\u03ac . ","1 \\u03ba.\\u03b3\\u03bb \\u03b1\\u03bb\\u03ac\\u03c4\\u03b9 ","25 \\u03b3\\u03c1. \\u03b4\\u03b1\\u03bc\\u03ac\\u03c3\\u03ba\\u03b7\\u03bd\\u03b1 \\u03c8\\u03b9\\u03bb\\u03bf\\u03ba\\u03bf\\u03bc\\u03bc\\u03ad\\u03bd\\u03b1","25 \\u03b3\\u03c1. \\u03c3\\u03cd\\u03ba\\u03b1 \\u03c8\\u03b9\\u03bb\\u03bf\\u03ba\\u03bf\\u03bc\\u03bc\\u03ad\\u03bd\\u03b1"]', '1) Διαλύουμε τη μαγιά με το νερό.<br>\r\n2) Σε ένα βαθύ μπολ προσθέτουμε το αλεύρι, το αλάτι και τα φρούτα. Συμπληρώνουμε το νερό με τη μαγιά και δημιουργούμε μία ζύμη.<br>\r\n3) Σκεπάζουμε και αφήνουμε τη ζύμη να ξεκουραστεί. \r\n<br>4) Σε αλευρωμένη επιφάνεια πλάθουμε τη ζύμη και τη χωρίζουμε σε ζυμαράκια. Δίνουμε σχήμα μικρής φραντζόλας και τοποθετούμε σε ταψί. <br>\r\n5) Ψήνουμε σε προθερμασμένο φούρνο 160ºC για 45’.', 'accepted'),
(72, 128, 'Πάστα φλώρα', '["\\u03bc\\u03b1\\u03c1\\u03bc\\u03b5\\u03bb\\u03ac\\u03b4\\u03b1  \\u03c7\\u03c9\\u03c1\\u03af\\u03c2 \\u03b6\\u03ac\\u03c7\\u03b1\\u03c1\\u03b7","2 \\u03ba\\u03bf\\u03cd\\u03c0\\u03b5\\u03c2 \\u03b1\\u03bb\\u03b5\\u03cd\\u03c1\\u03b9 \\u03c0\\u03bf\\u03c5 \\u03c6\\u03bf\\u03c5\\u03c3\\u03ba\\u03ce\\u03bd\\u03b5\\u03b9 \\u03bc\\u03cc\\u03bd\\u03bf \\u03c4\\u03bf\\u03c5","\\u03b1\\u03c3\\u03c0\\u03c1\\u03ac\\u03b4\\u03b9 \\u03b5\\u03bd\\u03cc\\u03c2 \\u03b1\\u03c5\\u03b3\\u03bf\\u03cd","1 \\u03ba\\u03bf\\u03c5\\u03c4. \\u03c3\\u03bf\\u03cd\\u03c0\\u03b1\\u03c2 \\u03b3\\u03ac\\u03bb\\u03b1","1 \\u03ba\\u03bf\\u03c5\\u03c4. \\u03b3\\u03bb\\u03c5\\u03ba\\u03bf\\u03cd \\u03c3\\u03bc\\u03c5\\u03c1\\u03bd\\u03b9\\u03cc"," 1\\/2 \\u03b2\\u03b9\\u03c4\\u03ac\\u03bc","1 \\u03ba\\u03bf\\u03c5\\u03c4. \\u03c3\\u03bf\\u03cd\\u03c0\\u03b1\\u03c2 \\u03bc\\u03ad\\u03bb\\u03b9"]', 'Τα ανακατεύουμε όλα μαζί (εκτός της μαρμελάδας). Τα πλάθουμε με το χέρι, οπότε το βιτάμ μόλις να μην το λιώσουμε και το αλεύρι το προσθέτουμε σταδιακά. Ίσως να χρειαστεί να ρίξουμε και λίγο παραπάνω(ανάλογα τι θα μας ζητήσει η ζύμη).Αφού το ζυμαράκι μας μπορεί να πλαστεί χωρίς να κολλάει στα χέρια μας βουτυρώνουμε μια φόρμα τάρτας και απλώνουμε τα 3/4 της ζύμης στη φόρμα καλύπτοντας και τα πλαινάς της. Απλώνουμε τη μαρμελάδα και στη συνέχεια κάνουμε το υπόλοιπο της ζύμης μπαστουνάκια για να δημιουργήσουμε τα τετραγωνάκια πάνω από τη μαρμελάδα. Φανταστείτε συνεχόμενα δίεση...Καλή επιτυχία!', 'accepted'),
(74, 136, 'Σουτζουκάκια Σμυρναίϊκα', '["\\u039a\\u03b9\\u03bc\\u03ac\\u03c2 \\u03c7\\u03bf\\u03b9\\u03c1\\u03b9\\u03bd\\u03cc\\u03c2 1\\/2 \\u03ba\\u03b9\\u03bb\\u03cc","\\u039a\\u03ac\\u03c1\\u03b4\\u03b1\\u03bc\\u03bf","\\u0392\\u03b1\\u03c3\\u03b9\\u03bb\\u03b9\\u03ba\\u03cc\\u03c2","2 \\u039a\\u03c1\\u03b5\\u03bc\\u03bc\\u03cd\\u03b4\\u03b9\\u03b1"]', 'Τα ανακατεύουμε και φτιάχνουμε σουτζουκάκια.', 'accepted');

-- --------------------------------------------------------

--
-- Table structure for table `step_machine`
--

DROP TABLE IF EXISTS `step_machine`;
CREATE TABLE IF NOT EXISTS `step_machine` (
  `key` varchar(25) NOT NULL,
  `memId` int(11) NOT NULL,
  `step` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`key`,`memId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `step_machine`
--

INSERT INTO `step_machine` (`key`, `memId`, `step`) VALUES
('photoApp', 17, 1),
('photoApp', 34, 4),
('photoApp', 41, 4),
('photoApp', 48, 4),
('photoApp', 105, 4),
('photoApp', 111, 3),
('photoApp', 112, 3),
('photoApp', 114, 1),
('photoApp', 115, 4),
('photoApp', 116, 1),
('photoApp', 118, 4),
('photoApp', 119, 3),
('photoApp', 120, 3),
('photoApp', 121, 4),
('photoApp', 122, 2),
('photoApp', 123, 3),
('photoApp', 124, 4),
('photoApp', 125, 4),
('photoApp', 126, 1),
('photoApp', 127, 1),
('photoApp', 128, 3),
('photoApp', 129, 4),
('photoApp', 131, 2),
('photoApp', 132, 1),
('photoApp', 133, 4),
('photoApp', 134, 4),
('photoApp', 135, 3),
('photoApp', 136, 2),
('recipe', 17, 4),
('recipe', 34, 4),
('recipe', 41, 4),
('recipe', 48, 4),
('recipe', 105, 2),
('recipe', 119, 2),
('recipe', 120, 1),
('recipe', 122, 4),
('recipe', 123, 2),
('recipe', 128, 3),
('recipe', 130, 2),
('recipe', 136, 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
