<?php
if (param('google.analytics.id')) {
    $cs = Yii::app()->getClientScript();

    $gAnalyticsValues = array(
        '_setAccount' => param('google.analytics.id')
    );
    if(param('google.analytics.domain'))
        $gAnalyticsValues['_setDomainName'] = param('google.analytics.domain');

    $gAnalytics = "var _gaq = _gaq || [];";
    foreach ($gAnalyticsValues as $key => $value) {
        $gAnalytics .= "_gaq.push(['$key', '$value']);";
    }
    $gAnalytics .= "_gaq.push(['_trackPageview']);
        (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        ";

    $cs->registerScript('gAnalytics', $gAnalytics, CClientScript::POS_END);
}
?>
