
<!-- BEGIN - Admin wrapper -->
<div id="admin-wrapper">
    <?php if (Yii::app()->user->isGuest): ?>
        <a class="logo" href="<?php echo Yii::app()->createUrl('site/externalAuth', array('name' => 'facebook')); ?>" title="descr...">
            login
        </a>
    <?php else: ?>
        <a class="logo" href="<?php echo Yii::app()->createUrl('site/logout', array('name' => 'facebook')); ?>" title="descr...">
            logout
        </a>
    <?php endif; ?>
    <br>
    <?php if(!Yii::app()->session['pageLiked']): ?>
        <a class="logo" href="?debug_pagelike" title="descr...">
            Debug page like
        </a>
    <?php else: ?>
        <a class="logo" href="?clearInsideFacebook" title="descr...">
            Clear page like and logout
        </a>
    <?php endif; ?>
    <br>
    <a class="logo" href="/adminSite" title="">
        AdminSite
    </a>
    <?php
    if (!Yii::app()->user->isGuest) {
        $member = Yii::app()->user->member;
        echo "id: " . Yii::app()->user->id . "<br/>";
        echo CHtml::image($member->photo_url) . "<br/>";
        echo "Username: " . Yii::app()->user->name . "<br/>";
        echo "Fname: " . $member->fname . "<br/>";
        echo "Active: " . $member->active . "<br/>";
//        echo "MemoryResults: " . $member->hasMemoryResults . "<br/>";

    } else {
        echo "<br/>anonymous<br/>";
    }
    $provider = $this->getAuthProvider($this->providerName);
    echo "<br/>InsideFacebook: " . ($provider->insideFacebook()?'true':'false') . "<br/>";
    echo "<br/>App data: " . "<br/>";
    ydump($provider->appData);
    echo "<br/>Page liked: " . "<br/>";
    ydump($provider->pageLiked());
    ydump(Yii::app()->session['pageLiked']);
    echo "<br/>GET: " . "<br/>";
    ydump($_GET);
    echo "<br/>POST: " . "<br/>";
    ydump($_POST);
    echo "<br/>Signed request: " . "<br/>";
    ydump($provider->signedRequest);
//    ydump(CJSON::encode(array('alpha'=>1,'beta'=>"http://local.wwforce.gr/?app_data=asdas%20da")));
//    ydump(urlencode(CJSON::encode(array('alpha'=>1,'beta'=>"http://local.wwforce.gr/?app_data=asdas%20da"))));
//    {\"alpha\":1,\"beta\":\"http:\\/\\/local.wwforce.gr\\/?app_data=asdas%20da\"}
//    %7B%22alpha%22%3A1%2C%22beta%22%3A%22http%3A%5C%2F%5C%2Flocal.wwforce.gr%5C%2F%3Fapp_data%3Dasdas%2520da%22%7D
    ?>
</div>
<!-- END - Admin wrapper -->