<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta name="author" content="wwforce" />
        <meta name="robots" content="index, follow" />
        <meta name="description" content="<?php echo $this->pageDescription; ?>" />
        <meta name="keywords" content="<?php echo $this->pageKeywords; ?>" />

        <title><?php
            if ($this->pageTitle != Yii::app()->name)
                echo CHtml::encode($this->pageTitle . " - " . Yii::app()->name);
            else
                echo CHtml::encode($this->pageTitle);
            ?></title>
        <link rel="icon" type="image/ico" href="/favicon.ico"></link>
        <link rel="stylesheet" type="text/css" href="/res/css/main.css" />

        <meta property="og:site_name" content="<?php echo Yii::app()->name; ?>" />
        <meta property="og:title" content="<?php echo CHtml::encode($this->pageTitle); ?>" />
        <meta property="og:description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />
        <meta property="og:url" content="http://www.<?php echo Yii::app()->name; ?>" />
        <meta property="og:image" content="/res/img/logo.png" />
        <meta property="og:type" content="website" />

        <?php
        $this->renderPartial('/layouts/_gAnalytics');
        ?>
    </head>

    <body>
        <!-- BEGIN - Page container -->
        <div class="page-container">

            <!-- BEGIN - Content wrapper -->
            <?php
            $this->loadDefaultScripts();
            $this->renderPartial('/layouts/_debug');
            ?>

            <!-- BEGIN - "CANVAS" -->
            <div id="fb-canvas" class="clearfix">
                <div class="clearfix" id="main-content">

                    <h1><?php echo $this->defaultHeader; ?></h1>
                    <a href="/">
                        <img class="logo small" src="/res/img/logo.png"/>
                    </a>

                    <div class="placeholders">
                        <div class="left-bubble">
                            <?php echo $this->clips['left-bubble']; ?>
                            <?php echo $content; ?>
                        </div>
                        <div class="placeholder-r-1">
                            <?php echo $this->clips['right-top']; ?>
                        </div>
                        <div class="placeholder-r-2">
                            <?php echo $this->clips['right-bubble']; ?>
                        </div>
                    </div>
                    <div class="footer">
                        <?php echo $this->clips['footer']; ?>
                    </div>
                </div>
            </div>
            <!-- END - "CANVAS" -->
            <div class="clear"/>
            <!-- END - Content wrapper -->
        </div>
        <!-- END - Page container -->

    </body>
</html>
