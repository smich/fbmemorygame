
<?php header('Access-Control-Allow-Origin: *'); ?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <style>
            body {
                background-color: #F4E7BD;
                color: #943415;
                font-family: sans-serif;
            }
            h1 {font-size: 110%}
        </style>
        <link rel="stylesheet" type="text/css" href="/res/css/memory.css" media="all">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="/res/js/memory.js"></script>
        <script type="text/javascript">
            $(function() {
                $.ajax({
                    url: "http://api.flickr.com/services/feeds/photos_public.gne?format=json&id=29272059@N02&jsoncallback=?",
                    type: "GET",
                    dataType: "json",
                    error: function(err) {
                        // fallback for connection errors, show the fout default pictures
                        var img = ['img1.jpg', 'img2.jpg', 'img3.jpg', 'img4.jpg'];
                        for (var i = 0; i < img.length; i++) {
                            addCard(img[i]);
                        };
                        startCardGame();
                        $("#loading").text("error retrieving flickr pictures, pictures from cache loaded");
                    },
                    success: function(data) {
                        var items = data.items;
                        var i, length, item;
                        length = (items.length > 8) ? 8 : items.length;
                        for (i=0; i<length; i++) {
                            item = items[i];
                            addCard(item.media.m);
                        }
                        startCardGame();
                        $("#loading").remove();
                        $("#notification").show();
                    },
                    timeout: 10000
                });

                function addCard(img) {
                    var objImg = '<img src="' + img + '" alt="">';
                    var newcard = '<li class="card">' + objImg + '</li>';
                    $("#cards").append(newcard).append(newcard);
                }

                function startCardGame() {
                    $('#cards').memoryGame();
                }

            });
        </script>
    </head>
    <body>
        <noscript>Για να δείτε την εφαρμογή χρειάζεστε browser με JavaScript!</noscript>
        <div class="container">
            <div id="loading">φορτώνει...</div>
            <div id="complete">Μπράβο!</div>
            <div id="notification"><p><span>0</span> προσπάθειες</p></div>
            <ul id="cards"></ul>
        </div>
    </body>
</html>