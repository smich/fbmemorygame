<?php
// Display a flash message if any
if (Yii::app()->user->hasFlash('memory-complete')):
    $flash = Yii::app()->user->getFlash('memory-complete');
    echo '<div class="flash-' . $flash['type'] . '">' . $flash['message'] . "</div>";

else:
    ?>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'memory-app-form',
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'hideErrorMessage' => true,
            'errorCssClass' => 'er',
            'afterValidate' => 'js: validateAppForm'
        ),
        'enableClientValidation' => true
            ));

    $form->errorSummary($model);
    ?>
    <div class="user-container">
        <div class="user-canvas clearfix">
            <div class="user-image" style="background-image: url(<? echo $user->photo_url; ?>);">

            </div>
            <div class="user-name"><?php echo $user->fname; ?></div>

        </div>
    </div>
    <div class="user-input clearfix">
        <?php //echo $form->labelEx($model,'message'); ?>
        <?php echo $form->textArea($model, 'message', array('placeholder' => 'Γράψτε το σχόλιό σας εδώ..')); ?>
        <?php echo $form->error($model, 'message'); ?>

        <div class="left">
            <div>
                <?php echo $form->checkBox($model, 'discloseName', array('checked' => $model->discloseName ? 'checked' : '')); ?>
                <?php echo $form->labelEx($model, 'discloseName'); ?>
            </div>
            <div>
                <?php echo $form->checkBox($model, 'iagree', array('checked' => $model->iagree ? 'checked' : '')); ?>
                <?php echo $form->labelEx($model, 'iagree', array('label' => 'Αποδέχομαι τους <a href="#">όρους χρήσης</a>')); ?>
            </div>
        </div>

        <?php echo CHtml::submitButton('Υποβολή', array('class' => 'right gutter-top')); ?>
    </div>

    <?php $this->endWidget(); ?>
<?php endif; ?>

<script>
    function validateAppForm(form, data, hasError){
        if( !$('#MemoryForm_iagree').is(':checked')){
            hasError = true;
            data.PhotoAppForm_iagree = 'Αποδεχτείτε του όρους χρήσης για να συνεχίσετε';
        }
        return WWF.validateForm(form, data, hasError);
    }
</script>