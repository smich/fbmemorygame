<?php
$this->layout = '/layouts/fbCanvas';
$this->widget('JsPageMemoryApp');

$this->beginClip('right-top');
?>
<?php
$this->endClip();

$this->beginClip('left-bubble');
?>
<div class="text">
	<strong>Κάνε το πιο φωναχτό
		<a href="<?php echo $this->actionUrl($this->action->id,array('nextStep'=>'1')); ?>">
			<img class="like" valign="middle" src="/res/img/icons/like.png" /></a>
	</strong><br/>
	Μόνο αν ενώσουμε τις φωνές μας,<br>μπορούμε να βοηθήσουμε.
	<div class="expert">
		<img class="left" src="/res/img/dr-melidonis.png"/>
		<br>
		<span class="title">Δρ. Ανδρέας Μελιδώνης</span><br>
		Πρόεδρος της Ελληνικής Διαβητολογικής Εταιρείας
	</div>
</div>
<?php
$this->endClip();

$this->beginClip('right-bubble');
?>
<div class="text">
	<br><br>
	Γράψε το μήνυμα σου για το διαβήτη, έλα στο Πνευματικό Κέντρο του Δήμου Αθηναίων, πάρε μέρος στην ποδηλατοδρομία και ενημερώσου για την &laquo;έξυπνη διατροφή των ατόμων με διαβήτη&raquo;.
	<br><br>
	<center class="prepend-1">
		<?php echo l(	'OK',
						$this->actionUrl($this->action->id,array('nextStep'=>'1')),
						array('class'=>'button')
					); ?>
	</center>
</div>
<?php
$this->endClip();

$this->beginClip('footer'); ?>
<?php
$this->endClip();
