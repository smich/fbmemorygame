<?php
$this->layout = '/layouts/fbCanvas';
$this->widget('JsPageMemoryApp');

$this->beginClip('right-top');
$this->endClip();

$this->beginClip('left-bubble');
$this->endClip();

$this->beginClip('right-bubble');
?>
<?php $this->renderPartial($partialView, array('model' => $model, 'formStep'=>$formStep)); ?>
<?php $this->endClip(); ?>


<?php $this->beginClip('footer'); ?>
<?php $this->endClip(); ?>
