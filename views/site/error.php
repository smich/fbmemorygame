<?php
$this->pageTitle = 'Error';

$this->beginClip('right-top');
echo "Error $code";
$this->endClip();

$this->beginClip('left-bubble');
echo CHtml::tag('div', array('class'=>'text large'), CHtml::encode($message));
$this->endClip();

$this->beginClip('right-bubble');
$this->endClip();

$this->beginClip('footer');
$this->endClip();
