<?php
$completePhotoApp = false;
$completeRecipe = false;
if (!Yii::app()->user->isGuest) {
//    $completePhotoApp = Yii::app()->user->member->hasMessage ? true : false;
//    $completeRecipe = Yii::app()->user->member->hasRecipe ? true : false;
}
$this->beginClip('navigation-buttons');

if (!$completePhotoApp)
    echo l('Μοιράσου μαζί μας το θετικό μήνυμά σου', '/memory/'.($this->insideFacebook?'index':'authenticate/type/manual'), array('class' => 'photoApp'));
elseif ($this->insideFacebook)
{
    $shareButton = true;
    echo CHtml::image ("/res/img/fb-buttons/share.png", '', array(
        'class' => 'gutter-top',
        'style' => 'cursor: pointer;position: absolute;top: 5px;left: 20px;',
        'data-facebook-ui' => 'apprequests',
        'data-facebook-params' => CJSON::encode(array(
        'message' => 'Στείλε το θετικό μήνυμα σου και υποστήριξε και εσύ τα άτομα με διαβήτη.'
    ))));
}

if (!isset ($shareButton) && $this->insideFacebook)
    echo CHtml::image ("/res/img/fb-buttons/share.png", '', array(
        'class' => 'gutter-top',
        'style' => 'cursor: pointer;position: absolute;top: 5px;left: 20px;',
        'data-facebook-ui' => 'apprequests',
        'data-facebook-params' => CJSON::encode(array(
        'message' => 'Στείλε το θετικό μήνυμα σου και υποστήριξε και εσύ τα άτομα με διαβήτη.'
    ))));

$this->endClip();

$this->layout = '/layouts/fbCanvas';

$this->widget('JsPageMemoryApp');
$this->forceMessagesSmile = true;

$this->beginClip('right-top');
?>
<strong>Δες τα θετικά μηνύματα & τις &laquo;έξυπνες συνταγές&raquo; που έχετε μοιραστεί
    μαζί μας μέχρι αυτή τη στιγμή.</strong><br>
<br>
<div class="right span-7">
    <?php
    if (isset($_GET['complete']) && $_GET['complete'] == 'recipe') {
        echo Recipe::THANKYOU_MESSAGE;
    } else if (isset($_GET['complete']) && $_GET['complete'] == 'photoApp') {
        echo PhotoApp::THANKYOU_MESSAGE;
    } else {
        echo 'Κάνε κλικ στις φωτογραφίες';
    }
    ?>
</div>
<?php
$this->endClip();

$this->beginClip('left-bubble');
$this->endClip();

$this->beginClip('right-bubble');
//overided by left-buble when in fullextramode!
$this->endClip();

$this->beginClip('footer'); ?>
<?php $this->endClip();
