
    <div class="prepend-2 span-10 last">
        <h2>Check how much it costs</h2>
        <div class="form reservation-container">

            <p class="note">Just fill-in the following fields.</p>

            <div class="row">
                <?php echo CHtml::label('Route','route'); ?>
                <?php echo CHtml::dropDownList('route',0,ReservationForm::optsRoutes(), array('class'=>'route')); ?>
            </div>
            
            <div class="row">
                <?php echo CHtml::label('Total number of passengers','passengers'); ?>
                <?php echo CHtml::textField('passengers',0, array('class'=>'passengers')); ?>
            </div>
            
            <div class="row">
                <?php echo CHtml::label('Children up to 4 years old','kids'); ?>
                <?php echo CHtml::textField('kids',0, array('class'=>'kids')); ?>
            </div>

        </div><!-- form -->
        <div class="reservation-info">
            <h2>Reservation Information</h2>
            <table>
                <tr>
                    <td>Ticket price per passenger:</td>
                    <td class="value">&euro;<span class="passenger-price">0</span></td>
                </tr>
                <tr>
                    <td>Passengers:</td>
                    <td class="value"><span class="passenger-number">0</span></td>
                </tr>
                <tr>
                    <td>Passengers below 4 years old:</td>
                    <td class="value"><span class="kids-number">0</span></td>
                </tr>
                <tr>
                    <td>Online reservation Discount:</td>
                    <td class="value">10%</td>
                </tr>
                <tr>
                    <td class="total-price">Total Price:</td>
                    <td class="total-price value">&euro;<span class="total">0</span></td>
                </tr>
                <tr class="discount-row" style="display: none;">
                    <th colspan="2"><h2>Activated Discount:</h2></th>
                </tr>
                <tr class="discount-row" style="display: none;">
                    <td colspan="2"><span class="discount"></span></td>
                </tr>
            </table>
        </div>
    </div>
