<?php

$this->forcePrelikeBg = true;
$this->layout = '/layouts/fbCanvas';

$this->beginClip('navigation-buttons');
$this->endClip();

$this->widget('JsPageMemoryApp');

$this->beginClip('right-top'); ?>

<?php
/*
if(isset($provider->appData['data']))
    echo ' type: '.$provider->appData['data'];
*/
$this->endClip();

$this->beginClip('prelike-text');?>
<div class="span-13 append-bottom">
		Κάνε <span class="blue">like</span> και υποστήριξε τα άτομα με διαβήτη, με έναν από τους παρακάτω τρόπους:
</div>

<ul class="blue clear prepend-top clearfix">
	<li>Μοιράσου μαζί μας το «θετικό» σου μήνυμα</li>
	<li>Στείλε μας την «έξυπνη» συνταγή σου</li>
	<li>Πάρε μέρος στην Ποδηλατοδρομία</li>
</ul>
<?php $this->endClip();

$this->beginClip('right-top'); ?>
<img class="like-arrow" src="/res/img/like-arrow.png" />
<?php $this->endClip();

$this->beginClip('footer'); ?>
<?php $this->endClip();
