<?php
$this->pageTitle='Reservation';
$this->breadcrumbs=array(
    'Reservation',
);

$cs = Yii::app()->getClientScript();
$cs->registerScriptFile('/res/js/billing.js',CClientScript::POS_END);
$cs->registerScriptFile('/res/js/reservation.js',CClientScript::POS_END);

?>

<div class="reservation-container">
    <h1>Make a reservation now</h1>

    <?php if(Yii::app()->user->hasFlash('reservation')): ?>

    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('reservation'); ?>
    </div>

    <?php else: ?>

    <div class="span-16">
        <div class="form">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'reservation-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        ));

        ?>

            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <?php echo $form->errorSummary($model); ?>

            <div class="row">
                <?php echo $form->labelEx($model,'route'); ?>
                <?php echo $form->dropDownList($model,'route', ReservationForm::optsRoutes(), array('class'=>'route')); ?>
                <?php echo $form->textField($model,'customRoute', array('class'=>'customRoute','placeholder'=>'From Place A To Place B')); ?>
                <?php echo $form->error($model,'route'); ?>
                <?php echo $form->error($model,'customRoute'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'departureDate'); ?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'departureDate',
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=>'dd/mm/yy'
                    ),
                    'htmlOptions'=>array(),
                ));?>
                <?php echo $form->error($model,'departureDate'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'departureTime'); ?>
                <?php echo Chtml::dropDownList('departureHour','09',ReservationForm::optsDepartureHours(),array('class'=>'departureHour')); ?>
                <?php echo Chtml::dropDownList('departureMinute','00',ReservationForm::optsDepartureMinutes(),array('class'=>'departureMinute')); ?>
                <?php echo $form->hiddenField($model,'departureTime', array('class'=>'departureTime')); ?>
                <?php echo $form->error($model,'departureTime'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'email'); ?>
                <?php echo $form->textField($model,'email'); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'tel'); ?>
                <?php echo $form->textField($model,'tel',array('placeholder'=>'e.g +30 213 029 1852')); ?>
                <?php echo $form->error($model,'tel'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'passengers'); ?>
                <?php echo $form->textField($model,'passengers', array('placeholder'=>4, 'class'=>'passengers', 'value'=>4)); ?>
                <?php echo $form->error($model,'passengers'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'kids'); ?>
                <?php echo $form->textField($model,'kids', array('placeholder'=>0, 'class'=>'kids','value'=>0)); ?>
                <?php echo $form->error($model,'kids'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'flightInfo'); ?>
                <?php echo $form->textField($model,'flightInfo',array('placeholder'=>'Flight No ~ From - To')); ?>
                <?php echo $form->error($model,'flightInfo'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'comments'); ?>
                <?php echo $form->textArea($model,'comments',array('placeholder'=>'Comments - Hotel Info','rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model,'comments'); ?>
            </div>

            <?php if(CCaptcha::checkRequirements()): ?>
            <div class="row">
                <?php echo $form->labelEx($model,'verifyCode'); ?>
                <div>
                <?php $this->widget('CCaptcha'); ?>
                <?php echo $form->textField($model,'verifyCode'); ?>
                </div>
                <div class="hint">Please enter the letters as they are shown in the image above.
                <br/>Letters are not case-sensitive.</div>
                <?php echo $form->error($model,'verifyCode'); ?>
            </div>
            <?php endif; ?>

            <div class="row buttons">
                <?php echo CHtml::submitButton('Send'); ?>
            </div>

        <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>
    <div class="span-7 last reservation-info-container">
        <div class="reservation-info">
            <h2>Reservation Information</h2>
            <table>
                <tr>
                    <td>Ticket price per passenger:</td>
                    <td class="value">&euro;<span class="passenger-price">0</span></td>
                </tr>
                <tr>
                    <td>Passengers:</td>
                    <td class="value"><span class="passenger-number">0</span></td>
                </tr>
                <tr>
                    <td>Children up to 4 years old:</td>
                    <td class="value"><span class="kids-number">0</span></td>
                </tr>
                <tr class="night-route-fee">
                    <td>Night route fee:</td>
                    <td class="value">+20%</td>
                </tr>
                <tr>
                    <td>Online reservation Discount:</td>
                    <td class="value">-5%</td>
                </tr>
                <tr>
                    <td class="total-price">Total Price:</td>
                    <td class="total-price value">&euro;<span class="total">0</span></td>
                </tr>
                <tr class="discount-row">
                    <th colspan="2"><h2>Activated Discount:</h2></th>
                </tr>
                <tr class="discount-row">
                    <td colspan="2"><span class="discount"></span></td>
                </tr>
            </table>
        </div>
        <div class="reservation-inquiry">
            <h2>Reservation Inquiry</h2>
            <p>For Prices on customized transportations regarding other destinations than those listed, please <a href="/site/contact" title="Contact our office representative via land line or e-mail and get immediate answers!">contact</a> our office agent.</p>
        </div>
    </div>
    <?php endif; ?>
</div>
