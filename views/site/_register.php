<div class="form prepend-1" style="text-align: center;" >

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'upload-form',
        'clientOptions' =>  array(
                            'validateOnSubmit'  =>  true,
                            'hideErrorMessage'  =>  true,
                            'errorCssClass'     =>  'er',
                            'afterValidate'     =>  'js: WWF.validateForm'
                        ),
        'enableClientValidation'=>  true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
            ));

    if ($formStep == 'step1')
        echo "<br><br>";
    ?>

    <p class="note">Τα πεδία με αστερίσκο<span class="required">*</span> είναι υποχρεωτικά.</p>

    <?php
//    echo $form->errorSummary($model);

    $fields = array(
        'fname' => 'textField',
        'lname' => 'textField',
        'email' => ($formStep == 'step1' ? 'textField' : 'hiddenField'),
        'photo' => 'fileField',
    );
    foreach ($fields as $field => $type) {
        if ($formStep == 'step1' && $field == 'email' || $formStep == 'step2') {
            if ($type == 'hiddenField')
                echo $form->{$type}($model, $field);
            else {
                echo '<div class="row">';
                echo $form->labelEx($model, $field);
                echo $form->{$type}($model, $field);
                echo $form->error($model, $field);
                echo '</div>';
            }
        }
    }
    ?>
    <div class="row buttons">
        <br>
        <?php
        $btnLabel = ($formStep == 'step1') ? "Συμπληρώστε το e-mail σας" : "Εγγραφή";
        echo CHtml::submitButton($btnLabel);

        if ($formStep == 'step1') {
            if (!$this->insideFacebook)
                echo '<div class="clear clearfix gutter-top">ή</div>'
                . l('<img src="/res/img/fb-buttons/connect.png"/>', '/memory/index', array('class' => 'photoApp gutter-top clearfix'));
        }
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
