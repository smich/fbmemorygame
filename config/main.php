<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Memory game',
    'language' => 'el',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.components.externalAuth.*',
        'application.components.externalAuth.provider.*',
        'application.components.widgets.*',
        'application.extensions.yii-mail.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'qwerty1',
            'generatorPaths' => array(
                'application.extensions.gii',
            ),
        ),
        'admin' => array(),
    ),
    // application components
    'components' => array(
        // Yii::app->adminUser in frontend, Yii::app()->user in admin module:
        'adminUser' => array(
            'class' => 'application.modules.admin.components.AdminWebUser',
            'allowAutoLogin' => true, // enable cookie-based authentication
            'loginUrl' => array('/admin/site/login'),
            'stateKeyPrefix' => 'admin',
        ),
        'user' => array(
            'class' => 'WebUser', // extends default functionality
            'allowAutoLogin' => true, // enable cookie-based authentication
            'stateKeyPrefix' => 'member',
        ),
        'stepMachine' => array(
            'class' => 'StepMachineComponent',
        ),
        'mail' => array(
            'class' => 'application.extensions.yii-mail.YiiMail',
            'transportType' => 'smtp', /// case sensitive!
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => 'info.athensairexpress@gmail.com',
                'password' => 'raplot2009',
                'port' => '465',
                'encryption' => 'ssl',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'gii' => 'gii',
                'gii/<controller:\w+>' => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
                // a standard rule mapping '/' to 'site/index' action
                '/' => 'site/index',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                // Administration section
                'adminSite/<_m>/<_c>/<_a>' => 'admin/<_m>/<_c>/<_a>',
                'adminSite/<_c>/<_a>' => 'admin/<_c>/<_a>',
                'adminSite' => 'admin/site',
            ),
        ),
        /* 'db'=>array(
          'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
          ), */
        // uncomment the following to use a MySQL database
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
        ),
        // Load shared configuration for clientScript component (reused in CLI apps):
        'clientScript' => array(
            'class' => 'ClientScriptPackageCompressor',
            'coreScriptPosition' => 2, // == CClientScript::POS_END
            // Requires JRE, so turn it on manually in local.php:
            'enablePackageCompression' => false,
            // We include jQuery from Google CDN:
            'scriptMap' => array(
                'jquery.js' => 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js',
                'jquery.min.js' => 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js',
                'jquery-ui.min.js' => 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.22/jquery-ui.min.js',
                'jquery-ui.css' => false,
            ),
            'packages' => array(
                // CONCRETE PACKAGE: Main
                //
                // This is our main package. It contains all Js framework files and adds
                // the neccessary dependencies. Be sure to add your Js page and module files here.
                'js-main' => array(
                    'baseUrl' => '/res/js',
                    'cdn' => true,
                    'js' => array(
                        'wwf.js',
                        'wwf.common.js',
                        'wwf.pages.MemoryApp.js',
                        'wwf.modules.FbApi.js',
                        'wwf.modules.GoogleTracking.js',
                    ),
                    'depends' => array('wwf-jquery-plugins', 'jquery.ui', 'bbq', 'yii', 'yiitab', 'yiiactiveform', 'cookie', 'qtip', 'listview', 'gridview'),
                ),
                // Our custom GridView
                'gridview' => array(
                    'basePath' => 'zii.widgets.assets.gridview',
                    'js' => array('jquery.yiigridview.js'),
                    'depends' => array('jquery', 'bbq'),
                ),
                // Our custom ListView
                'listview' => array(
                    'basePath' => 'zii.widgets.assets.listview',
                    'js' => array('jquery.yiilistview.js'),
                    'depends' => array('jquery', 'bbq'),
                ),
                // SUB-PACKAGE: All jQuery plugins we use in our Js Modules or Pages
                'wwf-jquery-plugins' => array(
                    'baseUrl' => '/res/js/lib',
                    'js' => array(
                        'jquery.min.js',
                        'jquery-ui-1.9.1.custom.min.js',
                        'jquery.qtip.js',
                        'jquery.tinyScrollbar.js'
                    ),
                    'depends' => array('jquery.ui'),
                ),
                'css-main' => array(
                    'baseUrl' => '/res/css',
                    'css' => array(
                        // resets (these should be kept at the start of the includes)
                        'screen.css',
                        'main.css',
                        'form.css',
                        'jquery.qtip.css',
                        'jq-ui-custom-theme/jquery-ui-1.9.1.custom.css',
                    )
                )
            )
        ),
        'externalAuth' => array(
            'class' => 'application.components.externalAuth.ExternalAuthManager',
        ),
        'externalRequest' => array(
            'class' => 'application.components.externalRequester.ExternalRequestManager',
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'info@mailinator.com',
        'adminPass' => 'infoaae123!@#',
        'adminName' => 'AthensAirExpress Info',

        'imageUrl' => '/uploads/',
        'imagePath' => dirname(__FILE__).'/../www/uploads/',
    ),
);
