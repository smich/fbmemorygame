<?php

return array(
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=fbMemoryGame',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'qwerty1',
            'charset' => 'utf8',
        ),

        'externalAuth' => array(
            'provider' => array(
                // https://developers.facebook.com/apps
                'facebook' => array(
                    'applicationId' => '501268576562527',
                    'consumerSecret' => '0e92ea7abc008f400db064ee35544068',
                    'namespace' => 'ww-memory-game',
//                    'canvas' => true, // true if is a canvas app, false otherwise
                    'admins' => implode(',', array(
                        '1044272508', //Giorgos
                    )),
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'google.analytics.id' => 'UA-36251440-3', // webwonder.gr
        'google.analytics.domain' => 'webwonder.gr',
    ),
);