<?php

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

return array(
    'components' => array(
        'log' => array(
            'routes' => array(
                // Add one ore more of the following here:
                // Adds log to end of every page (enable YII_DEBUG to see system trace)
                //  - set YII_DEBUG above to see system.* traces
                //  - set 'enableParamLogging' in db.php to see SQL queries
                array(
                    'class' => 'CWebLogRoute',
                ),
                // Adds profiling output to end of every page
                //  - set 'enableProfiling' for db, to see SQL query profiling
                array(
                    'class' => 'CProfileLogRoute',
                ),

                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning, info',
                    'logFile'   =>'main.log',
                    'logPath'=>dirname(__FILE__).'/../logs',
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
// using Yii::app()->params['paramName']
    'params' => array(),
);
?>
