/*!
 * Tiny Scrollbar 1.66
 * http://www.baijs.nl/tinyscrollbar/
 *
 * Copyright 2010, Maarten Baijs
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/gpl-2.0.php
 *
 * Date: 13 / 11 / 2011
 * Depends on library: jQuery
 * .tinyscrollbar()
 */

(function($){
    $.tiny = $.tiny || { };

    $.tiny.scrollbar = {
        options: {
            axis: 'y', // vertical or horizontal scrollbar? ( x || y ).
            wheel: 40,  //how many pixels must the mouswheel scroll at a time.
            scroll: true, //enable or disable the mousewheel;
            size: 200, //set the size of the scrollbar to auto or a fixed number.
            sizethumb: 'auto', //set the size of the thumb to auto or a fixed number.
            minsizethumb: 20 //set the minimum size of the thumb to false or a fixed number.
        }
    };

    $.fn.tinyscrollbar = function(options) {
        var options = $.extend({}, $.tiny.scrollbar.options, options);
        this.each(function(){
            $(this).data('tsb', new Scrollbar($(this), options));
        });
        return this;
    };
    $.fn.tinyscrollbar_update = function(sScroll) {
        return $(this).data('tsb').update(sScroll);
    };

    function Scrollbar(root, options){
        /** Class names **/
        var classPrefix     = 'tiny-scroll-';
        var classScrollbar  = classPrefix+'scrollbar';
        var classTrack      = classPrefix+'track';
        var classThumb      = classPrefix+'thumb';
        var classEnd        = classPrefix+'end';
        var classViewport   = classPrefix+'viewport';
        var classOverview   = classPrefix+'overview';

        createHtml(root);
        var rootPosition = root.css('top');
        var rootVisibility = root.is(":visible");
        if( !rootVisibility )
            root.css('top','-3000px').show();
        var oSelf = this;
        var oWrapper = root;
        var oViewport = {
            obj: $('.'+classViewport, root)
            };
        var oContent = {
            obj: $('.'+classOverview, root)
            };
        var oScrollbar = {
            obj: $('.'+classScrollbar, root)
            };
        var oTrack = {
            obj: $('.'+classTrack, oScrollbar.obj)
            };
        var oThumb = {
            obj: $('.'+classThumb, oScrollbar.obj)
            };
        var sAxis = options.axis == 'x', sDirection = sAxis ? 'left' : 'top', sSize = sAxis ? 'Width' : 'Height';
        var iScroll, iPosition = {
            start: 0,
            now: 0
        }, iMouse = {};

        function initialize() {
            oSelf.update();
            setEvents();
            if( root.find('.'+classViewport).height() > root.find('.'+classOverview).height() )
                root.find('.'+classViewport).height(root.find('.'+classOverview).height());
            if( !rootVisibility )
                root.css('top',rootPosition).hide();
            return oSelf;
        }
        function createHtml(root) {
            if( root.find('div.'+classScrollbar+' div.'+classTrack+' div.'+classThumb+' div.'+classEnd).length==0 ||
                root.find('div.'+classViewport+' div.'+classOverview).length==0
                ){
                var content = root.children();
                if( content.length==0 )
                    content = root.html();
                var end = $('<div></div>').addClass(classEnd).css('overflow','hidden');
                var thumb = $('<div></div>').addClass(classThumb)
                .css('cursor','pointer')
                .css('overflow','hidden')
                .css('position','absolute')
                .css('top','0')
                .append(end);
                var track = $('<div></div>').addClass(classTrack).css('position','relative').append(thumb);
                if(options.minTrackHeight)
                    track.css('min-height',options.minTrackHeight);
                var scrollbar = $('<div></div>').addClass(classScrollbar).css('position','relative').css('float','right').append(track);
                var overview = $('<div></div>').addClass(classOverview)
                .css('list-style','none')
                .css('position','absolute')
                .css('left','0')
                .css('top','0')
                .css('width','100%')
                .append(content);
                var viewport = $('<div></div>').addClass(classViewport)
                .css('overflow','hidden')
                .css('position','relative')
                .append(overview);
                if(options.size!='auto')
                    viewport.css('height',options.size);
                root.css('clear','both')
                .html('')
                .append(scrollbar)
                .append(viewport);
            }
        }
        this.update = function(sScroll){
            oViewport[options.axis] = oViewport.obj[0]['offset'+ sSize];
            oContent[options.axis] = oContent.obj[0]['scroll'+ sSize];
            oContent.ratio = oViewport[options.axis] / oContent[options.axis];
            oScrollbar.obj.toggleClass('hide', oContent.ratio >= 1);
            oTrack[options.axis] = options.size == 'auto' ? oViewport[options.axis] : options.size;
            oThumb[options.axis] = Math.min(oTrack[options.axis], Math.max(0, ( options.sizethumb == 'auto' ? (oTrack[options.axis] * oContent.ratio) : options.sizethumb )));
            if(options.minsizethumb && oThumb[options.axis]<options.minsizethumb) {
                options.sizethumb = options.minsizethumb;
                oThumb[options.axis] = Math.min(oTrack[options.axis], Math.max(0, ( options.sizethumb == 'auto' ? (oTrack[options.axis] * oContent.ratio) : options.sizethumb )));
            }
            oScrollbar.ratio = options.sizethumb == 'auto' ? (oContent[options.axis] / oTrack[options.axis]) : (oContent[options.axis] - oViewport[options.axis]) / (oTrack[options.axis] - oThumb[options.axis]);
            iScroll = (sScroll == 'relative' && oContent.ratio <= 1) ? Math.min((oContent[options.axis] - oViewport[options.axis]), Math.max(0, iScroll)) : 0;
            iScroll = (sScroll == 'bottom' && oContent.ratio <= 1) ? (oContent[options.axis] - oViewport[options.axis]) : isNaN(parseInt(sScroll)) ? iScroll : parseInt(sScroll);
            setSize();
        };
        function setSize(){
            oThumb.obj.css(sDirection, iScroll / oScrollbar.ratio);
            oContent.obj.css(sDirection, -iScroll);
            iMouse['start'] = oThumb.obj.offset()[sDirection];
            var sCssSize = sSize.toLowerCase();
            oScrollbar.obj.css(sCssSize, oTrack[options.axis]);
            oTrack.obj.css(sCssSize, oTrack[options.axis]);
            oThumb.obj.css(sCssSize, oThumb[options.axis]);
        };
        function setEvents(){
            oThumb.obj.bind('mousedown', start);
            oThumb.obj[0].ontouchstart = function(oEvent){
                oEvent.preventDefault();
                oThumb.obj.unbind('mousedown');
                start(oEvent.touches[0]);
                return false;
            };
            oTrack.obj.bind('mouseup', drag);
            if(options.scroll && this.addEventListener){
                oWrapper[0].addEventListener('DOMMouseScroll', wheel, false);
                oWrapper[0].addEventListener('mousewheel', wheel, false );
            }
            else if(options.scroll){
                oWrapper[0].onmousewheel = wheel;
            }
        };
        function start(oEvent){
            iMouse.start = sAxis ? oEvent.pageX : oEvent.pageY;
            var oThumbDir = parseInt(oThumb.obj.css(sDirection));
            iPosition.start = oThumbDir == 'auto' ? 0 : oThumbDir;
            $(document).bind('mousemove', drag);
            document.ontouchmove = function(oEvent){
                $(document).unbind('mousemove');
                drag(oEvent.touches[0]);
            };
            $(document).bind('mouseup', end);
            oThumb.obj.bind('mouseup', end);
            oThumb.obj[0].ontouchend = document.ontouchend = function(oEvent){
                $(document).unbind('mouseup');
                oThumb.obj.unbind('mouseup');
                end(oEvent.touches[0]);
            };
            return false;
        };
        function wheel(oEvent){
            if(!(oContent.ratio >= 1)){
                var oEvent = oEvent || window.event;
                var iDelta = oEvent.wheelDelta ? oEvent.wheelDelta/120 : -oEvent.detail/3;
                iScroll -= iDelta * options.wheel;
                iScroll = Math.min((oContent[options.axis] - oViewport[options.axis]), Math.max(0, iScroll));
                oThumb.obj.css(sDirection, iScroll / oScrollbar.ratio);
                oContent.obj.css(sDirection, -iScroll);

                oEvent = $.event.fix(oEvent);
                oEvent.preventDefault();
            };
        };
        function end(oEvent){
            $(document).unbind('mousemove', drag);
            $(document).unbind('mouseup', end);
            oThumb.obj.unbind('mouseup', end);
            document.ontouchmove = oThumb.obj[0].ontouchend = document.ontouchend = null;
            return false;
        };
        function drag(oEvent){
            if(!(oContent.ratio >= 1)){
                iPosition.now = Math.min((oTrack[options.axis] - oThumb[options.axis]), Math.max(0, (iPosition.start + ((sAxis ? oEvent.pageX : oEvent.pageY) - iMouse.start))));
                iScroll = iPosition.now * oScrollbar.ratio;
                oContent.obj.css(sDirection, -iScroll);
                oThumb.obj.css(sDirection, iPosition.now);
            }
            return false;
        };

        return initialize();
    };
})(jQuery);