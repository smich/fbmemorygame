/**
 * Main WWF javascript module.
 *
 * This is the main javascript module for WWF javascript code.
 *
 * It provides:
 *
 *  - Namespace for widgets and pages
 *  - Functional constructors for Module and Page
 *  - Common utility methods
 *
 * Be sure to include this file _after_ jQuery and _before_ any other wwf.*.js file!!!
 *
 * NOTE:
 *
 * To help understanding, it's recommended to search the Web for some of
 * Douglas Crockford's common JavaScript patterns:
 *
 *      - JavaScript module pattern
 *      - Functional (or Parasitic) inheritance
 *
 * Check Module() and Page() to see an example of this inheritance pattern.
 */
var WWF=(function($){

    var _reIdFromClass=/\bid-(\w+)/,     // RegEx for id classes like .id-245 or .id-ABC
        modMessages=false;               // Shortcut to initialized Messages module

    /**
     * @param {mixed} a data to compare
     * @param {mixed} b data to compare
     * @return {boolean} wether a and be are equal
     */
    function _equal(a,b)
    {
        if(typeof a =='object')
            a=$.param(a);

        if(typeof b =='object')
            b=$.param(b);

        return a==b;
    }

    /**
     * Make CSRF token and token name available in WWF.csrfToken adn WWF.csrfTokenName.
     * Value will be assigend from cookie if available. If not, page configuration is used.
     * @param {object} c page configuration
     */
    function _prepareCsrf(c)
    {
        WWF.csrfTokenName = c['csrfTokenName']!==undefined ? c['csrfTokenName'] : 'YII_CSRF_TOKEN';

        var t=$.cookie(WWF.csrfTokenName);

        if(t)
            WWF.csrfToken=t;
        else
            WWF.csrfToken= c['csrfToken']!==undefined ? c['csrfToken'] : '';
    }


    // Prepare public WWF object
    var wwf={

        /**
         * @var {object} current page object (always available through WWF.page.)
         */
        page: {},

        /**
         * @var {object} container / namespace for page modules
         */
        pages: {},

        /**
         * @var {object} container / namespace for modules
         */
        modules: {},
        /**
        * Store the PHP environment boolean YII_DEBUG
        * @type {boolean}
        */
        DEBUG: false,

        /**
         * @type {object} The metadata library
         */
        metadata: {},

        /**
         * Functional constructor for Modules
         *
         * A Module is the base object for Pages and other Modules. It implements a
         * simple event system for all Modules:
         *
         *      somemodule.bind('update',function(x) {
         *          alert('Update was triggered with '+x);
         *      },'Other data');
         *
         *      // In somemodule:
         *      that.trigger('update','Event data');
         *
         *      // Event handler:
         *      function update(eventData,otherData) {
         *          // eventData is 'Event data'
         *          // otherData is 'Other data'
         *      }
         *
         * @param {object} config Module configuration
         * @return {object} a new Module object with configuration applied
         */
        Module: function(config) {

            var _c={    // Default configuration for all modules
                },
                _e={};  // Event handlers

            var that={

                /**
                 * @var {string} id/name of module
                 */
                id: null,

                /**
                 * @var {object} current configuration of module
                 */
                config: $.extend(true,{},_c,config),

                /**
                 * Bind a event handler method to an event.
                 *
                 * The handler method should only accept a single argument.
                 *
                 * @param {string} name the event name to bind to
                 * @param {function} handler the function that should handle the event
                 * @return {object} the public object for method chaining
                 */
                bind: function(name,handler,data) {
                    if(typeof(_e[name])==='undefined')
                        _e[name]=[];

                    _e[name].push([handler,data]);
                    return that;
                },

                /**
                 * Unbind all handlers bound do an event
                 *
                 * @param {string} name of the event to unbind
                 */
                unbind: function(name) {
                    if(typeof(_e[name])!=='undefined')
                        delete _e[name];

                    return that;
                },

                /**
                 * Trigger an Event
                 *
                 * All connected event handlers will be called with the supplied argument.
                 * Inside an event handler, "this" will be a reference to the Module that
                 * triggered the event.
                 *
                 * @param {string} name the event to trigger.
                 , @param {mixed} arg the argument that should be supplied to the handlers
                 */
                trigger: function(name,arg) {
                    if (typeof(_e[name])!=='undefined')
                        $.each(_e[name], function(i,bound) {
                            if(typeof(bound[1])==='undefined')
                                bound[0].call(that,arg);
                            else
                                bound[0].call(that,arg,bound[1]);
                        });
                },

                /**
                 * Wether a handler was bound to given event.
                 * @param {string} name the event to check
                 */
                hasHandler: function(name) {
                    return typeof _e[name]!=='undefined' && _e[name].length!==0;
                },

                /**
                 * Store data for this module in URL hash (through BBQ plugin).
                 *
                 * Te be informed whenever the URL data for this module has changed,
                 * implement a public method in the module with this signature:
                 *
                 *      that.onHashChange(data)
                 *
                 * @param {mixed} data arbitrary data to store in URL
                 */
                setState: function(data) {
                    that.page.setState(that.id,data);
                },

                /**
                 * Retrieve data for this module from URL state
                 *
                 * Will return null if no data was stored in URL for this module.
                 */
                getState: function() {
                    return that.page.getState(that.id);
                },

                /**
                 * Default init function
                 */
                init: function() {}

            };

            return that;
        },

        /**
         * Functional constructor for Pages
         *
         * A Page is a special Module that manages configuration and initialization
         * of sub-modules. If config.modules is provided, it will be used
         * to set up modules on this page:
         *
         *      config.modules={
         *          ModuleNameA: {
         *              moduleParam1: configValue1,
         *              moduleParam2: configValue2
         *          },
         *          ModuleNameB: { ... }
         *      }
         *
         * @param {object} opt_config Page configuration
         * @return {object} a new Page object with configuration applied
         */
        Page: function(opt_config) {
            // in edge cases (when something goes horribly wrong)
            // the Page method can be called without any arguments
            // Therefore we take care of this with the assignment bellow
            var config = opt_config || {};

            var _c={         // Default Page configuration
                    modules: {},    // Modules used on this page
                    bu: ''          // Default baseUrl
                },
                c=$.extend(true,{},_c,config),
                that=WWF.Module(c);

            // Make CSRF data available in WWF namespace
            _prepareCsrf(c);
            // Initialize metadata library
            if (config.metadataConf) {
                WWF.metadata.init(config.metadataConf);
            }
            // check for DEBUG state
            WWF.DEBUG = config.DEBUG || false;

            function _onHashChange(e)
            {
                var n=e.getState(), // new state
                    o=that.state;   // old state

                $.each(that.modules, function(i,m) {
                    var has=n[i]!==undefined,   // module has new state
                        had=o[i]!==undefined,   // module already had state
                        changed=has && had && !_equal(n[i],o[i]);

                    // Trigger module event, if its data was added, removed or changed
                    if((has && !had  || !has && had || changed) && m['onHashChange']!==undefined)
                        m.onHashChange(n[i]);
                });

                that.state=n;
            }

            // Add Page specific properties and utilities to Module:
            $.extend(that,{

                /**
                 * @var {object} container for this page's modules
                 */
                modules: {},

                /**
                 * @var {object} current URL state data
                 */
                state: {},

                /**
                 * Redirect to other URL
                 * @param {string} url URL to redirect to
                 * @param {bool} top wether to redirect top parent window
                 */
                redirect: function(url,top) {
                    if(top)
                        window.top.location=url;
                    else
                        window.location=url;
                },

                /**
                 * Reload this page
                 */
                refresh: function() {
                    window.location.reload();
                },

                /**
                 * @param {string} name module name to check for
                 * @return {bool} wether the current page uses this module
                 */
                hasModule: function(name) {
                    return that.modules[name] ? true: false;
                },

                /**
                 * Store data in URL state
                 *
                 * If key is ommitted, data will be saved under key "page". If there is
                 * a key, it usually represents a module name. Examples:
                 *
                 *      setKey('MyModule', {name: 'Joe'});
                 *      setKey({filter: 1});
                 *
                 * @param {mixed} key (optional) key name, usually module name
                 * @param {mixed} data arbitrary data to save.
                 */
                setState: function(key,data) {
                    var state=$.bbq.getState();

                    if(!data) {
                        data=key;
                        key='page';
                    }
                    state[key]=data;
                    $.bbq.pushState(state);
                },

                /**
                 * Retrieve data from URL state
                 *
                 * If key is ommitted, data for page will be returned. Will return null
                 * if no data was stored in URL.
                 */
                getState: function(key) {
                    var state=$.bbq.getState();
                    if(!key)
                        key='page';

                    return state[key]==undefined ? null : state[key];
                },

                /**
                 * Run the page by initializing all submodules and the page itself
                 */
                run: function() {
                    // Configure and init modules
                    $.each(that.config.modules,function(name,moduleConfig) {
                        if (wwf.modules[name]) {
                            if(typeof moduleConfig['multiInstance'] !== 'undefined'){
                                for(var instanceId in moduleConfig){
                                    if(instanceId == 'multiInstance')
                                        continue;

                                    that.modules[name]=wwf.modules[name](moduleConfig[instanceId]);
                                    that.modules[name].page=that;       // Set reference to this page
                                    that.modules[name].id=name;         // Set module name
                                    that.modules[name].init();
                                }
                            }else{
                                that.modules[name]=wwf.modules[name](moduleConfig);
                                that.modules[name].page=that;       // Set reference to this page
                                that.modules[name].id=name;         // Set module name
                                that.modules[name].init();
                            }

                            if(name==='Messages')               // Shortcut to Messages
                                modMessages=that.modules[name];
                        }
                    });

                    // Set a reference in namespace and add ourself as module 'page'
                    WWF.page=that;
                    that.modules['page']=that;

                    // set global variables for qTip
                    if (!WWF.DEBUG) {
                        // we are in production mode
                        $.fn.qtip.defaults.suppressLogs = true;
                    }

                    // Init the page
                    that.init();

                    // Connect to BBQ's hashchange event
                    // TODO: we use deprecated bind() instead of on() to make legacy's jQuery 1.4.1 happy:
                    $(window).bind('hashchange',_onHashChange);
                }
            });

            // Set a global error handler for AJAX requests
            $('body').ajaxError(function(e,jqxhr,settings,err) {
                if (jqxhr.status === 0 || jqxhr.statusText === "abort") {
                    return; // don't show an error for aborted requests or requests that don't actually complete
                }
                var errorOptions = WWF.DEBUG?{persist:true,unique:'requestError-'+jqxhr.status}:{unique:'requestError-'+jqxhr.status};
                //Try to show a descent error message (parse response and get the message if possible)
                var parsedResponse, message;
                if(typeof jqxhr.responseText != 'undefined') {
                    try {
                        parsedResponse = $.parseJSON(jqxhr.responseText);
                    } catch(e){}
                    if ('object' == WWF.typeOf(parsedResponse) && 'string' == WWF.typeOf(parsedResponse.message)) {
                        message = parsedResponse.message;
                    }
                }

                //If we have specific error statuses, show the appropriate message
                if(!WWF.DEBUG && jqxhr.status==403)//accessDenied
                    WWF.error("You have been inactive for some time and you have to <a href='#' onclick='location.reload();return false;'>Log in</a> again.", errorOptions);
                else if(!WWF.DEBUG && 'string' == WWF.typeOf(message))
                    WWF.error("Request error:\n"+message, errorOptions);
                else //Default behavior
                    WWF.error("Request error\nError code: "+jqxhr.status+"\nMessage:\n"+jqxhr.responseText, errorOptions);
            });

            // Register the run() method of this page to DOM ready event
            $(function($){
                that.run();
            });

            return that;
        },

        /**
         * Extract Ids from an element's class name like .id-2345
         *
         * @param {mixed} raw or $-ified DOM element to extract id from
         * @return {mixed} the id as integer or null if none found
         */
        getIdFromClass: function(e) {
            var r=_reIdFromClass.exec($(e).attr('class'));

            return (r && r.length>1) ? r[1] : null;
        },

        /**
         * Extract the Id from the next parent with class .item
         *
         * Use case:
         *  <div class="item id-234">
         *      <div class="button-delete">...</div>
         *      <div class="button-update">...</div>
         *      ...
         *  </div>
         *
         *  All buttons can obtain their related Id with this method.
         *
         * @param {mixed} raw or $-ified DOM element to start at
         * @param {mixed} (optional) selector of the parent class. Defaults to '.item'
         * @return {mixed} the id as integer or null if none found
         */
        getIdFromParents: function(e,selector) {
            var $p=$(e).parents(selector ? selector : '.item');

            return $p.length ? WWF.getIdFromClass($p) : null;
        },

        /**
         * Add a parameter to given URL and either add '?' or '&'
         * @param {string} url  the URL to add the parameter to
         * @param {string} param the name of the URL param to add
         * @param {string} value the value to add. Will get URI encoded.
         * @return {string} the URL with parameter appended
         */
        addUrlParam: function(url,param,value) {
            return url + (url.indexOf('?')===-1 ? '?':'&') + param + '=' + encodeURIComponent(value);
        },

        /**
         * @return {object} CSRF data or an empty object. To be merged with POST data.
         */
        getCsrf: function() {
            if(WWF.csrfToken=='')
                return {};

            var c={};
            c[WWF.csrfTokenName]=WWF.csrfToken;
            return c;
        },
        /**
         * Fucntion to detect mobile
         */
        isMobile: function () {
            return $('.detect-mobile').is(':visible');
        },

        /**
         * Function to detect iPad
         */
        isIpad: function() {
        	return (navigator.userAgent.match(/iPad/i) != null);
        },

        /**
         * Function to detect touch devices
         */
        isTouch: function () {
            return 'ontouchstart' in window || "ontouchend" in document;
        },

        /**
         * Aliases for the Message Module functions.
         *
         * Use WWF.succes(), WWF.warning(), etc. to display success, warning
         * info and error messages.
         *
         * @param {string} the message to be displayed
         * @param {object} (optional) Messages Module config for this specific message
         */
        success: function(msg, config) {
            $.shout(msg, 'success', $.extend({header:'Success'}, config));
        },
        warning: function(msg, config) {
            $.shout(msg, 'warning', $.extend({header:'Warning'}, config));
        },
        info: function(msg, config) {
            $.shout(msg, 'info', $.extend({header:'Information'}, config));
        },
        error: function(msg, config) {
            $.shout(msg, 'error', $.extend({header:'Error'}, config));
        },


        validateForm: function(form, data, hasError) {
            if(!hasError)
                return true;

            jQuery.each(data, function(el, msg) {
                WWF.dom.error('#'+el, msg,{pos: 'left'});
            });


            return false;
        },

        /**
         * dom object for showing tips on specific page elements
         *
         * Usage and examples on our styleguide
         *
         * @param {string or $} element on which to show the tip
         * @param {string} message to show
         * @param {object} config options
         */
        dom:{
            simpletip:function (el,msg,conf) {
                _tip(el,msg,'simpletip',conf);
            },
            error:function (el,msg,conf) {
                _tip(el,msg,'error',conf);
            },
            info:function (el,msg,conf) {
                _tip(el,msg,'info',conf);
            },
            warning:function (el,msg,conf) {
                _tip(el,msg,'warning',conf);
            },
            success:function (el,msg,conf) {
                _tip(el,msg,'success',conf);
            },
            /**
             * help tips actually are different from the above, but for sake
             * of consistency in usage, we define them the same way
             */
            help:function (el,msg,conf) {
                var defaultConf = {
                    content : {
                        text: msg
                    },
                    position: {
                        my: 'left top',
                        at: 'right top'
                    },
                    show : {
                        event: 'mouseenter',
                        fixed: true
                    },
                    hide: {
                        fixed: true,
                        delay: 500
                    },
                    style: {
                        classes: 'help-state',
                        tip: {
                            corner: 'left top',
                            mimic: 'left center',
                            width: 10,
                            height: 12,
                            offset: -2,
                            border: 0
                        }
                    }
                };
                el.qtip('destroy').qtip($.extend({}, defaultConf, conf));
            }
        },
        /**
         * Strip backslashes from a string
         * @param  {string} text the string
         * @return {string} cleared of backslashes
         */
        stripSlashes: function(text) {
            // prep the regex to strip backslashes
            return text.replace(/[^\\][\\]/g, '');
        },

        /**
         * This is a "fixed" version of the typeof operator.  It differs from the typeof
         * operator in such a way that null returns 'null' and arrays return 'array'.
         * @param {*} value The value to get the type of.
         * @return {string} The name of the type.
         * @see http://closure-library.googlecode.com/svn/docs/closure_goog_base.js.source.html#line631
         */
        typeOf: function(value) {
            var s = typeof value;
            if (s == 'object') {
                if (value) {
                // Check these first, so we can avoid calling Object.prototype.toString if
                // possible.
                //
                // IE improperly marshals tyepof across execution contexts, but a
                // cross-context object will still return false for "instanceof Object".
                if (value instanceof Array) {
                    return 'array';
                } else if (value instanceof Object) {
                    return s;
                }

                // HACK: In order to use an Object prototype method on the arbitrary
                //   value, the compiler requires the value be cast to type Object,
                //   even though the ECMA spec explicitly allows it.
                var className = Object.prototype.toString.call(
                    /** @type {Object} */ (value));
                // In Firefox 3.6, attempting to access iframe window objects' length
                // property throws an NS_ERROR_FAILURE, so we need to special-case it
                // here.
                if (className == '[object Window]') {
                    return 'object';
                }

                // We cannot always use constructor == Array or instanceof Array because
                // different frames have different Array objects. In IE6, if the iframe
                // where the array was created is destroyed, the array loses its
                // prototype. Then dereferencing val.splice here throws an exception, so
                // we can't use goog.isFunction. Calling typeof directly returns 'unknown'
                // so that will work. In this case, this function will return false and
                // most array functions will still work because the array is still
                // array-like (supports length and []) even though it has lost its
                // prototype.
                // Mark Miller noticed that Object.prototype.toString
                // allows access to the unforgeable [[Class]] property.
                //  15.2.4.2 Object.prototype.toString ( )
                //  When the toString method is called, the following steps are taken:
                //      1. Get the [[Class]] property of this object.
                //      2. Compute a string value by concatenating the three strings
                //         "[object ", Result(1), and "]".
                //      3. Return Result(2).
                // and this behavior survives the destruction of the execution context.
                if ((className == '[object Array]' ||
                    // In IE all non value types are wrapped as objects across window
                    // boundaries (not iframe though) so we have to do object detection
                    // for this edge case
                    typeof value.length == 'number' &&
                    typeof value.splice != 'undefined' &&
                    typeof value.propertyIsEnumerable != 'undefined' &&
                    !value.propertyIsEnumerable('splice')

                    )) {
                    return 'array';
                }
                // HACK: There is still an array case that fails.
                //     function ArrayImpostor() {}
                //     ArrayImpostor.prototype = [];
                //     var impostor = new ArrayImpostor;
                // this can be fixed by getting rid of the fast path
                // (value instanceof Array) and solely relying on
                // (value && Object.prototype.toString.vall(value) === '[object Array]')
                // but that would require many more function calls and is not warranted
                // unless closure code is receiving objects from untrusted sources.

                // IE in cross-window calls does not correctly marshal the function type
                // (it appears just as an object) so we cannot use just typeof val ==
                // 'function'. However, if the object has a call property, it is a
                // function.
                if ((className == '[object Function]' ||
                    typeof value.call != 'undefined' &&
                    typeof value.propertyIsEnumerable != 'undefined' &&
                    !value.propertyIsEnumerable('call'))) {
                    return 'function';
                }


                } else {
                return 'null';
                }

            } else if (s == 'function' && typeof value.call == 'undefined') {
                // In Safari typeof nodeList returns 'function', and on Firefox
                // typeof behaves similarly for HTML{Applet,Embed,Object}Elements
                // and RegExps.  We would like to return object for those and we can
                // detect an invalid function by making sure that the function
                // object has a call method.
                return 'object';
            }
            return s;
        }

    }
    function _tip(el, msg, type, config) {
        var o = typeof el == 'string'?$(el):el;
        var options = {
            autoAppendClass: true,
            autoclose:false,
            classes: 'state-tip state '+type+'-state',
            pos:'right',
            qtipElement:false,
            useHook:true,
            closeOnFocus: false,
            zIndex: 800,
            tipContainer: false
        };
        $.extend(options,config);

	// if the element is inside a popup, increase the z-index to 1010
        // TODO: THIS IS JUST A TEMPORARY QUICKFIX. We need to review the z-index policy
        if (o.parents('.ui-dialog-content').length)
            options.zIndex = 1010;

        var qtipElement = options.qtipElement?options.qtipElement:el;

        if (typeof msg == 'undefined' || msg == 'close') {
            if (options.useHook)
                $(o.attr('data-tip-hook')).qtip('hide');
            else
                $(qtipElement).qtip('hide');

            o.removeClass(type);
            return;
        }

        if (options.autoAppendClass)
            o.removeClass('error warning info success').addClass(type);

        if (options.useHook)
            o.attr('data-tip-hook', typeof qtipElement=='string'?qtipElement:qtipElement.selector);

        var my, at, tip, css;
        switch (options.pos) {
            case 'top':
                my = 'bottom center';
                at = 'top center';
                tip = {
                };
                css = {
                    bottom:'-8px'
                };
                break;
            case 'bottom':
                my = 'top center';
                at = 'bottom center';
                tip = {
                };
                css = {
                    top:'-14px'
                };
                break;
            case 'right':
                my = 'left center';
                at = 'right center';
                tip = {
                };
                css = {
                };
                break;
            case 'left':
            default:
                my = 'right center';
                at = 'left center'
                tip = {
                };
                css = {
                };
        };

        var _content = '<span class="left icon '+(type=='warning'?'error':type)+' light"></span>'+'<div class="ui-tooltip-container">'+msg+'</div>';
        $.fn.qtip.zindex = options.zIndex;

        $(qtipElement).qtip('destroy').qtip({
            content: {
                text: _content
            },
            position : {
                my: my,
                at: at,
                container: options.tipContainer
            },
            style : {
                tip:$.extend({
                    width: 6,
                    height: 10
                }, tip),
                classes : options.classes
            },
            show : {
                event  : false,
                effect : function () {
                    $(this).fadeIn(500);
                    $('.ui-tooltip-tip').css(css);
                }
            },
            hide : {
                event  : false,
                effect : function () {
                    $(this).fadeOut(500);
                }
            }
        }).qtip('show');
        if (options.autoclose)
            setTimeout(function () {
                $(qtipElement).qtip('hide');
            }, options.autoclose);

        if (options.closeOnFocus && !options.autoclose) {
            $(qtipElement).one('focus mouseup', function(){
                if (options.useHook)
                    $(o.attr('data-tip-hook')).qtip('hide');
                else
                    $(qtipElement).qtip('hide');

                o.removeClass(type);
            });
        }
    }

    return wwf;
})(jQuery);
