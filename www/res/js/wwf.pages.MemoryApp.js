/**
 * This is the recipe page
 */
WWF.pages.MemoryApp=function(config) {
    //Default Page configuration.
    var _c={
        // Modules
        modules:{
            facepile : {}
        },
        //Actions
        actions                 : {
            archive: {
                url: '/stream/archive/'
            }
        }
    };

    // Use functional inheritance to extend WWF.Page.
    var that=WWF.Page($.extend(true,{},_c,config));

    // Prepare a shortcut to the config object
    var c=that.config,
        filters = {};

    /**
     * Setup photoapp submission form
     */
    var _setupForm = function()
    {

    }

    /**
     * REQUIRED: This is the mandatory init() method.
     */
    that.init=function()
    {
        // Setup photoapp submission form
        _setupForm();
    }

    return that;
}