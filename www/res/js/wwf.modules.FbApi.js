/**
 * FbApi
 *
 * This module provides the Facebook Javascript API on the clientside.
 *
 * To load the FB SDK files asynchronously, WWF.modules.FbApi.load(cb) can be called.
 * The callback 'cb' will get called after the SDK was loaded, or immediately, if it
 * already was loaded.
 *
 * The  module can also auto-login a user if he is logged in to Facebook and has
 * connected (==integrated) his FB account with us. The user will not be auto-logged
 * in, if he logged out during the current browser session. A cookie is used to flag
 * that a manual logout has happened.
 */
WWF.modules.FbApi=function(config){

    // Default configuration
    var _c={
        externalId:     null,
        appId:          null,
        namespace:      null,
        redirectUrl:    null,
        insideFacebook: false,
        apiUrl:         '//connect.facebook.net/en_US/all.js',
    };

    // REQUIRED: Use functional inheritance to extend WWF.Module.
    var that=WWF.Module($.extend({},_c,config)),
    c=that.config,
    _fbApi,
    loadStarted = false,
    logUrl= "/social/log",
    logData = []
    ;

    /**
     * Check if user is logged in and has authenticated our app
     * Get a response and decide to call callback function or not
     * States can be:
     *     connected: the user is logged into Facebook and has authenticated your application
     *     not_authorized: the user is logged into Facebook but has not authenticated your application
     *     unknown: the user is not logged into Facebook at this time and so we don't know if they've authenticated your application or not
     *
     * @param function cb the function to call with response as argument
     */
    _authRequest=function(cb)
    {
        that.load(function() {
            FB.getLoginStatus(function(response) {
                if(typeof response.error != 'undefined')
                    that.instantLog('error',response.error.message,'FbApi.authRequest');
                else {
                    // the user is logged in and has authenticated our app
                    if (response.status === 'connected') {
                        if (c.externalId === null || response.authResponse.userID == c.externalId) {
                            if(typeof(cb)==='function')
                                cb(response);
                        } else {
                            // the user is logged in to Facebook with a
                            // different account than the integrated
                            that.instantLog('error','The user is logged in to Facebook using a different account than the integrated','FbApi.authRequest');
                        }
                    } else if (response.status === 'not_authorized') {
                        // the user is logged in to Facebook,
                        // but has not authenticated your app
                        that.instantLog('error','The user is logged in to Facebook but has not authenticated our app','FbApi.authRequest');
                    } else {
                        // the user isn't logged in to Facebook
                        that.instantLog('error','The user isn\'t logged in to Facebook','FbApi.authRequest');
                    }
                }
            });
        });
    }

    /**
     * Load FB API
     * @param function cb the function to call after SDK was loaded. Also works, if already loaded
     */
    that.load=function(cb)
    {
        if(window.FB===undefined){
            if(!loadStarted) {
                loadStarted = true;
                $.getScript(c.apiUrl).done(function() {
                    FB.init({
                        appId:  c.appId,
                        status: true,       // check login status
                        cookie: true,       // enable cookies to allow server to access session
                        xfbml:  true,       // parse XFBML on page
                        oauth:  true        // enable OAuth 2.0
                    });

                    if(typeof(cb)==='function')
                        cb();

                    loadStarted = false;

                });
            } else {
                var myInterval = setInterval(function(){
                    if(loadStarted===false){
                        clearInterval(myInterval);
                        if(typeof(cb)==='function')
                            cb();
                    }
                },300);
            }
        } else if(typeof(cb)==='function')
            cb();
    }

    /**
     * Check if user is logged in and provide an access token
     * @param function cb the function to call with access token as argument or null if user is not logged in
     */
    that.checkLoginStatus=function(cb)
    {
        that.load(function() {
            window.FB.getLoginStatus(function(r) {
                if (cb!==undefined)
                    cb( (r.status==='connected') ? r.authResponse.accessToken : null)
            });
        });
    }

    /**
     * REQUIRED: This is the mandatory init() method.
     */
    that.init=function()
    {
        that.load();

        if(c.insideFacebook){
            if (window.location==top.location)
                window.location='?clearInsideFacebook=1';
        }

        $('[data-facebook-ui]').on('click',function(e){
            var $this= $(this);
            var params = {};
            if(typeof $this.data('facebook-params') != 'undefined')
                params = $this.data('facebook-params');

            that.ui($this.data('facebook-ui'),params);
        });
    }

    /**
     * Sent requests to Facebook API signed with the access token if needed
     */
    that.request=function(apiUrl, method, options, callback)
    {
        if(typeof callback == 'undefined' || callback === null)
            callback = function(response){
                if(typeof response.error != 'undefined') {
                    that.addLog('request', {
                        result: false,
                        message: response.error.message
                    },
                    'FbApi.request');
                }
                that.triggerError();
            };

        _authRequest(function(response){
            if(response)
                window.FB.api(apiUrl, method, options, callback);
        });
    }

    /**
     * Sent requests to Facebook API signed with the access token if needed
     */
    that.ui=function(method, params, callback)
    {
        if(typeof callback == 'undefined' || callback === null)
            callback = function(response){
                if(typeof response.error != 'undefined') {
                    that.addLog('request', {
                        result: false,
                        message: response.error.message
                    },
                    'FbApi.ui');
                }
            };

        _authRequest(function(response){
            if(response){
                params = $.extend({
                    method: method,
                    app_id: c.appId,
                    display: 'popup'
                },params);

                if(response.status=='connected' )
                    params.access_token = response.authResponse.accessToken;

                if(c.insideFacebook &&  c.redirectUrl !== null){
		    params.display = 'async';
                    params.redirect_uri = c.redirectUrl;
		}

                window.FB.ui(params, callback);
            }
        });
    }

    /**
     * Set log details to use it when the log is triggered
     **/
    that.addLog=function(type, data, trace, clear)
    {
        //console.log('addLog: ' + type + ' - trace: '+ trace + ' - result: '+ (typeof data.result != 'undefined' ? data.result : 'undefined' )+ (typeof data.message != 'undefined' ? ' - message: '+data.message : '' ));

        if($.inArray(type, ["error","warning", "info","success","action","request"]) == -1)
            throw("Log type '"+type+"'not valid");

        if(typeof clear != 'undefined' && clear)
            that.clearLog();

        if(typeof ref == 'undefined')
            ref = 'unknown';

        if(typeof data != 'object')
            data = {
                message: data
            };

        logData.push($.extend(data, {
            type: type,
            trace: trace
        }));

        return true;
    }

    /**
     * Clear log details if obsolete
     **/
    that.clearLog=function()
    {
        logData = [];
    }

    /**
     * Sent logging request to server
     **/
    that.triggerLog=function()
    {
        if(!that.hasLogs())
            return true;
        else {
            $.ajax({
                url: logUrl,
                data: $.extend(WWF.getCsrf(),{
                    data: logData
                }),
                type: 'POST',
                success: function(response) {
                    that.clearLog();
                    if(typeof response.result == 'undefined')
                        throw 'Result variable in response not defined';

                    if(typeof response.message != 'undefined')
                        if((typeof response.type == 'undefined' && response.result) || (typeof response.type != 'undefined' && response.type == 'success'))
                            console("success: "+response.message);
                        else if((typeof response.type == 'undefined' && !response.result) || (typeof response.type != 'undefined' && response.type == 'error'))
                            console("error: "+response.message);
                        else if(response.type == 'warning')
                            console("warning: "+response.message);
                        else if(response.type == 'info')
                            console("info: "+response.message);
                        else
                            throw 'Message and result variables don\'t make sense';

                },
                error: function(xhr) {
                    that.clearLog();
                    throw('Error occured while senting log to server');
                }
            });
        }
    }

    /**
     * Set details and request to server in one step. In common cases the previous
     * log details are obsolete but if needed they can be reset.
     **/
    that.instantLog=function(type, data, ref, clear)
    {
        if(typeof ref == 'undefined')
            ref = 'ExtRequester.instantLog';

        that.addLog(type, data, ref, clear);
        that.triggerLog();
    }

    /**
     * If there is an error in response, do a request to write it in server logs
     * @param response object from Facebook or just a string message
     * @return error object with result and message
     */
    that.hasLogs=function(type)
    {
        if(typeof type == 'undefined')
            return logData.length > 0;
        else {
            var result = false;
            $.each(logData, function(i, data){
                if(data.type == type)
                    result = true;
            });
            return result;
        }
    }

    that.googleTrackAction=function(category, action, label, value, noninteraction)
    {
        if(typeof WWF.page.modules.GoogleTracking !== 'undefined'){
            WWF.page.modules.GoogleTracking.trackEvent(category, action, label, value, noninteraction);
            that.instantLog('info', "GoogleTracking - category: "+category+" - action: "+ action +" - label: "+ label +" - value: " + value, "ExtRequester.googleTrackAction", true);
        } else {
            that.instantLog('error', "GoogleTracking was not loaded", "ExtRequester.googleTrackAction", true);
        }
    }

    return that;
};

/* vim: set syntax=javascript: */
