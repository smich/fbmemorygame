jQuery(document).ready(function($) {

    //bind jQ UI Dialog for footer links that have .dialog class 
    $('.footer-menu a.dialog').click(function(e){
        e.preventDefault();
        var el = $($(this).data('content-selector'));

        el.dialog({
            title: $(this).attr('title'),
            modal: true,
            draggable: false,
            minHeight: 450,
            width: 'auto',
            resizable: false,
            position: { my: "center top", at: "center top", of: top }
        });
    });

});