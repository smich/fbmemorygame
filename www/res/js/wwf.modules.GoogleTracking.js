/**
 * GoogleTracking
 *
 * This module provides the Google Javascript API on the clientside.
 *
 * To load the Google js asynchronously, WWF.modules.GoogleTracking.load(cb) can be called.
 * The callback 'cb' will get called after the SDK was loaded, or immediately, if it
 * already was loaded.
 *
 */
WWF.modules.GoogleTracking=function(config){

    // Default configuration
    var _c={
        apiUrl: '//google-analytics.com/ga.js',
        googleAnalyticsQueue: []
    };

    // REQUIRED: Use functional inheritance to extend WWF.Module.
    var that=WWF.Module($.extend({},_c,config)),
    c=that.config;

    /**
     * Load Google API
     * @param function cb the function to call after Google was loaded. Also works, if already loaded
     */
    that.isLoaded=function()
    {
        return (_gaq!==undefined);
    }

    /**
     * REQUIRED: This is the mandatory init() method.
     */
    that.init=function()
    {
    }

    that.trackEvent = function(category, action, label, value, noninteraction){
        if(typeof label == 'undefined')
            label = null;
        if(typeof value == 'undefined')
            value = null;
        if(typeof noninteraction == 'undefined')
            noninteraction = null;

        if(that.isLoaded()){
            return _gaq.push([
                '_trackEvent',
                category,
                action,
                label,
                value,
                noninteraction
                ]);
        }

    }

    return that;
};

/* vim: set syntax=javascript: */
