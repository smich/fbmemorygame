<?php

class AdminModule extends CWebModule
{
	/**
     * @var string default layout for all controllers in this module
     */
    public $layout='column1';

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
            'admin.models.*',
            'admin.components.*',
            'admin.components.widgets.*',
        ));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// Yii::app()->user should become adminUser in this module.
            // Frontend user is accessible through Yii::app()->frontendUser.
            Yii::app()->setComponent('frontendUser',Yii::app()->user);
            Yii::app()->setComponent('user',Yii::app()->adminUser);
            return true;
		}
		else
			return false;
	}
}
