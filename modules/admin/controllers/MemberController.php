<?php
class MemberController extends AdminController
{
    public $layout='column1';

    public $defaultAction = 'index';
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                // 'users' => array('@'),
                'actions'   => array('index','update'),
                'roles'     => array(Users::USER_SUPERADMIN, Users::USER_ADMIN),
            ),
            array('allow',
                'actions'   => array('delete'),
                'roles'     => array('superadmin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model=new Member();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Member']))
            $model->attributes=$_GET['Member'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $view = 'update';

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Member']))
        {
            $model->attributes=$_POST['Member'];

            if($model->save())
                $this->redirect('index');
        }

        $this->render($view,array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a user from the DB and all associated data, that is recipes, messages and steps of the StepMachine.
     *
     * @param integer $id the ID of the member to be deleted
     */
    public function actionDelete($id)
    {
        $member = $this->loadModel($id);

        // Erase all data
        Recipe::model()->deleteAllByAttributes(array('mem_id'=>$id));
        PhotoApp::model()->deleteAllByAttributes(array('mem_id'=>$id));
        StepMachine::model()->deleteAllByAttributes(array('memId'=>$id));
        $member->delete();
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=Member::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested member does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='stream-system-message-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
