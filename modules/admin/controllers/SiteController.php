<?php

class SiteController extends AdminController
{
    public $pageDescription;
    public $pageKeywords;
    public $defaultPageTitle = 'Add the page title here...';
    public $defaultMetaDescription = "Add the page meta description here...";
    public $defaultMetaKeywords = "Add keywords here";

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'users' => array('@'),
                'actions'=>array('index','logout')
            ),
            array('allow',
                'users' => array('*'),
                'actions'=>array('login')
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->pageTitle = $this->defaultPageTitle;
        $this->pageDescription = $this->defaultMetaDescription;
        $this->pageKeywords = $this->defaultMetaKeywords;

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        // Hide the main menu in the login page
        $this->showMainMenu = false;

        $model = new AdminLoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['AdminLoginForm']))
        {
            $model->attributes=$_POST['AdminLoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                // $this->redirect(Yii::app()->user->returnUrl);
                $this->redirect('/adminSite');
        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        // logout without destroying the session, we just unset anything related to
        // the backend user, using the keyStatePrefix as set in our configuration
        Yii::app()->user->logout(false);
        // $this->redirect(Yii::app()->homeUrl);
        $this->redirect('/adminSite/site/login');
    }
}
