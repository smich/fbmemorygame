<?php
class RecipeController extends AdminController
{
    public $layout='column1';

    public $defaultAction = 'index';
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                // 'users' => array('@'),
                'actions'   => array('index','update'),
                'roles'     => array(Users::USER_SUPERADMIN, Users::USER_ADMIN,Users::USER_RECIPES_MODERATOR),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model=new Recipe();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Recipe']))
            $model->attributes=$_GET['Recipe'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $view = 'update';

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Recipe']) && isset($_POST['Ingredients']))
        {
            $model->attributes=$_POST['Recipe'];
            $model->ingredientItems = array();
            foreach($_POST['Ingredients'] as $ingredient) {
                if(empty($ingredient)) {
                    $model->addError('ingredients','Συμπληρώστε το κενό συστατικό');
                    break;
                }
                $model->ingredientItems[] = $ingredient;
            }

            if($model->validate())
            {
                // Save the message and display a proper notification to the user
                if($model->save()) {
                    $this->redirect('index');
                }
            }
        }

        $this->render($view,array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=Recipe::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested member does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='recipe-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
