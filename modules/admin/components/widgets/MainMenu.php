<?php
Yii::import('zii.widgets.CMenu');
/**
 * MainMenu
 *
 * Extends CMenu so that it renders the logo + header menu on top of each page.
 * Menu items are hardcoded in this class.
 */
class MainMenu extends CMenu
{
    /**
     * @var array hardcoded CSS class
     */
    public $htmlOptions=array();

    /**
     * @var bool do not HTML encode labels
     */
    public $encodeLabel=false;

    /**
     * @var string default template for every menu item
     */
    public $itemTemplate='{menu}';

    /**
     * @var mixed add the active lable to the parents by default
     */
    public $activateParents=true;

    /**
     * Override CMenu::init() to set menu items and id
     */
    public function init()
    {
        $user = Yii::app()->user;
        $controllerId = $this->controller->id;

        // Define main items first. We use string indexes to access menu items below.
        $this->items = array(
            'member'=>array(
                'label'         => "Χρήστες",
                'url'           => "/adminSite/member/index",
                'active'        => in_array($controllerId,array('member')),
                'linkOptions'   => array(),
                'visible'       => ($user->checkAccess(Users::USER_SUPERADMIN) || $user->checkAccess(Users::USER_ADMIN))
            ),
            'photoApp'=>array(
                'label'         => "Μηνύματα",
                'url'           => "/adminSite/photoApp/index",
                'active'        => in_array($controllerId,array('photoApp')),
                'linkOptions'   => array(),
                'visible'       => ($user->checkAccess(Users::USER_SUPERADMIN) || $user->checkAccess(Users::USER_ADMIN) || $user->checkAccess(Users::USER_MESSAGES_MODERATOR))
            ),
            'recipe'=>array(
                'label'         => "Συνταγές",
                'url'           => "/adminSite/recipe/index",
                'active'        => in_array($controllerId,array('recipe')),
                'linkOptions'   => array(),
                'visible'       => ($user->checkAccess(Users::USER_SUPERADMIN) || $user->checkAccess(Users::USER_ADMIN) || $user->checkAccess(Users::USER_RECIPES_MODERATOR))
            ),
            'logout'=>array(
                'label'         => "Έξοδος",
                'url'           => "/adminSite/site/logout",
            ),
        );
        $this->lastItemCssClass = 'pLogout';

        parent::init();
    }

    /**
     * Override CMenu::run() to decorate the rendered menu some divs + top logo
     */
    public function run()
    {
        echo '<div class="container">'.
             '<div class="headercon clearfix">'.
             CHtml::link('<img class="logo" src="/res/img/logo.png" width="100">',array('/adminSite/site/index')).
             '<span class="right">Γειά σου, '.Yii::app()->user->name.'</span>'.
             '<div id="mainMenu" class="clearfix">';
        parent::run();
        echo '</div></div></div>';
    }

    /**
     * Override CMenu::renderMenuItem() to render the clean separators
     */
    protected function renderMenuItem($item)
    {
        if (!isset($item['url']))
            return $item['label'];

        $options=isset($item['linkOptions']) ? $item['linkOptions'] : array();
        return CHtml::link($item['label'],$item['url'],$options);
    }
}
