<?php

class Users
{
	// User types
	const USER_SUPERADMIN         = 'superadmin';
    const USER_ADMIN        	  = 'admin';
    const USER_RECIPES_MODERATOR  = 'recipes-moderator';
    const USER_MESSAGES_MODERATOR = 'messages-moderator';

	const USERS_FILE_PATH  = 'data/accounts';

	// --------------------------------
	// PROTECTED FUNCTIONS
	// --------------------------------

	/**
	 * Load user accounts from the file
	 *
	 * @return Array User account details
	 */
	protected static function loadFromFile()
	{
		return CJSON::decode(file_get_contents(dirname(__FILE__).'/../'.self::USERS_FILE_PATH));
	}

	// --------------------------------
	// PUBLIC FUNCTIONS
	// --------------------------------

	/**
	 * Traverse the list of user accounts and get the requested user based
	 * on the supplied user name. A user object has the following information:
	 * {
	 *     user_id   :'',
	 *     user_name :'',
	 *     password  :'',
	 *     roles     :''
	 * }
	 *
	 * @param  mixed $id User id in case of integer, username in case of string
	 * @return StdClass An object that represents the requested user
	 */
	public static function getUser($id)
	{
		// Determine in which field we should search based on the type of id
		$field = (is_numeric($id))?'user_id':'user_name';
		// Load all user accounts
		$adminUsers = self::loadFromFile();

		// Find the requested user
		$user = null;
		foreach($adminUsers as $index=>$aUser) {
            if($aUser[$field]==$id) {
                $user = (object)$aUser;
                break;
            }
        }
        return $user;
	}

	/**
	 * Verify if the provided password matches the one defined by the user
	 *
	 * @param  stdClass $user     The reference to the user object
	 * @param  String   $password The provided password
	 * @return boolean  true if the provided password matches the one defined by the user
	 */
	public static function verifyPassword($user, $password)
	{
		return $user->password==$password;
	}
}