<?php
/**
 * Custom AdminWebuser class
 *
 * Extends  CWebuser to handle legacy authentication and ease the connection
 * betweeen AdminWebuser - DB user.
 */
class AdminWebUser extends CWebUser
{
    // pointer to User model
    private $_model=false;

    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params=array())
    {
        if (empty($this->id)) {
            // Not identified => no rights
            return false;
        }
        // Get the user role
        $role = $this->getState("roles");

        // SUPERadmin role has access to everything
        if ($role === Users::USER_SUPERADMIN) {
            return true;
        }

        // allow access if the operation request is the current user's role
        return ($operation === $role);
    }

    /**
     * Override CWebUser::login() to store the related Users object on login.
     * UserIdentity must be able to provide such a member object through getMember().
     *
     * It also returns the login status (i.e. false if login was prevented due to fraud)
     *
     * @param UserIdentity $identity the authenticated UserIdentity
     * @param int $duration lifetime in seconds for the auto login cookie
     * @return bool wether login was successful
     */
    public function login($identity,$duration=0)
    {
        if(($user=$identity->getUser())===null)
            return false;

        // Make Users object available in beforeLogin/afterLogin
        $this->setModel($user);
        parent::login($identity,$duration);
        if(!$this->getIsGuest())
            return true;

        // Remove Users object if login was not successful (e.g. fraud)
        $this->setModel(null);
        return false;
    }

    /**
     * Returns the connected Users record for the current logged in user.
     *
     * @return mixed the associated record that is connected to the logged in user or null for guests
     */
    public function getModel()
    {
        if ($this->_model===false)
        {
            if (((int)$id=$this->getId())===null)    // not logged in
                return $this->_model=null;

            $this->_model = Users::getUser($id);
            CVarDumper::dump($this->_model,10,true);
        }

        return $this->_model;
    }

    /**
     * Returns the connected Users record for the current logged in user.
     *
     * @return mixed the associated record that is connected to the logged in user or null for guests
     */
    public function getMember()
    {
        return $this->getModel();
    }

    /**
     * Override Webuser::getId() to use legacy session var
     *
     * @return mixed id of logged in user, null if guest
     */
    public function getId()
    {
        return Yii::app()->session->get('uid');
    }

    /**
     * Override WebUser::getName() to use legacy session var
     *
     * @return mixed username of logged in user, null if guest
     */
    public function getName()
    {
        return Yii::app()->session->get('name');
    }

    /**
     * @param Users (Administrators|Accounts|Managers|Users) the associated DB record that should be connected to the logged in user
     */
    public function setModel($user)
    {
        $this->_model=$user;
    }

    /**
     * @return bool wether the current visitor is logged in to seagrave
     */
    public function getLoggedInAsAdmin()
    {
        return Yii::app()->session->get('uid')!==null;
    }

    /**
     * This method is called after each successful login. We use it to set additional
     * session data for the legacy app and to update the login time.
     *
     * @param boolean $fromCookie wether login is based on cookie
     */
    protected function afterLogin($fromCookie)
    {
        $user = $this->getModel();

        $session=Yii::app()->session;
        $session->add('uid',(int)$user->user_id);    // IMPORTANT: Indicates admin has logged in
        $session->add('name',$user->user_name);
    }
}
