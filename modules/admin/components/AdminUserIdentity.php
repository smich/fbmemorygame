<?php
/**
 * AdminUserIdentity
 *
 * This class represents an admin user identiy and can authenticate a User.
 * After successful authencition or when the identity was created through
 * createAuthenticated(), getUser() will return the related Users object.
 */
class AdminUserIdentity extends CUserIdentity
{
    /**
     * @var Users reference to logged user
     */
    public $_user;

    /**
     * @var integer user's id
     */
    public $_id;

    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        // Try to fetch the user based on the supplied username
        $user = Users::getUser($this->username);

        if ($user===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        elseif (!Users::verifyPassword($user,$this->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else {
            $this->_user    = $user;
            $this->_id      = (int)$user->user_id;
            $this->setState('roles', $user->roles);
            $this->errorCode= self::ERROR_NONE;
            Yii::app()->user->setModel($user);
        }
        YII_DEBUG && Yii::trace(
            'Authentication '.($this->errorCode===self::ERROR_NONE?'true':$this->errorCode),
            'app.auth.adminuseridentity'
        );

        return $this->errorCode===self::ERROR_NONE;
    }

    /**
     *
     * @return Users model of authenticated user or null if not authenticated
     */
    public function getUser(){
        return $this->_user;
    }
}
