<?php
/**
 * This is the base class for all controllers in the admin module
 */
class AdminController extends Controller
{
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();

    /**
     * @var mixed used to disable the main menu in some pages like the login page without
     * using a new layout
     */
    public $showMainMenu = true;

    public $layout;

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                // Allow authenticated users
                'users'=>array('*'),
            ),
            // Deny everyone else
            array('deny'),
        );
    }

    /**
    * @return array action filters
    */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }
}
