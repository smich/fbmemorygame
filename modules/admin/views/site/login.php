<?php
$this->layout='column1';

$this->pageTitle=Yii::app()->name . ' - Σύνδεση';
$this->breadcrumbs=array(
    'Σύνδεση',
);
?>

<div id="pLoginForm" class="span-24">
    <div class="logo"><img src="/res/img/logo.png" width="200"></div>
    <div id="pLoginFormBox">
        <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'login-form',
            // 'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        )); ?>

            <div class="row">
                <?php echo $form->labelEx($model,'username'); ?>
                <?php echo $form->textField($model,'username'); ?>
                <?php echo $form->error($model,'username'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'password'); ?>
                <?php echo $form->passwordField($model,'password'); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>

            <div class="row rememberMe">
                <?php echo $form->checkBox($model,'rememberMe'); ?>
                <?php echo $form->label($model,'rememberMe', array('class'=>'rememberMe-label')); ?>
                <?php echo $form->error($model,'rememberMe'); ?>
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton('Σύνδεση'); ?>
            </div>

        <?php $this->endWidget(); ?>
        </div><!-- form -->

    </div>
</div>