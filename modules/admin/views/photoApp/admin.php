<?php
Yii::app()->clientScript->registerScript('search', "

");
?>
<h1>Διαχείριση μηνυμάτων</h1>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'photoApp-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
		'mem_id',
        array(
            'name' => 'message',
            'value' => '$data->message',
            'htmlOptions'=>array('width'=>'500', 'style' => ''),
        ),
        array(
            'name' => 'status',
            'value' => '$data->statusLabel',
            'htmlOptions'=>array('style' => ''),
        ),
        array(
            'name' => 'create_dt',
            'value' => '$data->create_dt',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
        array(
            'name' => 'update_dt',
            'value' => '$data->update_dt',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
		array(
            'class'=>'CButtonColumn',
            'template'=>'{update}',
        ),
    ),
)); ?>
