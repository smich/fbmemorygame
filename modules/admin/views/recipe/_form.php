<div class="form recipe-form-container backend">

<?php
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'recipe-form',
		'enableAjaxValidation'=>false,
	));

?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title'); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ingredients'); ?>
		<?php
			if(count($model->ingredientItems))
			{
				foreach($model->ingredientItems as $key=>$value)
				{
					echo '<div class="ingredient-row">';
					echo '<input class="ingredient" type="text" value="'.$value.'" name="Ingredients[ingredient'.($key+1).']" id="Ingredients_ingredient'.($key+1).'">';
					if($key!='ingredient1') {
						echo '<span><a class="remove-ingredient" href="#">&nbsp;</a></span>';
					}
					echo '</div>';
				}
			}
			else {
				echo '<div class="ingredient-row">';
				echo '<input class="ingredient" type="text" value="" name="Ingredients[ingredient1]" id="Ingredients_ingredient1">';
				echo '</div>';
			}
		?>
		<?php echo CHtml::link(Yii::t('views-recipe','+Πρόσθεσε άλλο ένα'),'#',array('class'=>'add-ingredient')); ?>
		<?php echo $form->textArea($model,'ingredients', array('cols'=>50, 'rows'=>5, 'style'=>'display:none;')); ?>
		<?php echo $form->error($model,'ingredients'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description', array('cols'=>50, 'rows'=>5)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo CHtml::activeDropDownList($model,'status',Recipe::optsStatus()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rating_a'); ?>
		<?php echo CHtml::activeDropDownList($model,'rating_a',Recipe::optsRating()); ?>
		<?php echo $form->error($model,'rating_a'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rating_b'); ?>
		<?php echo CHtml::activeDropDownList($model,'rating_b',Recipe::optsRating()); ?>
		<?php echo $form->error($model,'rating_b'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');
		echo CHtml::link('Cancel',array('/adminSite/recipe/index'),array('class'=>'cancel-link'));?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
