<?php
Yii::app()->clientScript->registerScript('search', "

");
?>
<h1>Ενημέρωση συνταγών</h1>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'recipe-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
		'mem_id',
        array(
            'name' => 'title',
            'value' => '$data->title',
            'htmlOptions'=>array('style' => 'width: 150px;'),
        ),
        'ingredients',
        array(
            'name' => 'description',
            'value' => '$data->description',
            'htmlOptions'=>array('style' => 'width: 200px;'),
        ),
        array(
            'name' => 'status',
            'value' => '$data->statusLabel',
            'htmlOptions'=>array('style' => ''),
        ),
        array(
            'name' => 'rating_a',
            'value' => '$data->rating_a',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
        array(
            'name' => 'rating_b',
            'value' => '$data->rating_b',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
        array(
            'name' => 'create_dt',
            'value' => '$data->create_dt',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
        array(
            'name' => 'update_dt',
            'value' => '$data->update_dt',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
		array(
            'class'=>'CButtonColumn',
            'template'=>'{update}',
        ),
    ),
)); ?>
