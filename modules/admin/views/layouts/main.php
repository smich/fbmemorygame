<?php
/**
 * Main layout file for blueprint pages
 */
$cs=Yii::app()->clientScript;
$cs->registerCoreScript('jquery');
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" content="noindex" />
    <link rel="stylesheet" href="/res/css/screen.css" type="text/css" media="screen, projection" />
    <!--[if lt IE 8]><link rel="stylesheet" href="/res/css/ie.css" type="text/css" media="screen, projection" /><![endif]-->
    <link rel="stylesheet" href="/res/css/adminSite.css" type="text/css" media="screen, projection" />
    <title><?php echo $this->getPageTitle() ?></title>
</head>
<body>
    <?php
    	if($this->showMainMenu) {
    		$this->widget('MainMenu');
    	}
	?>

    <?php echo $content ?>
</body>
</html>
