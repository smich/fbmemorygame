<?php
// This view path is required to make forum submodule views work:
$this->beginContent('//../modules/admin/views/layouts/main');

?>
<div class="container">
    <div id="content">
        <?php echo $content; ?>
    </div><!-- content -->
</div>
<?php $this->endContent(); ?>
