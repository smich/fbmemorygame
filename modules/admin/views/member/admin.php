<?php
Yii::app()->clientScript->registerScript('search', "

");
?>
<h1>Διαχείριση χρηστών</h1>
<?php
// Display the delete button only to the superadmin
$buttons = (Yii::app()->user->checkAccess(Users::USER_SUPERADMIN)) ? '{delete}{update}' : '{update}';

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'member-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'photo_url',
            'type'=>'html',
            'value' => 'CHtml::image($data->photo_url, $data->fname, array("width"=>50))',
            'htmlOptions'=>array('width'=>'50px', 'style' => 'text-align: center'),
        ),
        array(
            'name' => 'id',
            'htmlOptions'=>array('width'=>'50px', 'style' => 'text-align: center'),
        ),
        'fname',
        'lname',
        'email',
        array(
            'name' => 'active',
            'value' => '$data->active?"Ναι":"Όχι"',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
        array(
            'name' => 'disclose_name',
            'value' => '$data->disclose_name?"Ναι":"Όχι"',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
        array(
            'name' => 'external_id',
            'value' => '$data->external_id?"Ναι":"Όχι"',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
        array(
            'name' => 'create_dt',
            'value' => '$data->create_dt',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
        array(
            'name' => 'update_dt',
            'value' => '$data->update_dt',
            'htmlOptions'=>array('style' => 'text-align: center'),
        ),
        /* 'link'=>array(
          'header'=> '',
          'type'  => 'raw',
          'value' => 'CHtml::button("button label",array("onclick"=>"document.location.href=\'".Yii::app()->controller->createUrl("controller/action",array("id"=>$data->id))."\'"))',
          ), */
        array(
            'class' => 'CButtonColumn',
            'template' => $buttons,
        ),
    ),
));
?>
