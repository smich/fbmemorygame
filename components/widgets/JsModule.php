<?php
/**
 * JsModule
 *
 * Abstract Base class for all Javascript modules in www/res/js/.
 *
 * This widget registers all required Javascript files for a WWF JS Module,
 * including the core Javascript files. A JsModule must have an associated
 * JS module file in www/res/js/wwf.modules.<moduleName>.js. It must override
 * $moduleName and set it to <moduleName>.
 */
abstract class JsModule extends CWidget
{
    /**
     * @var string the name of the Js module. JsModules must override this property.
     */
    protected $moduleName;

    /**
     * @var array Configuration options for the Js module on client side
     */
    public $options=array();

    /**
     * @var array Configuration options for the JsModule on server side (multiInstance)
     */
    public $instanceOptions=array();

    /**
     * Register script files
     */
    public function init()
    {
        $this->registerClientScripts();
    }

    /**
     * Register the Js files that this Module requires
     */
    protected function registerClientScripts()
    {
        if($this->moduleName!==null) {
            Yii::app()->clientScript->registerJsModule($this->moduleName,$this->options);
        }
    }

}
