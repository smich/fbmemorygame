<?php
/**
 * JsModuleExample
 *
 * This is a demo module for the WWF Js framework.
 *
 * NOTE: While this page also shows how to integrate with Yii widgets like
 * CGridView, in some cases the overhead of these widgets might not be apropriate.
 */
class JsModuleExample extends JsModule
{
    public $id='examplegrid';

    /**
     * @var mixed the name of the action
     */
    public $action;

    /**
     * @var string the name of this Js module
     */
    protected $moduleName='Example';

    /**
     * This is the place where we do optional updates to the module options.
     * If nothing needs to be done, this method is not required.
     */
    public function init()
    {
        // Send the grid id to the Js module
        $this->options['gridId']=$this->id;
        parent::init();
    }

    /**
     * The run() method is only required, if the module has to render some output.
     *
     * In our example here we implement a simple mechanism to also let the widget
     * handle different AJAX actions from the according Javascript module.
     */
    public function run()
    {
        switch($this->action)
        {
            case 'ajaxView':
                $this->ajaxView();
                break;

            default:
                $this->render('jsModuleExample',array(
                    'provider'=>$this->getExampleProvider(),
                ));
        }
    }

    public function ajaxView()
    {
        $id=(int)$_GET['id'];
        if(($item=$this->getItem($id))===null)
            throw new CHttpException(404,'Item not found!');

        echo json_encode(array(
            'item'=>$item,
            'html'=>$this->render('jsModuleExampleView',array(
                'item'=>$item,
            ),true),
        ));
    }


    /**
     * @return CArrayDataProvider with static example data
     */
    private function getExampleProvider()
    {
        return new CArrayDataProvider($this->getExampleData(),array(
            'id'=>'exampledata',
        ));
    }

    /**
     * @return array list of static example data
     */
    private function getExampleData()
    {
        $data=array();
        foreach(range('A','Z') as $i=>$l)
            $data[]=array(
                'id'    =>$i+1,
                'name'  =>"User $l",
                'email' =>"$l@example.com",
                'hash'  =>md5($l),
            );
        return $data;
    }

    private function getItem($id)
    {
        foreach($this->getExampleData() as $data)
            if($data['id']==$id)
                return $data;
    }
}
