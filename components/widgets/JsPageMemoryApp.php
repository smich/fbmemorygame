<?php
class JsPageMemoryApp extends JsPage
{
    /**
     * @var string name of the JS page
     */
    protected $pageName='MemoryApp';

    /**
     * @var array list of JS Modules that this Page uses
     */
    protected $modules=array();
}
?>