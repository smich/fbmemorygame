<?php
/**
 * JsPage
 *
 * Base class for all Javascript Pages. It registers the required page files (if any).
 *
 * There are two ways to use this widget:
 *
 *  - Extend this widget, if you have a custom Page file in www/res/js/wwf.pages.<pageName>.js
 *    Then you must override $pageName and set it to <pageName>. And if you use any other
 *    Modules on that page you must specify them int $modules (see comment there).
 *
 * OR
 *
 *  - If you want to use several JS Modules on a page you could either render each of them
 *    as a widget or use this widget and specify the 'modules' option for convenience:
 *
 *      $this->widget('JsPage',array(
 *          'SomeModule'    =>array( ... options for some module ...),
 *          'OtherModule'   =>true,  // Use OtherModule with default optoins
 *      ));
 */
class JsPage extends JsModule
{
    /**
     * @var string the name of the Js Page, e.g. 'MyJobs'
     */
    protected $pageName;

    /**
     * @var array the clientScript package(s) that provides the required scripts
     */
    protected $packages=array('js-main','css-main');

    /**
     * Concrete Js Pages must override this if they use Modules, and list all of them here:
     *
     *  array(
     *      'Example'       =>array( ... options for Example module ... ),
     *      'OtherModule'   =>true,    // Module without specific options
     *  )
     */
    protected $modules=array();

    // Modules registered through jsWidget()
    private $_m=array();

    /**
     * Register the Js files required for this page
     */
    protected function registerClientScripts()
    {
        $modules=isset($this->options['modules']) ? $this->options['modules'] : array();
        $this->options['modules']=CMap::mergeArray($this->modules,$modules);

        $cs=Yii::app()->clientScript;
        foreach($this->packages as $package)
            $cs->registerPackage($package);

        // Do this later, as jsWidget() may also render some modules meanwhile
        // and we do not want to double include module configuration
        $cs->onBeforeRender=array($this,'registerPageConfig');
    }

    /**
     * Register the remaining module configurations for those modules not rendere through jsWidget().
     */
    public function registerPageConfig()
    {
        // exclude module configuration for modules registered through jsWidget()
        foreach(array_keys($this->_m) as $module)
            if(isset($this->options['modules'][$module]))
                unset($this->options['modules'][$module]);

        $cs=Yii::app()->clientScript;
        if($this->pageName!==null)
            $cs->registerJsPage($this->pageName,$this->options);
        else
            foreach($this->options['modules'] as $module=>$options)
                $cs->registerJsModule($module,is_array($options) ? $options : array());
    }

    /**
     * Helper method to render a JsModule widget on a JsPage.
     *
     * This method can be called from run() on a JsPage, for example if the Page requires
     * an specific JsModule to render some output. If a module needs to create output on
     * some specific location in the view, it can also be called from the view like this:
     *
     *  $jsPage=$this->widget('JsPageExample');
     *    ... other markup ...
     *  $jsPage->jsWidget('JsModuleExample');
     *
     * Note, that the JsModule widget must be defined in $modules!
     *
     * @param string $name Full name of JsModule to render, e.g. 'JsModuleExample'
     * @param array $params (optional) widget parameters that should override config in $modules
     * @param bool $captureOutput (optional) whether to capture the output of the widget
     */
    public function jsWidget($name,$params=array(),$captureOutput=false)
    {
        $sName=substr($name,8);
        if(!isset($this->modules[$sName]))
            throw new CException(sprintf(
                'JsModule "%s" is not configured in JsPage $modules',
                $name
            ));

        $params=CMap::mergeArray(
            array(
                'options'=>isset($this->options['modules'][$sName]) ?
                    $this->options['modules'][$sName] : array(),
            ),
            $params
        );

        $this->_m[$sName]=true;

        return $this->controller->widget($name,$params,$captureOutput);
    }
}
