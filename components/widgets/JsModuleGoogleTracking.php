<?php
/**
 * JsModuleGoogleTracking
 *
 * This module provides the Google Javascript API on the clientside.
 *
 * To retrieve the API PPH.modules.GoogleTracking.getApi() can be called. If the API wasn't
 * loaded, it will be included asynchronously.
 *
 */
class JsModuleGoogleTracking extends JsModule
{
    /**
     * @var string the name of this Js module
     */
    protected $moduleName='GoogleTracking';

    /**
     * Add FB application Id to client module config
     */
    public function init()
    {
        if(param('google.analytics.id')!==null) {
            $this->options = array(
                'googleAnalyticsQueue' => array(
                    array('_setAccount',param('google.analytics.id')),
                    array('_trackPageview'),
                    array('_trackPageLoadTime'),
                )
            );

            parent::init();
        }
    }

    /**
     * Render the root element required by FB
     */
    public function run()
    {
    }
}
