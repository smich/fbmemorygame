<?php

/**
 * JsModuleFbApi
 *
 * This module provides the Facebook Javascript API on the clientside.
 *
 * To retrieve the API WWF.modules.FbApi.getApi() can be called. If the API wasn't
 * loaded, it will be included asynchronously.
 *
 * The  module can also auto-login a user if he is logged in to Facebook and has
 * connected (==integrated) his FB account with us. The user will not be auto-logged
 * in, if he logged out during the current browser session. A cookie is used to flag
 * that a manual logout has happened.
 *
 * REQUIREMENTS:
 * The Facebook externalAuth provider must be configured.
 */
class JsModuleFbApi extends JsModule {

    /**
     * @var string the name of this Js module
     */
    protected $moduleName = 'FbApi';

    /**
     * Add FB application Id to client module config
     */
    public function init() {
        $fb = Yii::app()->externalAuth->getProvider('facebook');

        if ($fb == null)
            throw new CException('Facebook auth provider not configured!');


        // Send the FB appId to the js module
        $this->options['insideFacebook'] = $fb->insideFacebook();
        $this->options['appId'] = $fb->applicationId;
        $this->options['namespace'] = $fb->namespace;
        $this->options['redirectUrl'] = param('redirectUrl');

        parent::init();
    }

    /**
     * Render the root element required by FB
     */
    public function run() {
        echo '<div id="fb-root"></div>';
    }

}
