<?php
/**
 * JsPageExample
 *
 * This is a demo Page for the WWF Js framework.
 */
class JsPageExample extends JsPage
{
    /**
     * @var string name of the JS page
     */
    protected $pageName='Example';

    protected $packages=array('wwf-main');

    /**
     * @var array list of JS Modules that this Page uses
     */
    protected $modules=array(
        'Example'=>array(
            'moduleOpt3'=>'Set in widget file of JsExamplePage',
            'moduleOpt4'=>'Set in widget file of JsExamplePage',
        ),
        'Messages'=>true,
    );

    /**
     * Optional: Render extra markup for this page module
     */
    public function run()
    {
        // If a module has to register extra scripts or must render some
        // extra markup, we can render it here explicitely:
        //$this->jsWidget('JsModuleExample');
    }
}
