<?php

class StepMachineComponent extends CApplicationComponent {

    private $_active;
    private $_key;
    private $_mapping;
    private $_memId;
    private $_step;
    private $_stepMachine;

    public function init() {
        parent::init();
    }

    public function config($key, $mapping) {
        if ($key === null ||
                $mapping === null ||
                !is_array($mapping)
        )
            return false;

        $this->_active = true;
        $this->_key = $key;
        $this->_mapping = $mapping;
        if (!Yii::app()->user->isGuest)
            $this->_memId = Yii::app()->user->id;

        return $this;
    }

    public function getIsValid() {
        if ($this->key !== null &&
                $this->_memId !== null &&
                $this->mapping !== null &&
                is_array($this->mapping)
        )
            return true;

        return false;
    }

    public function getStepMachine() {
        if (!$this->_active)
            return null;

        if ($this->_stepMachine === null) {
            $stepMachine = StepMachine::model()->findByAttributes(array(
                'key' => $this->_key,
                'memId' => $this->_memId,
                    ));
            Yii::log("StepMachine record " . ($stepMachine ? "was loaded" : "doesn't exist") . " for mem_id: " . $this->_memId, CLogger::LEVEL_INFO, 'app.components.stateMachine');

            if ($stepMachine === null) {
                $this->_stepMachine = new StepMachine();
                $this->_stepMachine->key = $this->_key;
                $this->_stepMachine->step = 0;
                if (!Yii::app()->user->isGuest && $this->_memId) {
                    Yii::log("Create a new step machine records for key:$this->_key - memId:$this->_memId", CLogger::LEVEL_ERROR, 'app.components.stateMachine');
                    $this->_stepMachine->memId = Yii::app()->user->id;
                    if ($this->saveStepMachine())
                        Yii::log("StepMachine record created", CLogger::LEVEL_INFO, 'app.components.stateMachine');
                    else
                        ydump($this->_stepMachine->errors);
                }
            } else {
                $this->_stepMachine = $stepMachine;
            }
        }

        return $this->_stepMachine;
    }

    public function saveStepMachine() {
        if (!$this->_active)
            return null;
        $stepMachine = $this->getStepMachine();

        if ($stepMachine->step > $this->_step)
            Yii::log("Step is reduced form {$stepMachine->step} to $this->_step", CLogger::LEVEL_INFO, 'app.components.stateMachine');

        $stepMachine->step = $this->_step;
        return $stepMachine->save();
    }

    public function getStep() {
        if (!$this->_active)
            return null;

        $this->stepMachine;
        if ($this->_step === null) {
            $this->_step = (int) $this->stepMachine->step;
        }

        return $this->_step;
    }

    public function getNextAction() {
        if (!$this->_active)
            return null;

        $nextstep = ($this->step + 1 <= (count($this->_mapping) - 1)) ? $this->step + 1 : $this->step;
        Yii::log("Next step: {$nextstep} - action: " . $this->_mapping[$nextstep], CLogger::LEVEL_INFO, 'app.components.stateMachine');

        return $this->_mapping[$nextstep];
    }

    public function setStep($step) {
        if (!$this->_active)
            return null;

        $this->_step = $step;
        return $this->saveStepMachine();
    }

    public function gainStep() {
        if (!$this->_active)
            return null;

        return $this->setStep($this->_step + 1);
    }

    public function resetStep() {
        if (!$this->_active)
            return null;

        return $this->setStep(0);
    }

}

?>
