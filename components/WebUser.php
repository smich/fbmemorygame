<?php

/**
 * Custom Webuser class
 */
class WebUser extends CWebUser {

    private $_member = false;

    /**
     * Returns the connected Member
     *
     * @return mixed Member record connected to the logged in user, null for guests
     */
    public function getMember() {
        if ($this->_member === false) {
            if (($id = $this->getId()) === null)
                return null;

            $this->_member = Member::model()->findByPk($id);

            if($this->_member===null){
                $this->setId(null);
                return null;
            }

            // if member is deactivated, force him to log out and redirect him to the homepage
            if (!$this->_member->active) {
                $this->logout(false);
                Yii::app()->request->redirect(url(Yii::app()->homeUrl, false)); // No SSL for home
            }

            YII_DEBUG && Yii::trace( 'Loaded member ' . $id, 'app.components.webuser' );
        }

        return $this->_member;
    }

    /**
     * @param mixed Clients/Freelancer record connected to the logged in user
     */
    public function setMember($member) {
        $this->_member = $member;
    }

    /**
     * @return mixed name of social auth provider if user logged in through it or null otherwhise
     */
    public function getLoggedInVia() {
        return $this->getState('_loggedInVia', null);
    }

    /**
     * @param string $name the name of the social authentication provider (facebook, ...)
     */
    private function setLoggedInVia($name) {
        $this->setState('_loggedInVia', $name);
    }

    /**
     * Returns the URL that the user should be redirected to after successful login.
     * This property is usually used by the login action. If the login is successful,
     * the action should read this property and use it to redirect the user browser.
     * @param string $defaultUrl the default return URL in case it was not set previously. If this is null,
     * the application entry URL will be considered as the default return URL.
     * @param string $clear true if the stored session variable must be cleared when used, false otherwise
     * @return string the URL that the user should be redirected to after login.
     * @see loginRequired
     */
    public function getReturnUrl($defaultUrl = null, $clear = false) {
        $url = parent::getReturnUrl($defaultUrl);
        if ($clear)
            parent::setReturnUrl(null);
        return $url;
    }

}
