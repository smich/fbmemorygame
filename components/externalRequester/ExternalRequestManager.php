<?php

/**
 * ExternalRequestManager
 *
 * This application component manages external requests.
 * The provider can be configured through the {@link provider} property.
 * Use {@link getProvider()} to obtain a configured provider object.
 */
class ExternalRequestManager extends CApplicationComponent {

    /**
     * Unique key used in Metadata
     */
    const METADATA_KEY = 'member.socialActions';

    /**
     * Array of errors
     * @var Array of strings
     */
    private $_errors = array();

    /**
     *  List of available external providers, like Facebook, Twitter etc
     * @var Array of Strings
     */
    public $availableProviders = array('facebook');

    /**
     *  List of available sharable objects, like Hourlie, Job, Member
     * @var Array of Strings
     */
    public $availableObjects = array('member','hourlie','job');

    /**
     *  List of available actions per object, like Buy, Star etc.
     *  The values are taken on init() from every model using $model->socialActions
     * @var Array of Strings
     */
    public $availableActions = array();

    /**
     *  List of important actions that will be posted even if the daily limit
     * of the meber is exceeded
     * @var Array of Strings
     */
    public $importantActions = array('buy', 'post', 'rate');

    /**
     * Temporary storage of the instantiated auth providers
     * @var Array of Auth provider models
     */
    protected $_p = array();

    /**
     * Import directory with provider classes
     */
    public function init() {
        YiiBase::import('application.components.externalRequester.BaseRequester');
        YiiBase::import('application.components.externalRequester.requester.*');
        $memberPrivacy = Yii::app()->user->member->privacy;

        foreach ($this->availableObjects as $object) {
            switch ($object) {
                case 'hourlie':
                    $this->availableActions[$object] = Hourlie::socialActions($memberPrivacy);
                    break;
                case 'job':
                    $this->availableActions[$object] = Projects::socialActions($memberPrivacy);
                    break;
                case 'member':
                    $this->availableActions[$object] = Members::socialActions($memberPrivacy);
                    break;
                default:
                    break;
            }
        }
    }

    public function getProvider($name) {
        if (!isset($this->_p[$name]))
            $this->_p[$name] = $this->createProvider($name);
        return $this->_p[$name];
    }

    /**
     * Create and init a specific AuthProvider.
     *
     * @param string $name of AuthProvider
     * @return AuthProvider the created AuthProvider object
     */
    public function createProvider($name) {
        if(!in_array($name,$this->availableProviders))
            throw new CHttpException(500, 'Requester for ' . ucfirst($this->name) . ' not configured');

        $provider = YiiBase::createComponent( sprintf('application.components.externalRequester.requester.%sRequester', ucfirst($name)));
        $provider->init();
        return $provider;
    }

    /**
     * Make an action request to all available services
     * @param Model $model that action is performed to
     * @param String $action like buy, star etc.
     * @param Array $options mixed params needed in action
     * @param Constant $requestAccessLevel When to start the process to ask the provider for a higher access level
     * if request fails because the approved level of authentication/authorization is less than the needed
     * (start the integration process, open an Aprroval popUp or anything else needed)
     * @return bool whether request was successful
     */
    public function action($model, $action, $options = array(), $requestAccessLevel = null) {
        foreach ($this->availableProviders as $name) {
            $provider = $this->getProvider($name);
            if(!$provider->requestAction($model, $action, $options, $requestAccessLevel)){
                $this->addErrors($provider->errors);
            }
        }

        return !$this->hasErrors();
    }

    /**
     * Make a request to url if user is qualified for it
     * @param String $url to do the request
     * @param String $method whether to do a post or get request
     * @param Constant $requestAccessLevel When to start the process to ask the provider for a higher access level
     * @return Bool whether request was successful
     */
    public function url($provider, $url,$method='post',$options=array(), $requestAccessLevel = false) {
        Yii::log('Url request to ' . ucfirst($provider) . ' activated - url'.$url, CLogger::LEVEL_TRACE, 'app.components.extRequest.Manager');
        $provider = $this->getProvider($name);

        if($provider->requestUrl($url,$method,$options, $requestAccessLevel)){
            $this->addErrors($provider->errors);
        }

        return !$this->hasErrors();
    }

	/**
	 * Returns a value indicating whether there is any validation error.
	 * @param string $attribute attribute name. Use null to check all attributes.
	 * @return boolean whether there is any error.
	 */
	public function hasErrors()
	{
        return $this->_errors!==array();
	}

	/**
	 * Returns the errors for all attribute or a single attribute.
	 * @param string $attribute attribute name. Use null to retrieve errors for all attributes.
	 * @return array errors for all attributes or the specified attribute. Empty array is returned if no error.
	 */
	public function getErrors()
	{
        return $this->_errors;
	}

	/**
	 * Adds a new error to the specified attribute.
	 * @param string $attribute attribute name
	 * @param string $error new error message
	 */
	public function addError($error)
	{
		$this->_errors[]=$error;
	}

	/**
	 * Adds a list of errors.
	 * @param array $errors a list of errors. The array keys must be attribute names.
	 * The array values should be error messages. If an attribute has multiple errors,
	 * these errors must be given in terms of an array.
	 * You may use the result of {@link getErrors} as the value for this parameter.
	 */
	public function addErrors($errors)
	{
		foreach($errors as $error)
		{
			if(is_array($error))
			{
				foreach($error as $e)
					$this->addError($e);
			}
			else
				$this->addError($error);
		}
	}

	/**
	 * Removes errors for all attributes or a single attribute.
	 * @param string $attribute attribute name. Use null to remove errors for all attribute.
	 */
	public function clearErrors()
	{
        $this->_errors=array();
	}

    /**
     *
     * @return type
     */
    public function getMetadataKey()
    {
        return self::METADATA_KEY;
    }

}

?>
