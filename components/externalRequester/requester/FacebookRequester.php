<?php

/**
 * FacebookRequester
 *
 * This is the Requester to perform Facebook requests.
 */
class FacebookRequester extends BaseRequester {

    private $_namespace;

    private $_coreActions = array('feed');

    /**
     * Set up Facebook API object
     */
    public function init()
    {
        $this->_namespace = Yii::app()->params['social.facebook.namespace'];
        parent::init();
    }

    /**
     * Make an action request if user is qualified for it
     * @param Model $model that action is performed to
     * @param String $action like buy, star etc.
     * @param Array $options mixed params needed in action
     * @param Constant $requestAccessLevel When to start the process to ask the provider for a higher access level
     * @return bool whether request was successful
     */
    public function requestAction($model,$action,$options=array(), $requestAccessLevel = null) {
        Yii::log('Action ' . ucfirst($action) . ' activated for ' . get_class($model), CLogger::LEVEL_TRACE, 'app.components.extRequest.Facebook');
        $params = $this->prepareRequest($model,$action,$options);
        return $this->requestUrl($params['url'],$params['method'],$params['options'], $requestAccessLevel);
    }

    /**
     * Make a request to url if user is qualified for it
     * @param String $url to do the request
     * @param String $method whether to do a post or get request
     * @param Constant $requestAccessLevel When to start the process to ask the provider for a higher access level
     * @return Bool whether request was successful
     */
    public function requestUrl($url,$method='post',$options=array(), $requestAccessLevel = null) {
        Yii::log('Url request activated - url: '.$url, CLogger::LEVEL_TRACE, 'app.components.extRequest.Facebook');
        $response = false;
        if ($this->qualifies($requestAccessLevel)) {
            $response = $this->provider->request($url,$method,$options);
        }

        if($this->provider->hasErrors()){
            $this->addErrors($this->provider->errors);
            if($requestAccessLevel)
                if($response===null)
                    $this->provider->login();
                else
                    $this->provider->askPermission();
        }

        return !$this->hasErrors();
    }

    private function prepareRequest($model,$action,$options=array()) {
        Yii::log('Prepare request', CLogger::LEVEL_TRACE, 'app.components.extRequest.Facebook');
        $sa = $model->socialAttributes;

        if(empty($sa['actionsAllowed']) || in_array($action, $sa['actionsAllowed']) || in_array($action, $this->_coreActions)) {
            if(in_array($action, $this->_coreActions))
                $url = "/me/$action";
            else{
                $object = $sa['metaType'];
                $objectUrl = url($sa['url'], null, true, 'web');
                $options = array_merge(array($object => $objectUrl), $options);
                $url = "/me/$this->_namespace:$action";
            }
            $method = self::METHOD_POST;
        } else {
            throw new CHttpException(500, 'Action ' . ucfirst($action) . ' not configured for Facebook');
        }

        return array(
            'url' => $url,
            'method' => $method,
            'options' => $options,
        );
    }

}
