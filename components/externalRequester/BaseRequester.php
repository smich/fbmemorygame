<?php

/**
 * BaseRequester
 *
 * This is the abstract base class for external requesters.
 *
 * IMPORTANT: Make sure that names of extended classes end with "Requester"!
 */
abstract class BaseRequester extends CComponent {

    const METHOD_POST = 'post';
    const METHOD_GET = 'get';

    /**
     * Auth provider
     * @var Model
     */
    protected $_p;

    /**
     * Array of errors
     * @var Array of strings
     */
    private $_errors = array();

    /**
     * @var String Name specific to the child class
     */
    private $_name;

    /**
     * @var Mixed $_permissions String to check a specific permission, Array to check many permissions
     */
    private $_permissions = array();

    /**
     * Detect name. Always call parent::init() in derived classes.
     */
    public function init() {
        // 'facebook', 'linkedin', ...
        $this->_name = strtolower(substr(get_class($this), 0, -9));
    }

    /**
     * @return string short name of this auth provider ('facebook', 'linkedin', ...)
     */
    public function getName() {
        return $this->_name;
    }

	/**
	 * Returns a value indicating whether there is any validation error.
	 * @param string $attribute attribute name. Use null to check all attributes.
	 * @return boolean whether there is any error.
	 */
	public function hasErrors()
	{
        return $this->_errors!==array();
	}

	/**
	 * Returns the errors for all attribute or a single attribute.
	 * @param string $attribute attribute name. Use null to retrieve errors for all attributes.
	 * @return array errors for all attributes or the specified attribute. Empty array is returned if no error.
	 */
	public function getErrors()
	{
        return $this->_errors;
	}

	/**
	 * Adds a new error to the specified attribute.
	 * @param string $attribute attribute name
	 * @param string $error new error message
	 */
	public function addError($error)
	{
		$this->_errors[]=$error;
	}

	/**
	 * Adds a list of errors.
	 * @param array $errors a list of errors. The array keys must be attribute names.
	 * The array values should be error messages. If an attribute has multiple errors,
	 * these errors must be given in terms of an array.
	 * You may use the result of {@link getErrors} as the value for this parameter.
	 */
	public function addErrors($errors)
	{
		foreach($errors as $error)
		{
			if(is_array($error))
			{
				foreach($error as $e)
					$this->addError($e);
			}
			else
				$this->addError($error);
		}
	}

	/**
	 * Removes errors for all attributes or a single attribute.
	 * @param string $attribute attribute name. Use null to remove errors for all attribute.
	 */
	public function clearErrors()
	{
        $this->_errors=array();
	}

    /**
     * Get an instance of a auth provider. Instantiate it if it is the first time we use it.
     * @param String $provider
     * @return Auth provider model
     * @throws CHttpException Authentication provider not configured for requests
     */
    public function getProvider() {
        if (!$this->_p) {
            if (($provider = Yii::app()->externalAuth->getProvider($this->name)) === null)
                throw new CHttpException(500, 'Authentication provider for ' . ucfirst($this->name) . ' not configured');
            else
                $this->_p = $provider;
        }
        return $this->_p;
    }

    /**
     * Make an action request if user is qualified for it
     * @param Model $model that action is performed to
     * @param String $action like buy, star etc.
     * @param Array $options mixed params needed in action
     * @param Constant $requestAccessLevel When to start the process to ask the provider for a higher access level
     * @return bool whether request was successful
     */
    abstract public function requestAction($model,$action,$options=array(), $requestAccessLevel = null);

    /**
     * Make a request to url if user is qualified for it
     * @param String $url to do the request
     * @param String $method whether to do a post or get request
     * @param Constant $requestAccessLevel When to start the process to ask the provider for a higher access level
     * @return Bool whether request was successful
     */
    abstract public function requestUrl($url,$method='post',$options=array(), $requestAccessLevel = null);

    /**
     * Check if user qualifies for requests or a specific request
     * @param Constant $requestAccessLevel When to start the process to ask the provider for a higher access level
     * @return bool whether request was successful
     */
    public function qualifies($requestAccessLevel = null) {
        Yii::log('Check qualification', CLogger::LEVEL_TRACE, 'app.components.extRequest.Facebook');

        // Check if he CAN do the request aka integrated, connected with correct permissions
        if(!$this->qualifyExternalPermissions($requestAccessLevel))
            return false;

        // Check if he WANT to do the request aka check settings
        if(!$this->qualifyMemberSettings())
            return false;

        // Check if we WANT him to do the request aka check history
        if(!$this->qualifyRequest())
            return false;
        else
            return true;
    }

    /**
     * Check if member has given the authoentication and authorization permissions
     * @param Constant $requestAccessLevel When to start the process to ask the provider for a higher access level
     * @return bool whether member qualifies or not
     */
    public function qualifyExternalPermissions($requestAccessLevel = null){
        $type = (is_array($this->_permissions))?BaseAuthProvider::PERM_FORMAT_ARRAY:BaseAuthProvider::PERM_FORMAT_STRING;

        if($this->provider->checkAccess( $this->_permissions, $type ))
            return true;
        else
            if($requestAccessLevel)
                return $this->provider->askPermissions();
            else
                return false;
    }

    /**
     * Check if the request qualifies depending on member settings
     * @return bool whether request qualifies or not
     */
    public function qualifyMemberSettings(){
        return true;
    }

    /**
     * Check if the request qualifies depending on previous requests and statistics
     * @return bool whether request qualifies or not
     */
    public function qualifyRequest(){
        return true;
    }
}