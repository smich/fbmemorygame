<?php

class BaseAppController extends Controller {

    public $providerName = 'facebook';
    public $defaultHeader = '&nbsp;';
    public $defaultPageTitle = 'Κάθε Φωνή, Ένα Μέλλον';
    public $defaultMetaDescription = 'Στείλε το θετικό μήνυμα σου και υποστήριξε και εσύ τα άτομα με διαβήτη.';
    public $defaultMetaKeywords = '';
    public $pageDescription = "Στείλε το θετικό μήνυμα σου και υποστήριξε και εσύ τα άτομα με διαβήτη. Μοιράσου την έξυπνη συνταγή σου.";// και έλα μαζί μας στο Πνευματικό Κέντρο του Δήμου Αθηνών την Κυριακή 16/12 στις 11.00 π.μ. να στηρίξεις την προσπάθεια μας.
    public $pageKeywords = '';

    public $forceMessagesSmile = false;
    public $forcePrelikeBg = false;

    /**
     * StepMachine handles the page each member should see
     * @var Model stepMachine
     */
    public $stepMachine;

    /**
     * Using this mapping of actions we can redirect to the first/next/any step of the process
     * @var array of actions included in stepMachine
     */
    public $mapping = array();

    /**
     * @return array rulse for the "accessControl" filter.
     */
    public function accessRules() {
        return array(
            array('allow',
                'users' => array('@'),
                'actions' => array('index'),
            ),
            array('deny'), // Deny anything else
        );
    }

    /**
     * Checks the validity of the actions in mapping and the instantiation of the stepMachine
     * @throws CHttpException If an action in mapping is not defined
     * @throws CHttpException Step machine failed to instantiate
     */
    public function init() {
        $this->pageTitle = $this->defaultPageTitle;
        $this->pageDescription = $this->defaultMetaDescription;
        $this->pageKeywords = $this->defaultMetaKeywords;

        foreach ($this->mapping as $value) {
            $action = 'action' . ucfirst($value);
            if (!method_exists($this, $action)) {
                Yii::log("Action $action is not defined", CLogger::LEVEL_ERROR, 'app.controllers.baseAppController');
                throw new CHttpException(500, "$action is not defined");
            }
        }
        $this->stepMachine = Yii::app()->stepMachine->config($this->id, $this->mapping);
        if ($this->stepMachine === false) {
            Yii::log('Step machine failed to instantiate', CLogger::LEVEL_ERROR, 'app.controllers.baseApp');
            throw new CHttpException(500, 'Step machine failed to instantiate');
        }
    }

    /**
     * Overloads the default Controller filters
     * @return array of filters
     */
    public function filters() {
        if (is_array($this->mapping)) {
            $actions = implode(',', array_diff(
                            $this->mapping, array(
                        'authenticate',
                    )));
            return array_merge(array(
                        'InsideFacebook',
                        'TraceAction - prelike',
//                        'TraceAction +' . $actions,
                        'CheckStep +' . $actions,
                        'CheckAuthentication',
                            )
                            , parent::filters());
        } else
            return parent::filters();
    }

    /**
     * Check if the page opened InsideFacebook and write a Session variable
     * @param $filterChain The default param passed in filters
     */
    public function filterInsideFacebook($filterChain) {
        if (isset($_REQUEST['clearInsideFacebook'])) {
            Yii::log("clearInsideFacebook is present: Log out user", CLogger::LEVEL_WARNING, 'app.controllers.baseApp');
            Yii::app()->session['insideFacebook'] = null;
            Yii::app()->session['insidePage'] = null;
            Yii::app()->session['pageLiked'] = null;
            Yii::app()->user->logout();
            $this->redirect(Yii::app()->homeUrl);
        }

        if (isset($_REQUEST['signed_request']))
            Yii::app()->session['insideFacebook'] = true;

        $provider = $this->getAuthProvider($this->providerName);
        if($provider->insidePage())
            Yii::app()->session['insidePage'] = true;

        if($provider->pageLiked() || isset($_REQUEST['debug_pagelike']))
            Yii::app()->session['pageLiked'] = true;

        $filterChain->run();
    }

    /**
     * Just a trace for the current selected action
     * TODO: Remove it when done testing
     * @param $filterChain The default param passed in filters
     */
    public function filterTraceAction($filterChain) {
        Yii::log("Page: " . $this->id . '/' . $this->action->id . ' - User is ' . (Yii::app()->user->isGuest ? 'guest' : 'logged in'), CLogger::LEVEL_INFO, 'app.controllers.base');

        if(Yii::app()->session['insidePage'] && !Yii::app()->session['pageLiked'])
            $this->redirect (Yii::app()->createUrl('site/prelike'));

        $filterChain->run();
    }

    /**
     * Checks if the step is correct for the current user
     * Redirects to the correct step if validation fails
     * @param $filterChain The default param passed in filters
     */
    public function filterCheckStep($filterChain) {
        $currentStep = $this->stepMachine->step;

        if ($this->mapping[$currentStep] != $this->action->id) {
            if (Yii::app()->user->isGuest)
                Yii::log("Anonymous user tried to access {$this->id}/{$this->action->id}", CLogger::LEVEL_WARNING, 'app.controllers.baseApp');
            else
                Yii::log("Member " . Yii::app()->user->id . " tried to access {$this->id}/{$this->action->id}", CLogger::LEVEL_WARNING, 'app.controllers.baseApp');

            // Redirect them to the correct step
            $this->redirectTo($currentStep);
        }

        $filterChain->run();
    }

    /**
     * Check some special authentication cases
     * @param $filterChain The default param passed in filters
     */
    public function filterCheckAuthentication($filterChain) {
        // Log out and redirect user to home page if the user logged in Facebook
        // is different than the one logged in our app and
        if (!Yii::app()->user->isGuest) {
            $provider = $this->getAuthProvider($this->providerName);
            if ($provider->userId !== null && Yii::app()->user->member->external_id !== null && $provider->userId != Yii::app()->user->member->external_id) {
                Yii::app()->user->logout();
                $this->redirect(Yii::app()->homeUrl);
            }
        } else
        // Take him to externalAuth if there is a CODE request
        if ($this->action->id != 'externalAuth' && isset($_REQUEST['code'])) {
            Yii::log("Code variable exists outside externalAuth page, take him to externalAuth", CLogger::LEVEL_WARNING, 'app.controllers.baseApp');
            $this->redirect(Yii::app()->createUrl('site/externalAuth', array(
                        'name' => $this->providerName,
                        'code' => $_REQUEST['code'],
                        'signed_request' => $_REQUEST['signed_request'],
                    )));
        }

        $filterChain->run();
    }

    /**
     * Default action that redirects users to the correct step specified in the StepMachine
     */
    public function actionIndex() {
        $currentStep = (int) $this->stepMachine->step;
        $this->redirectTo($currentStep);
    }

    public function actionAuthenticate($type = 'facebook') {
        $user = Yii::app()->user;

        Yii::log("User is ".(Yii::app()->user->isGuest?'guest':'logged in'), CLogger::LEVEL_INFO, 'app.controllers.baseApp');
        if (!Yii::app()->user->isGuest) {
            if (Yii::app()->user->hasFlash('member-register')) {
                $flashOptions = Yii::app()->user->getFlash('member-register');
                $this->facebookPost($flashOptions);
            }
            Yii::log("Authenticated - id: " . $this->action->id . " - nextStep: " . $this->nextStep($this->action->id), CLogger::LEVEL_INFO, 'app.controllers.baseApp');
            $this->stepMachine->setStep($this->nextStep($this->action->id));
            $this->redirectTo($this->nextStep($this->action->id));
        }

        if ($type == 'facebook')
            $redirectUrl = Yii::app()->createUrl('site/externalAuth', array('name' => $this->providerName));
        else
            $redirectUrl = Yii::app()->createUrl('site/create');

        $user->setReturnUrl($this->actionUrl($this->action->id, array('nextStep' => 1)));
        Yii::log("Go to ".$redirectUrl, CLogger::LEVEL_INFO, 'app.controllers.baseApp');
        $this->redirect($redirectUrl);
    }

    /**
     * Render form and handle post request
     */
    public function actionPreSubmit() {
        if (isset($_GET['nextStep'])) {
            // Advance to next step and redirect
            $this->stepMachine->setStep($this->nextStep($this->action->id));
            $this->redirectTo($this->nextStep($this->action->id));
        }

        $this->render('preSubmit');
    }

    public function actionComplete() {
        $this->redirect(array('site/index', 'complete' => $this->id));
    }

    /*     * ** HELPING FUNCTIONS ** */

    public function actionFromStep($step) {
        return $this->mapping[$step];
    }

    public function nextAction($current) {
        if (!is_int($current))
            $current = $this->stepFromAction($current);
        return $this->mapping[$current + 1];
    }

    public function stepFromAction($action) {
        return (int) array_search($action, $this->mapping);
    }

    public function nextStep($current) {
        if (!is_int($current))
            $current = $this->stepFromAction($current);
        return (int) $current + 1;
    }

    public function redirectTo($target, $params = array(), $top = false) {
        if (is_int($target))
            $target = $this->actionFromStep($target);

        Yii::log("Redirect to $target", CLogger::LEVEL_INFO, 'app.controllers.base');
        $this->redirect($this->actionUrl($target, $params, $top));
    }

    public function actionUrl($target, $params = array(), $top = false) {
        if (is_int($target))
            $target = $this->actionFromStep($target);
        if ($top)
            $params = array_merge($params, array('target' => 'top'));

        return Yii::app()->createUrl($this->id . "/" . $target, $params);
    }

    /**
     * Push a post to Facebook
     */
    public function facebookPost($options) {
        if (Yii::app()->user->isGuest) {
            Yii::log("Post to Facebook for Anonymous user... fail", CLogger::LEVEL_ERROR, 'app.controllers.base');
            return;
        }

        if (($provider = $this->getAuthProvider($this->providerName)) === null) {
            Yii::log("Provider not defined to use it to post to Facebook", CLogger::LEVEL_ERROR, 'app.controllers.base');
            return;
        }

        if ($provider->getUserId() === null) {
            Yii::log("User not loaded from Facebook", CLogger::LEVEL_ERROR, 'app.controllers.base');
            return;
        }

        if (Yii::app()->user->member->permissions === null) {
            Yii::log("Permissions not loaded", CLogger::LEVEL_ERROR, 'app.controllers.base');
            return;
        }

        $permissions = CJSON::decode(Yii::app()->user->member->permissions);
        if (!in_array('publish_stream', $permissions)) {
            Yii::log("Permission to publish to stream is not appproved", CLogger::LEVEL_ERROR, 'app.controllers.base');
            return;
        }

        $feedOptions = array(
            'picture' => Yii::app()->createAbsoluteUrl('/res/img/logo.png'),
            'link' => 'http://www.facebook.com/podilatodromia/app_296935417075368',
            'actions' => array(
                'name' => 'Ένωσε τη φωνή σου!',
                'link' => 'http://www.facebook.com/podilatodromia/app_296935417075368',
            )
        );
        $feedOptions = array_merge($options, $feedOptions);

        if (!$provider->request('/me/feed', 'POST', $feedOptions)) {
            ydump("Push to Facebook failed " . CJSON::encode($provider->errors), CLogger::LEVEL_ERROR, 'app.controllers.base');
            Yii::log("Push to Facebook failed " . CJSON::encode($provider->errors), CLogger::LEVEL_ERROR, 'app.controllers.base');
        }
    }

    public function getInsideFacebook() {
        return Yii::app()->session->get('insideFacebook', false);
    }

    /**
     * Load JsModules needed in every page
     */
    public function loadDefaultScripts() {
        $this->widget('JsModuleGoogleTracking');
        $this->widget('JsModuleFbApi');
    }

}
