<?php
/**
 * BaseAuthProvider
 *
 * This is the abstract base class for authentication providers.
 *
 * IMPORTANT: Make sure that names of extended classes end with "AuthProvider"!
 */
abstract class BaseAuthProvider extends CComponent
{
    const CALLBACK_PARAM='__externalAuthCb__';

    private $_api;
    private $_errors = array();

    private $_userProfile=false;
    private $_userId=false;
    private $_photoUrl=false;
    private $_experiences;
    private $_educations;
    private $_skills;
    private $_recommendations;

    private $_name;

    /**
     * @var Array of stings, list of permissions
     */
    public $_permissions = array();
    CONST PERM_FORMAT_COMMA_SEPARATED = 'comma-separated';
    CONST PERM_FORMAT_JSON = 'json';
    CONST PERM_FORMAT_ARRAY = 'array';
    CONST PERM_FORMAT_STRING = 'string';

    /**
     * @var Constant showing the
     */
    public $error_level;
    CONST ACCESS_LEVEL_UNKNOWN = 0;
    CONST ACCESS_LEVEL_NOT_LOGGED_IN = 10;
    CONST ACCESS_LEVEL_NOT_INTEGRATED = 20;
    CONST ACCESS_LEVEL_NOT_ENOUGH_PERMISSIONS = 30;

    /**
     * Start authentication by redirecting to the login URL of provider
     *
     * @param string $callbackUrl the URL to redirect to after authentication
     */
    abstract public function redirectToLogin($callbackUrl);

    /**
     * Process the authentication callback request from external site
     *
     * @return bool wether authentication was successful
     */
    abstract public function processLoginCallback();

    /**
     * Logout authenticated user
     *
     * @return bool wether logout was successful
     */
    abstract public function logout();

    /**
     * @return mixed the access token for the current user session or null if not connected
     */
    abstract public function getAccessToken();

    /**
     * @param string $token the access token for the current user session
     */
    abstract public function setAccessToken($token);

    /**
     * Load ID if user on external site through API
     *
     * @return mixed User ID or null if user is not logged in
     */
    abstract protected function loadUserId();

    /**
     * Load user profile from external site through API
     *
     * @return mixed the AuthUserProfile object or null if user is not logged in
     */
    abstract protected function loadUserProfile();

    /**
     * @return mixed the URL to the user photo or null if none
     */
    abstract protected function loadPhotoUrl();

    /**
     * Load user experiences from external site through API
     *
     * @return array of new MemExperience records
     */
    abstract protected function loadExperiences();

    /**
     * Load user educations from external site through API
     *
     * @return array of new MemEducation records
     */
    abstract protected function loadEducations();

    /**
     * Load user skills from external site through API
     *
     * @return array of new MemSkill records
     */
    abstract protected function loadSkills();

    /**
     * Load user recommendations from external site through API
     *
     * @return array of new MemRecommendation records
     */
    abstract protected function loadRecommendations();

    /**
     * Detect name. Always call parent::init() in derived classes.
     */
    public function init()
    {
        // 'facebook', 'linkedin', ...
        $this->_name=strtolower(substr(get_class($this),0,-12));
    }

    /**
     * Log in a user through an external Auth provider.
     *
     * This method will redirect the user browser to the external login site. It will
     * also process the callback request from that site.
     *
     * NOTE: This method will exit the current script, when the user is redirected.
     * So it will not return anything in this case. This allows to write a simple
     * login action like this:
     *
     *      public function actionLoginFacebook()
     *      {
     *          $provider=Yii::app()->externalAuth->getProvider('facebook');
     *
     *          if($provider->login())
     *              // user logged in
     *          else
     *              // user cancelled login
     *      }
     *
     * See redirectToLogin() and processLoginCallback() for a more fine grained control flow.
     *
     * @return bool wether user was logged in successfully.
     */
    public function login()
    {
        if($this->getIsCallback())
        {
            $result=$this->processLoginCallback();

            Yii::log("Processing callback: ".($result ? 'success' : 'error ('.  implode(',', $this->errors).')'), CLogger::LEVEL_INFO, 'app.components.baseAuth');
            return $result;
        }
        else
        {
            YII_DEBUG && Yii::trace('Redirect to '.$this->name.' for login','app.components.baseAuth');

            // #3604 Make sure, all leftovers from previous authentications are reset
            $this->logout();

            // Will stop execution of this script:
            return $this->redirectToLogin($this->getCallbackUrl());
        }
    }

    /**
     * @return mixed the API object for this auth provider
     */
    public function getApi()
    {
        return $this->_api;
    }

    /**
     * @param mixed $api the API object for this auth provider
     */
    protected function setApi($api)
    {
        $this->_api=$api;
    }

    /**
     * @return bool wether this is a callback request. (Only useful when login() is used)
     */
    public function getIsCallback()
    {
        return isset($_GET[self::CALLBACK_PARAM]) && $_GET[self::CALLBACK_PARAM]==='1';
    }

	/**
	 * Returns a value indicating whether there is any validation error.
	 * @param string $attribute attribute name. Use null to check all attributes.
	 * @return boolean whether there is any error.
	 */
	public function hasErrors()
	{
        return $this->_errors!==array();
	}

	/**
	 * Returns the errors for all attribute or a single attribute.
	 * @param string $attribute attribute name. Use null to retrieve errors for all attributes.
	 * @return array errors for all attributes or the specified attribute. Empty array is returned if no error.
	 */
	public function getErrors()
	{
        return $this->_errors;
	}

	/**
	 * Adds a new error to the specified attribute.
	 * @param string $attribute attribute name
	 * @param string $error new error message
	 */
	public function addError($error)
	{
		$this->_errors[]=$error;
	}

	/**
	 * Adds a list of errors.
	 * @param array $errors a list of errors. The array keys must be attribute names.
	 * The array values should be error messages. If an attribute has multiple errors,
	 * these errors must be given in terms of an array.
	 * You may use the result of {@link getErrors} as the value for this parameter.
	 */
	public function addErrors($errors)
	{
		foreach($errors as $error)
		{
			if(is_array($error))
			{
				foreach($error as $e)
					$this->addError($e);
			}
			else
				$this->addError($error);
		}
	}

	/**
	 * Removes errors for all attributes or a single attribute.
	 * @param string $attribute attribute name. Use null to remove errors for all attribute.
	 */
	public function clearErrors()
	{
        $this->_errors=array();
	}

    /**
     * Get permissions from the stored value or make the provider ask for them
     * @param String $format the format of the returned values
     * @param bool $forceUpdate ONLY USED in each specific provider
     * @return mixed depending on the chosen $format
     * @throws CHttpException
     */
    public function getPermissions($format = self::PERM_FORMAT_COMMA_SEPARATED, $forceUpdate = false)
    {
        YII_DEBUG && Yii::log('Get permissions: '.json_encode($this->_permissions), CLogger::LEVEL_TRACE ,'app.components.baseAuth');
        switch ($format) {
            case self::PERM_FORMAT_COMMA_SEPARATED:
                return implode(',', $this->_permissions);
                break;
            case self::PERM_FORMAT_JSON:
                return CJSON::encode($this->_permissions);
                break;
            case self::PERM_FORMAT_ARRAY:
                return $this->_permissions;
                break;
            default:
                throw new CHttpException(500, "Format '$format' of getPermissions not configured");
                break;
        }
    }

    /**
     * Set permissions to the temp stored value
     * @param Array of strings $permissions
     * @param String $format the format of the returned values
     * @throws CHttpException
     */
    public function setPermissions($permissions, $format = self::PERM_FORMAT_COMMA_SEPARATED)
    {
        switch ($format) {
            case self::PERM_FORMAT_COMMA_SEPARATED:
                $this->_permissions = explode(',', $permissions);
                break;
            case self::PERM_FORMAT_JSON:
                $this->_permissions = CJSON::decode($permissions);
                break;
            case self::PERM_FORMAT_ARRAY:
                $this->_permissions = $permissions;
                break;
            case self::PERM_FORMAT_STRING:
                $this->_permissions = array($permissions);
                break;
            default:
                throw new CHttpException(500, "Format '$format' of setPermissions not configured");
                break;
        }
    }

    /**
     * Check if permission/permissions are available
     * @param Mixed $permissions String to check a specific permission, Array to check many permissions
     * @param String $format the format of the returned values
     * @throws CHttpException
     */
    public function checkAccess($memberPermissions, $format = self::PERM_FORMAT_COMMA_SEPARATED)
    {
        switch ($format) {
            case self::PERM_FORMAT_COMMA_SEPARATED:
                $permissions = explode(',', $memberPermissions);
            case self::PERM_FORMAT_JSON:
                if(!isset($permissions))
                    $permissions = CJSON::decode($memberPermissions);
            case self::PERM_FORMAT_ARRAY:
                if(!isset($permissions))
                    $permissions = $memberPermissions;
            case self::PERM_FORMAT_STRING:
                if(!isset($permissions))
                    $permissions = array($memberPermissions);
                $result = array_diff($permissions, $this->_permissions);
                break;
            default:
                throw new CHttpException(500, "Format '$format' of checkPermissions not configured");
                break;
        }
        YII_DEBUG && Yii::trace(
            'Check '.(empty($result) ? 'succeeded' : 'failed').' for needed permissions '.CJSON::encode($permissions).' on '.CJSON::encode($this->_permissions),
            'app.components.baseAuth'
        );
        return empty($result);
    }

    /**
     * TODO: Make a different process for authentication and authorization
     */
    public function askPermissions(){
        YII_DEBUG && Yii::trace(
            'Redirect to '.$this->_name.' to ask for needed permissions',
            'app.components.baseAuth'
        );
        $this->login();
    }

    /**
     * @return string short name of this auth provider ('facebook', 'linkedin', ...)
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @return mixed the User ID on the external site or null if user is not logged in
     * @param bool $refresh wether to refresh the user ID from the API
     */
    public function getUserId($refresh=false)
    {
        if($this->_userId===false || $refresh)
            $this->_userId=$this->loadUserId();
        return $this->_userId;
    }

    /**
     * @return mixed the AuthUserProfile object or null if user is not logged in
     */
    public function getUserProfile()
    {
        if($this->_userProfile===false)
            $this->_userProfile=$this->loadUserProfile();
        return $this->_userProfile;
    }

    /**
     * @return mixed the URL to the user photo or null if the provider has not photo
     */
    public function getPhotoUrl()
    {
        if($this->_photoUrl===false)
            $this->_photoUrl=$this->loadPhotoUrl();
        return $this->_photoUrl;
    }

    /**
     * @return array of new (unsaved) MemExperience records
     */
    public function getExperiences()
    {
        if($this->_experiences===null)
            $this->_experiences=$this->loadExperiences();
        return $this->_experiences;
    }

    /**
     * @return array of new (unsaved) MemEducation records
     */
    public function getEducations()
    {
        if($this->_educations===null)
            $this->_educations=$this->loadEducations();
        return $this->_educations;
    }

    /**
     * @return array of new (unsaved) MemSkill records
     */
    public function getSkills()
    {
        if($this->_skills===null)
            $this->_skills=$this->loadSkills();
        return $this->_skills;
    }

    /**
     * @return array of new (unsaved) MemRecommendation records
     */
    public function getRecommendations()
    {
        if($this->_recommendations===null)
            $this->_recommendations=$this->loadRecommendations();
        return $this->_recommendations;
    }

    /**
     * @return string current URL including callback flag
     */
    public function getCallbackUrl()
    {
        $params=$_GET;
        $params[self::CALLBACK_PARAM]='1';
        array_unshift($params,Yii::app()->controller->route);

        return url($params,null,true);
    }

    /**
     * @return string the serialized authentication token data (e.g. to store in DB) or null if not authenticated
     */
    public function serialize()
    {
        if(($token=$this->getAccessToken())!==null)
            return serialize(array(
                'uid'   =>$this->getUserId(),
                'token' =>$token,
            ));
    }

    /**
     * @param string serialized authentication token data (e.g. from DB) that should be restored in provider
     */
    public function unserialize($data)
    {
        $data=unserialize($data);
        if(isset($data['token']))
            $this->setAccessToken($data['token']);
    }

}

/**
 * AuthUserProfile
 *
 * This class is a container for user profile data from an external site.
 *
 * It extends from CFormModel to use some of its convenience methods, e.g.
 * to retrieve all member related attributes at once in scenario 'member';
 *
 * So to assign all profile attributes to a Member object we can:
 *
 *  $profile=$provider->profile;
 *  $profile->scenario='member';
 *  $member->attributes=$profile->attributes;
 *
 * NOTE 1: You should use a scenario for $member above, to make sure that only
 *         the attributes you want are overwritten!
 *
 * NOTE 2: Not all properties are available from all provider
 */
class AuthUserProfile extends CFormModel
{

    public $provider;
    public $external_id;

    public $fname;
    public $lname;
    public $email;
}
