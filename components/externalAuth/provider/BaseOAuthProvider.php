<?php
/**
 * BaseOAuthProvider
 *
 * This is the base class for all OAuth based providers. It sets up the OAuth object,
 * and implements the generic OAuth authentication process.
 *
 * IMPORTANT: Make sure that names of extended classes end with "AuthProvider"!
 */
abstract class BaseOAuthProvider extends BaseAuthProvider
{
    /**
     * @var string $consumerKey OAuth consumer key
     */
    public $consumerKey;

    /**
     * @var string $consumerSecret OAuth consumer
     */
    public $consumerSecret;

    /**
     * @return string the URL to retrieve the OAuth request token from the provider
     */
    public abstract function getRequestTokenUrl();

    /**
     * @return string the URL to retrieve the OAuth access token from the provider
     */
    public abstract function getAccessTokenUrl();

    /**
     * @param mixed $token oauth_token from request token
     * @param mixed $token_secret oauth_token_secret from request token
     * @return string the URL to redirect the user to for authentication
     */
    public abstract function getLoginUrl($token,$token_secret);

    /**
     * Set up OAuth as API object
     */
    public function init()
    {
        if(!extension_loaded('oauth'))
            throw new CException('OAuth extension is required for OAuth based AuthProviders!');

        parent::init();
        $oauth=new OAuth(
            $this->consumerKey,
            $this->consumerSecret
        );
        $oauth->setRequestEngine(OAUTH_REQENGINE_CURL);
        $this->setApi($oauth);
    }

    /**
     * Redirect to OAuth provider's login page (will end script execution!)
     *
     * @param string $callbackUrl URL to call after successful login
     */
    public function redirectToLogin($callbackUrl)
    {
        $oauth=$this->getApi();

        YII_DEBUG && $oauth->enableDebug();

        // Retrieve a request token
        try {
            $t=$oauth->getRequestToken($this->getRequestTokenUrl(),$callbackUrl);

            YII_DEBUG && Yii::trace(sprintf('Try to get request token from %s: %s',
                $this->name,
                var_export($oauth->debugInfo,true)
            ),'app.auth.oauth');

            if($t===false)
                $this->addError('Could not get OAuth request token');
        } catch (OAuthException $e) {
            $this->addError($e->getMessage());
            return false;
        }

        // Save request token
        $this->setPersistentToken('request',$t);

        // Redirect to OAuth provider's login page
        Yii::app()->request->redirect(
            $this->getLoginUrl($t['oauth_token'],$t['oauth_token_secret'])
        );
    }

    /**
     * Process the authentication callback request from external site
     *
     * @return bool wether authentication was successful
     */
    public function processLoginCallback()
    {

        // We should have a request token. And we can delete it at this point.
        $token=$this->getPersistentToken('request',false);
        $this->setPersistentToken('request',null);

        if($token===false || !isset($_GET['oauth_token'],$_GET['oauth_verifier']))
            return false;

        // Set request token in OAuth
        $oauth=$this->getApi();
        YII_DEBUG && $oauth->enableDebug();
        $oauth->setToken($_GET['oauth_token'],$token['oauth_token_secret']);

        // oauth_verifier is passed automatically if present in $_GET
        $a=$oauth->getAccessToken($this->getAccessTokenUrl());

        YII_DEBUG && Yii::trace(sprintf('Try to get access token from %s: %s',
            $this->name,
            var_export($oauth->debugInfo,true)
        ),'app.auth.oauth');


        if($a===false)
            return false;

        $this->setPersistentToken('access',$a);

        return true;
    }

    /**
     * Logout the user by deleting their OAuth access token
     *
     * @return bool wether logout was successful
     */
    public function logout()
    {
        $this->setPersistentToken('request',null);
        $this->setPersistentToken('access',null);
        return true;
    }

    /**
     * @return mixed the access token for the current user session or null if not connected
     */
    public function getAccessToken()
    {
        if(($id=$this->getUserId())!==null)
            return $this->getPersistentToken('access');
    }

    /**
     * @param string $token the access token for the current user session
     */
    public function setAccessToken($token)
    {
        $this->api()->setPersistentToken('access',$token);
    }

    /**
     * Override BaseAuthProvider::getApi(). The returned OAuth object will have
     * access tokens already set, if there are any in our persistent storage.
     *
     * @return OAuth the OAuth object for this provider
     */
    public function getApi()
    {
        $oauth=parent::getApi();

        if(($t=$this->getPersistentToken('access',false))!==false)
            $oauth->setToken($t['oauth_token'],$t['oauth_token_secret']);

        return $oauth;
    }

    public function getUserId($refresh=false)
    {
        if($this->getPersistentToken('access')!==null)
            return parent::getUserId($refresh);
        return null;
    }

    /**
     * @param string $name token name (e.g. 'request', 'access')
     * @param mixed $default optional default value, if no value is set. Defaults to null.
     * @return mixed a token array with keys 'oauth_token' and 'oauth_token_secret' or null if none
     */
    public function getPersistentToken($name,$default=null)
    {
        return Yii::app()->user->getState($this->getPersistentTokenKey($name),$default);
    }

    /**
     * @param string $name token name to save (e.g. 'request', 'access')
     * @param mixed $value to save in persistent storage
     */
    public function setPersistentToken($name,$value)
    {
        Yii::app()->user->setState($this->getPersistentTokenKey($name),$value);
    }

    /**
     * @param string $name token name (e.g. 'request', 'access')
     * @return string key for persistent storage e.g. 'oauth.linkedin.access'
     */
    public function getPersistentTokenKey($name)
    {
        return sprintf('oauth.%s.%s',$this->getName(),$name);
    }

}
