<?php

/**
 * FacebookAuthProvider
 *
 * This is the AuthProvider to perform Facebook authentication.
 */
class FacebookAuthProvider extends BaseAuthProvider {

    const FB_API_ALIAS = 'application.lib.facebook.facebook';
    const CANVAS_BASE_URL = 'https://apps.facebook.com/';
    const PHOTO_URL = 'https://graph.facebook.com/%s/picture';

//    const PHOTO_URL = 'https://graph.facebook.com/%s/picture?type=large';

    /**
     * @var string FB application ID
     */
    public $applicationId;

    /**
     * @var string FB consumer_secret param
     */
    public $consumerSecret;

    /**
     * @var string FB namespace
     */
    public $canvas;

    /**
     * @var string FB namespace
     */
    public $namespace;

    /**
     * @var string Url callback
     */
    public $customCallbackUrl;

    /**
     * @var string FB admins
     */
    public $admins;
    // http://developers.facebook.com/docs/reference/api/permissions/
    private $_permissionsNeeded = array(
        'email', // Not user-revokable
        'publish_stream', // Extended Permission
        'publish_actions'           // Open Graph Permission
    );
    private $_fbMe;

    /**
     * Set up Facebook API object
     */
    public function init() {
        parent::init();
        Yii::import(self::FB_API_ALIAS, true);
        $this->setApi(new Facebook(array(
                    'appId' => $this->applicationId,
                    'secret' => $this->consumerSecret,
                    'canvas' => (int) $this->canvas,
                )));
    }

    /**
     * Redirect to Facebook login page (will end script execution!)
     *
     * @param string $callbackUrl URL to call after successful login
     */
    public function redirectToLogin($callbackUrl) {
        if ($this->customCallbackUrl)
            $url = $this->customCallbackUrl;
        else
            $url = $this->getApi()->getLoginUrl(array(
                'client_id' => $this->applicationId,
                'scope' => $this->_permissionsNeeded,
                'redirect_uri' => $callbackUrl,
                    ));
        Yii::log("redirectToLogin: " . $url, CLogger::LEVEL_INFO, 'app.components.facebookAuth');
        if ($this->canvas) {
            echo "<script> top.location.href='" . $url . "'</script>";
            Yii::app()->end();
        } else {
            Yii::app()->request->redirect($url);
        }
    }

    /**
     * @return string current URL including callback flag
     */
    public function getCallbackUrl() {
        if ($this->canvas) {
            return self::CANVAS_BASE_URL . $this->namespace;
        }

        return parent::getCallbackUrl();
    }

    /**
     * @return bool wether this is a callback request. (Only useful when login() is used)
     */
    public function getIsCallback() {
        Yii::log("getIsCallback -- extId: " . json_encode($this->getUserId()) . ' -- canvas: ' . json_encode($this->canvas), 'app.components.facebookAuth');
        if ($this->canvas) {
            return $this->getUserId() ? true : false;
        }

        return isset($_GET[self::CALLBACK_PARAM]) && $_GET[self::CALLBACK_PARAM] === '1';
    }

    /**
     * Process the authentication callback request from external site
     *
     * @return bool wether authentication was successful
     */
    public function processLoginCallback() {
        YII_DEBUG && Yii::log((isset($_GET['code']) ? 'Code' : 'No code') . ' available from callback', CLogger::LEVEL_TRACE, 'app.auth.fb');

        if ($this->getUserId() !== null)
            return true;

        if (isset($_GET['error']))
            $this->addError($_GET['error']);

        return false;
    }

    /**
     * Logout authenticated user
     *
     * @return bool wether logout was successful
     */
    public function logout() {
        $api = $this->getApi();
        $request = Yii::app()->request;
        $cookies = $request->cookies;

        // Reverse engineered Facebook SESSION and COOKIE data (SDK is missing reset feature!):
        $session = Yii::app()->session;
        $session->remove("fb_{$this->applicationId}_code");
        $session->remove("fb_{$this->applicationId}_access_token");
        $session->remove("fb_{$this->applicationId}_user_id");
        $session->remove("fb_{$this->applicationId}_state");

        // FB adds a '.' before the cookie domain. We must match it to delete it:
        if (isset($request->secondLevelDomain)) {
            $domain = '.' . $request->secondLevelDomain;
            if (($c = $cookies->itemAt("fbsr_{$this->applicationId}")) !== null)
                $c->domain = $domain;
            if (($c = $cookies->itemAt("fbm_{$this->applicationId}")) !== null)
                $c->domain = $domain;
        }
        $request->cookies->remove("fbsr_{$this->applicationId}");
        $request->cookies->remove("fbm_{$this->applicationId}");

        // TODO Only required for legacy (FB import)
        $session->remove('facebook');

        $api->destroySession();
        $this->loadUserProfile();
        return true;
    }

    /**
     * @return mixed the access token for the current user session or null if not connected
     */
    public function getAccessToken() {
        if (($id = $this->getUserId()) !== null)
            return $this->getApi()->getAccessToken();
    }

    /**
     * @param string $token the access token for the current user session
     */
    public function setAccessToken($token) {
        $this->getApi()->setAccessToken($token);
    }

    /**
     * Get permissions from the stored value or make the provider ask for them
     * @param String $format the format of the returned values
     * @param bool $forceUpdate ONLY USED in each specific provider
     * @return mixed depending on the chosen $format
     * @throws CHttpException
     */
    public function getPermissions($format = self::PERM_FORMAT_COMMA_SEPARATED, $forceUpdate = false) {
        if ($forceUpdate)
            $this->_permissions = $this->loadPermissions();

        return parent::getPermissions($format, $forceUpdate);
    }

    /**
     * @return mixed Facebook user ID or null if user is not logged in
     */
    protected function loadPermissions() {
        $response = $this->request('/me/permissions', 'get');
        $permissions = array();
        if (!$this->hasErrors() && isset($response['data'])) {
            $permissions = array_keys($response['data'][0]);
            if (in_array('installed', $permissions))
                $permissions = array_unique(array_merge(array(
                            'email', // Not user-revokable
                            'user_work_history', // Not user-revokable
                            'user_education_history', // Not user-revokable
                            'user_location', // Not user-revokable
                            'user_about_me', // Not user-revokable
                                ), $permissions));
        }
        return $permissions;
    }

    /**
     * @return mixed Facebook user ID or null if user is not logged in
     */
    protected function loadUserId() {
        $api = $this->getApi();

        try {
            $id = $api->getUser();
        } catch (FacebookApiException $e) {
            $fbException = $e->getResult();
            $this->addError(sprintf("%s [Code: %s]", $fbException['error']['message'], $fbException['error']['code']));
            $id = 0;
        }

        YII_DEBUG && Yii::log(
                        'Loading user ID via API: ' . ($id != 0 ? 'success' : 'failed'), CLogger::LEVEL_INFO, 'app.auth.fb'
        );

        return ($id != 0) ? $id : null;
    }

    /**
     * @return array Facebook profile as returned by api('/me')
     */
    protected function getFbMe() {
        if ($this->_fbMe === null || $this->_fbMe['id'] !== $this->getUserId()) {
            try {
                $this->_fbMe = $this->getApi()->api('/me');
            } catch (FacebookApiException $e) {
                $fbException = $e->getResult();
                $this->addError(sprintf("%s [Code: %s]", isset($fbException['error']['message']) ? $fbException['error']['message'] : 'Uknknown message', isset($fbException['error']['code']) ? $fbException['error']['code'] : 'Uknknown code'
                        ));
                $errorCode = isset($fbException['error']['code']) ? $fbException['error']['code'] : null;
                $this->_fbMe = null;
            }

            Yii::trace(
                    'Loading profile via API: ' . (isset($this->_fbMe['id']) ? 'success' : 'failed (' . implode(',', $this->errors) . ')'), 'app.auth.fb'
            );

            if (!isset($this->_fbMe['id']))
                $this->_fbMe = null;
        }

        Yii::trace('getFbMe: ' . ($this->_fbMe ? $this->_fbMe['id'] : 'NULL'), 'app.auth.fb');
        return $this->_fbMe;
    }

    /**
     * Make a request to FB api and return the response
     * @param Array $params mixed type $params used in request
     * @return mixed null on failure, FB response object otherwise
     */
    public function request($url, $method, $options = array()) {
        if (($fb = $this->getFbMe()) === null) {
            Yii::trace('Request to API failed - User not logged in - url: ' . $url . ' options:' . CJSON::encode($options), 'app.auth.fb');
            return null;
        }

        try {
            $response = $this->getApi()->api($url, $method, $options);
        } catch (FacebookApiException $e) {
            Yii::log('FacebookApi exception ' . CJSON::encode($e->getResult()), CLogger::LEVEL_INFO, 'app.auth.fb');
            $fbException = $e->getResult();
            $this->addError(sprintf("%s [Code: %s]", $fbException['error']['message'], $fbException['error']['code']));
            $response = false;
        }

        YII_DEBUG && Yii::trace('Request to API: ' . ($response !== false ? 'success (' . CJSON::encode($response) . ')' : 'failed (' . CJSON::encode($this->errors) . ')') . ' - url: ' . $url . ' - options: ' . CJSON::encode($options), 'app.auth.fb');
        return $response;
    }

    /**
     * @return mixed the AuthUserProfile for the FB user or null if user is not logged in
     */
    protected function loadUserProfile() {
        if (($fb = $this->getFbMe()) === null)
            return null;

        $profile = new AuthUserProfile;
        $profile->provider = 'facebook';

        $fb2profile = array(
            'id' => 'external_id',
            'first_name' => 'fname',
            'last_name' => 'lname',
            'email' => 'email',
        );

        foreach ($fb2profile as $f => $p)
            if (isset($fb[$f]))
                $profile->$p = $fb[$f];

        return $profile;
    }

    /**
     * @return mixed the URL to the user photo or null if none
     */
    protected function loadPhotoUrl() {
        return ($id = $this->getUserId()) !== null ? sprintf(self::PHOTO_URL, $id) : null;
    }

    /**
     * Load user experiences from external site through API
     *
     * @return array of new MemExperience records
     */
    protected function loadExperiences() {
        if (($fb = $this->getFbMe()) === null)
            return array();

        $ret = array();

        if (isset($fb['work']))
            foreach ($fb['work'] as $i) {
                $r = new MemExperience;
                $r->setAttributes(array(
                    'company' => isset($i['employer']['name']) ? $i['employer']['name'] : '',
                    'role' => isset($i['position']['name']) ? $i['position']['name'] : '',
                    'description' => isset($i['description']['name']) ? $i['description']['name'] : '',
                    'started_at' => (isset($i['start_date']) && $i['start_date'] > 0 ) ?
                            "$i[start_date]-01" : '0000-00-00',
                    'ended_at' => (isset($i['end_date']) && $i['end_date'] > 0 ) ?
                            "$i[end_date]-01" : '0000-00-00',
                        ), false);
                $ret[] = $r;
            }

        return $ret;
    }

    /**
     * Load user educations from external site through API
     *
     * @return array of new MemEducation records
     */
    protected function loadEducations() {
        if (($fb = $this->getFbMe()) === null)
            return array();

        $ret = array();

        if (isset($fb['education']))
            foreach ($fb['education'] as $i) {
                $r = new MemEducation;
                $r->setAttributes(array(
                    'degree' => isset($i['degree']['name']) ? $i['degree']['name'] : '',
                    'school' => isset($i['school']['name']) ? $i['school']['name'] : '',
                    'started_at' => '0000-00-00',
                    'ended_at' => isset($i['year']['name']) ? "$i[year][name]-01-01" : '0000-00-00',
                        ), false);
                if (isset($i['concentration'])) {
                    $fields = array();
                    foreach ($i['concentration'] as $c)
                        if (isset($c['name']))
                            $fields[] = $c['name'];

                    // Create field of study like "a", "a & b" or "a, b & c";
                    if (($last = array_pop($fields)) === null)
                        $r->field_of_study = '';
                    elseif (count($fields) === 0)
                        $r->field_of_study = $last;
                    else
                        $r->field_of_study = implode(', ', $fields) . ' & ' . $last;
                } else
                    $r->field_of_study = '';

                $ret[] = $r;
            }

        return $ret;
    }

    /**
     * Load user skills from external site through API
     *
     * @return array of new MemSkill records
     */
    protected function loadSkills() {
        return array(); // not provided by FB
    }

    /**
     * Load user recommendations from external site through API
     *
     * @return array of new MemRecommendation records
     */
    protected function loadRecommendations() {
        return array(); // not provided by FB
    }

    public function insideFacebook() {
        return isset($_REQUEST['signed_request']) || Yii::app()->session->get('insideFacebook', false);
    }

    public function insidePage() {
        return ($this->signedRequest !== null && isset($this->signedRequest['page']));
    }

    public function pageLiked() {
//        Yii::log("pageLiked: " . json_encode($this->signedRequest), CLogger::LEVEL_INFO, 'app.components.facebookAuth');
        if($this->signedRequest !== null && isset($this->signedRequest['page']))
            if(isset($this->signedRequest['page']['liked']) && $this->signedRequest['page']['liked'])
                return true;
            else if(isset($this->signedRequest['page']['admin']) && $this->signedRequest['page']['admin'])
                return true;
        return false;
    }

    public function getSignedRequest() {
        return $this->getApi()->getSignedRequest();
    }

    public function getAppData() {
        $appData = array();

        $sources = array();
        if ($this->signedRequest !== null && is_array($this->signedRequest) && isset($this->signedRequest['app_data']))
            $sources[] = $this->signedRequest['app_data'];
        if (isset($_REQUEST['signed_request']) && is_array($_REQUEST['signed_request']) && isset($_REQUEST['signed_request']['app_data']))
            $sources[] = $_REQUEST['signed_request']['app_data'];
        if (isset($_REQUEST['app_data']))
            $sources[] = $_REQUEST['app_data'];

        foreach ($sources as $value) {
            if($this->isJson($value))
                $value = CJSON::decode ($value, true);
            if (!is_array($value))
                $value = array('data' => $value);

            $appData = array_merge($appData, $value);
        }
        return $appData;
    }

    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}
