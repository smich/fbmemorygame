<?php
/**
 * LinkedinAuthProvider
 *
 * This is the OAuth based AuthProvider to perform LinkedIn authentication.
 */
class LinkedinAuthProvider extends BaseOAuthProvider
{
    const URL_REQUEST_TOKEN ='https://api.linkedin.com/uas/oauth/requestToken';
    const URL_ACCESS_TOKEN  ='https://api.linkedin.com/uas/oauth/accessToken';
    const URL_AUTH          ='https://www.linkedin.com/uas/oauth/authenticate?oauth_token=%s';
    const URL_PROFILE       ='https://api.linkedin.com/v1/people/~:(%s)?format=json';

    private $_me;

    /**
     * @return string the URL to retrieve the OAuth request token from the provider
     */
    public function getRequestTokenUrl()
    {
        return self::URL_REQUEST_TOKEN;
    }

    /**
     * @return string the URL to retrieve the OAuth access token from the provider
     */
    public function getAccessTokenUrl()
    {
        return self::URL_ACCESS_TOKEN;
    }

    /**
     * @param mixed $token oauth_token from request token
     * @param mixed $token_secret oauth_token_secret from request token
     * @return string the URL to redirect the user to for authentication
     */
    public function getLoginUrl($token,$token_secret)
    {
        return sprintf(self::URL_AUTH,$token);
    }

    /**
     * @return mixed LinkedIn User id
     */
    protected function loadUserId()
    {
        $oauth=$this->getApi();
        $oauth->fetch(sprintf(self::URL_PROFILE,'id'));
        if($json=$oauth->getLastResponse()) {
            $data=json_decode($json,true);
            if(isset($data['id']))
            {
                // TODO This is only here to make legacy happy:
                if(($a=$this->getPersistentToken('access'))!==null)
                    Yii::app()->session->add('linkedin',array(
                        'token' => $a['oauth_token'],
                        'secret'=> $a['oauth_token_secret'],
                    ));

                return $data['id'];
            } else
                return null;
            return isset($data['id']) ? $data['id'] : null;
        } else
            return null;
    }

    /**
     * @return array Full LinkedIn profile as returned by api
     */
    protected function getMe()
    {
        if($this->_me===null || $this->_me['id']!==$this->getUserId())
        {
            $oauth=$this->getApi();
            $oauth->fetch(sprintf(self::URL_PROFILE,'id,first-name,last-name,main-address,date-of-birth,picture-url,headline,location:(name,country:(code)),phone-numbers,im-accounts,twitter-accounts,educations,positions,recommendations-received,specialties,summary,skills:(skill,proficiency,years),industry'));
            if($json=$oauth->getLastResponse())
                $this->_me=json_decode($json,true);
        }
        return $this->_me;
    }

    /**
     * @return mixed the AuthUserProfile for the FB user or null if user is not logged in
     */
    protected function loadUserProfile()
    {
        if(($li=$this->getMe())==null)
            return null;

        $profile=new AuthUserProfile;
        $profile->provider='linkedin';

        // linkedin -> AuthUserProfile attribute mapping
        $li2profile=array(
            'id'            =>'providerUid',
            'firstName'     =>'fname',
            'lastName'      =>'lname',
            'summary'       =>'about',
            'picture-url'   =>'photo',
            'industry'      =>'industry',
            'mainAddress'   =>'line1',
        );
        foreach($li2profile as $l=>$p)
            if(isset($li[$l]))
                $profile->$p=$li[$l];

        if(isset($li['phoneNumers']['values']))
            foreach($li['phoneNumbers']['values'] as $phone)
                if($phone['type']==='mobile')
                    $mobile=$phone['phoneNumber'];
                else
                    $phone=$phone['phoneNumber'];
        if(isset($mobile,$phone))
            $profile->setAttributes(array(
                'mobile'=>$mobile,
                'phone'=>$phone,
            ),false);
        elseif(isset($mobile))
            $profile->phone=$mobile;
        elseif(isset($phone))
            $profile->phone=$phone;


        if(isset($li['headline']))
            $profile->job_title=preg_replace('/ at .*/','',$li['headline']);

        if(isset($li['specialties']))
            $profile->about= empty($profile->about) ? $li['specialties'] : $profile->about."\n".$li['specialties'];

        if(!empty($profile->industry))
            $profile->industry= Members::IND_OTHER.':'.$profile->industry;

        if(isset($li['location']['name']))
            $profile->area_covered=$li['location']['name'];

        if(isset($li['imAccounts']['values']))
            foreach($li['imAccounts']['values'] as $im)
                if($im['imAccountType']==='skype')
                    $profile->skype_id=$im['imAccountName'];
                else
                    $profile->im_id="$im[imAccountType]: $im[imAccountName]";

        if(isset($li['twitterAccounts']['values'][0]))
            $profile->others_id=$li['twitterAccounts']['values'][0]['providerAccountName'];

        return $profile;
    }

    /**
     * @return mixed the URL to the user photo or null if none
     */
    protected function loadPhotoUrl()
    {
        if(($li=$this->getMe())===null)
            return null;

        return isset($li['pictureUrl']) ? $li['pictureUrl'] : null;
    }

    /**
     * Load user experiences from external site through API
     *
     * @return array of new MemExperience records
     */
    protected function loadExperiences()
    {
        if(($li=$this->getMe())===null)
            return array();

        $ret=array();

        if(isset($li['positions']['values']))
            foreach($li['positions']['values'] as $p)
            {
                $r=new MemExperience;
                $r->setAttributes(array(
                    'role'          =>isset($p['title']) ? $p['title'] : '',
                    'description'   =>isset($p['summary']) ? $p['summary'] : '',
                    'company'       =>isset($p['company']['name']) ? $p['company']['name'] : '',
                    'started_at'    =>isset($p['startDate']) ? $this->importDate($p['startDate']) : '0000-00-00',
                    'ended_at'      =>isset($p['endDate']) ? $this->importDate($p['endDate']) : '0000-00-00',
                ),false);
                $ret[]=$r;
            }

        return $ret;
    }

    /**
     * Load user educations from external site through API
     *
     * @return array of new MemEducation records
     */
    protected function loadEducations()
    {
        if(($li=$this->getMe())===null)
            return array();

        $ret=array();

        if(isset($li['educations']['values']))
            foreach($li['educations']['values'] as $e)
            {
                $r=new MemEducation;
                $r->setAttributes(array(
                    'degree'        =>isset($e['degree']) ? $e['degree'] : '',
                    'school'        =>isset($e['schoolName']) ? $e['schoolName'] : '',
                    'started_at'    =>isset($e['startDate']) ? $this->importDate($e['startDate']) : '0000-00-00',
                    'ended_at'      =>isset($e['endDate']) ? $this->importDate($e['endDate']) : '0000-00-00',
                ),false);

                if(isset($e['fieldOfStudy']))
                {
                    // Create field of study like "a", "a & b" or "a, b & c";
                    $fields=explode(',',$e['fieldOfStudy']);
                    $fields=array_map('trim',$fields);      // trim values...
                    $fields=array_filter($fields,'trim');   // .. and remove empty
                    if(($last=array_pop($fields))===null)
                        $r->field_of_study='';
                    elseif(count($fields)===0)
                        $r->field_of_study=$last;
                    else
                        $r->field_of_study=implode(', ',$fields).' & '.$last;
                } else
                    $r->field_of_study='';

                $ret[]=$r;
            }

        return $ret;
    }

    /**
     * Load user skills from external site through API
     *
     * @return array of new MemSkill records
     */
    protected function loadSkills()
    {
        if(($li=$this->getMe())===null)
            return array();

        $ret=array();
        $l=array(
            'beginner'      =>2,
            'intermediate'  =>5,
            'advanced'      =>7,
            'expert'        =>10,
        );

        if(isset($li['skills']['values']))
            foreach($li['skills']['values'] as $s)
                if(isset($s['skill']['name'])) {
                    $r=new MemSkills;
                    $r->name=$s['skill']['name'];
                    $r->setAttributes(array(
                        'level'     =>isset($s['proficiency']['level']) && isset($l[$s['proficiency']['level']]) ? 
                            $l[$s['proficiency']['level']] : null,
                        'exp_years' =>isset($s['years']['id']) ? $s['years']['id'] : '',
                    ),false);
                    $ret[]=$r;
                }

        return $ret;
    }

    /**
     * Load user recommendations from external site through API
     *
     * @return array of new MemRecommendation records
     */
    protected function loadRecommendations()
    {
        if(($li=$this->getMe())===null)
            return array();

        $ret=array();

        if(isset($li['recommendationsReceived']['values']))
            foreach($li['recommendationsReceived']['values'] as $e)
            {
                $r=new MemRecommendation;
                $r->setAttributes(array(
                    'fname'         =>isset($e['recommender']['firstName']) ? $e['recommender']['firstName'] : '',
                    'lname'         =>isset($e['recommender']['lastName']) ? $e['recommender']['lastName'] : '',
                    'comment'       =>isset($e['recommendationText']) ? $e['recommendationText'] : '',
                    'experience_id' =>MemRecommendation::EXP_IMPORTED,
                    'created_at'    =>date('Y-m-d'),
                ),false);

                $ret[]=$r;
            }

        return $ret;
    }

    /**
     * Convert an array like array('year'=>'2009', 'month'=>'10') into '2009-10-01'.
     * Year is mandatory, month/day are replaced by '01' if not present.
     *
     * @param array $value date array
     * @return string the date string in format 'YYYY-MM-DD'
     */
    private function importDate($value)
    {
        if(!isset($value['year']))
            return '0000-00-00';

        $month=isset($value['month']) ? $value['month'] : '01';
        $day=isset($value['day']) ? $value['day'] : '01';

        return "$value[year]-$month-$day";
    }
}
