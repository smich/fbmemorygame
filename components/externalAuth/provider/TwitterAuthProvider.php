<?php
/**
 * TwitterAuthProvider
 *
 * This is the OAuth based AuthProvider to perform Twitter authentication.
 */
class TwitterAuthProvider extends BaseOAuthProvider
{
    const URL_REQUEST_TOKEN ='https://api.twitter.com/oauth/request_token';
    const URL_ACCESS_TOKEN  ='https://api.twitter.com/oauth/access_token';
    const URL_AUTH          ='https://api.twitter.com/oauth/authorize?oauth_token=%s';
    const URL_PROFILE       ='https://api.twitter.com/1/account/verify_credentials.json';

    // Cache the profile to be reused between loadUserId/loadUserProfile
    private $_profile;

    /**
     * @var string Twitter application ID
     */
    public $applicationId;

    /**
     * @return string the URL to retrieve the OAuth request token from the provider
     */
    public function getRequestTokenUrl()
    {
        return self::URL_REQUEST_TOKEN;
    }

    /**
     * @return string the URL to retrieve the OAuth access token from the provider
     */
    public function getAccessTokenUrl()
    {
        return self::URL_ACCESS_TOKEN;
    }

    /**
     * @param mixed $token oauth_token from request token
     * @param mixed $token_secret oauth_token_secret from request token
     * @return string the URL to redirect the user to for authentication
     */
    public function getLoginUrl($token,$token_secret)
    {
        return sprintf(self::URL_AUTH,$token);
    }

    /**
     * @return mixed Twitter User id
     */
    protected function loadUserId()
    {
        if($this->_profile===false)
            $this->loadUserProfile();

        return $this->_profile!==null && !empty($this->_profile->id);
    }

    /**
     * @return mixed the AuthUserProfile for the FB user or null if user is not logged in
     */
    protected function loadUserProfile()
    {
        $oauth=$this->getApi();
        $oauth->fetch(self::URL_PROFILE);
        if($json=$oauth->getLastResponse())
        {
            $twitter=json_decode($json,true);

            $this->_profile=new AuthUserProfile;
            $this->_profile->provider='twitter';

            $twitter2profile=array(
                'id'                =>'providerUid',
                'description'       =>'about',
                'profile_image_url' =>'photo',
                'url'               =>'website',
            );
            foreach($twitter2profile as $t=>$p)
                if(isset($twitter[$t]))
                    $this->_profile->$p=$twitter[$t];

            if(isset($twitter['name']))
            {   // Try to guess fname/lname
                $names=split(' ',$twitter['name']); // array('fname1', 'fname2', 'fname3', 'lname')
                $this->_profile->lname = isset($names[1]) ? array_pop($names) : '';
                $this->_profile->fname = implode(' ',$names);
            }

            return $this->_profile;
        }
        else
            return null;
    }

    protected function loadPhotoUrl()
    {
        return array(); // not provided by Twitter
    }

    protected function loadExperiences()
    {
        return array(); // not provided by Twitter
    }

    protected function loadSkills()
    {
        return array(); // not provided by Twitter
    }

    protected function loadEducations()
    {
        return array(); // not provided by Twitter
    }

    protected function loadRecommendations()
    {
        return array(); // not provided by Twitter
    }
}
