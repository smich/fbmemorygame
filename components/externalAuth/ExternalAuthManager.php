<?php
/**
 * ExternalAuthManager
 *
 * This application component manages authentication provider (AuthProvider).
 * The provider can be configured through the {@link provider} property.
 * Use {@link getProvider()} to obtain a configured provider object.
 */
class ExternalAuthManager extends CApplicationComponent
{
    /**
     * List of provider configuration indexed by provider name:
     *
     *      array(
     *          'facebook'=>array(
     *              'applicationId' =>'Your FB Application ID',
     *              'consumerSecret'=>'Your FB Consumer Secret',
     *          ),
     *          'linkedin'=>array(
     *              'consumerKey'   =>'Your LinkedIn appKey',
     *              'consumerSecret'=>'Your LinkedIn appSecret',
     *          ),
     *      );
     *
     * See the respective provider classes for all configuratoin options.
     *
     * @var array provider configurations
     */
    public $provider=array();

    // provider
    private $_p=array();

    /**
     * Import directory with provider classes
     */
    public function init()
    {
        YiiBase::import('application.components.externalAuth.BaseAuthProvider');
        YiiBase::import('application.components.externalAuth.provider.*');
    }

    public function getProvider($name)
    {
        if(!isset($this->_p[$name]))
            $this->_p[$name]=$this->createProvider($name);
        return $this->_p[$name];
    }

    /**
     * Create and init a specific AuthProvider.
     *
     * @param string $name of AuthProvider
     * @return AuthProvider the created AuthProvider object
     */
    public function createProvider($name)
    {
        $config=isset($this->provider[$name]) ? $this->provider[$name] : array();

        if(!isset($config['class']))
            $config['class']=sprintf(
                'application.components.externalAuth.provider.%sAuthProvider',
                ucfirst($name)
            );

        $provider=YiiBase::createComponent($config);
        $provider->init();
        return $provider;
    }
}
