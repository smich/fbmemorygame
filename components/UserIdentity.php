<?php
/**
 * UserIdentity
 *
 * This class represents a user identity and can authenticate a Member.
 * After successful authencition or when the identity was created through
 * createAuthenticated(), getMember() will return the related Members object.
 */
class UserIdentity extends CUserIdentity
{
    const ERROR_USER_INACTIVE=20;

    private $_id;
    private $_member;

    /**
     * Authenticates a user.
     *
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $this->_member=Member::model()->findByAttributes(array('email'=>$this->username));

        if ($this->_member===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        elseif(!$this->_member->active)
            $this->errorCode=self::ERROR_USER_INACTIVE;
        else {
            $this->_id=$this->_member->mem_id;
            $this->errorCode=self::ERROR_NONE;
        }

        return $this->errorCode===self::ERROR_NONE;
    }

    /**
     * @return mem_id of logged in user
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return mixed Member record of member to authenticate or null if not found
     */
    public function getMember()
    {
        return $this->_member;
    }

    /**
     * Check if a user is Active
     *
     * @return boolean whether checks succeed.
     */
    public function isUserValid()
    {
        return $this->_member->active;
    }

    /**
     * @param mixed Member object or mem_id for the user
     * @return UserIdentity an authenticated identity for this user or null if unknown mem_id
     */
    public static function createAuthenticated($member)
    {
        if(!is_object($member))
            $member=Member::model()->findByPk((int)$member);

        if($member===null)
            return null;

        $id=new self($member->id,'');
        $id->_member=$member;
        $id->_id=$member->id;
        $id->username=$member->email;
        $id->errorCode=self::ERROR_NONE;
        return $id;
    }
}
