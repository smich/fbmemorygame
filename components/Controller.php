<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    private $_externalAuthProvider = array('facebook');

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /**
     * @param string $name name of a valid external authentication provider
     * @return object a provider object for external authentication
     */
    public function getAuthProvider($name) {
        if (!in_array($name, $this->_externalAuthProvider))
            throw new CHttpException(400, 'Unsupported authentication mechanism');

        if (($provider = Yii::app()->externalAuth->getProvider($name)) === null)
            throw new CHttpException(500, 'Authentication provider not configured');

        return $provider;
    }

    /**
     * Render a JSON response with correct Content-type header.
     *
     * This method will also supress all log route output via {@link disableLogOutput}.
     *
     * @param mixed $data the data to render as JSON. Usually an array or object.
     * @param bool $endApplication wether to end the application afterwards. Defaults to false.
     * @param mixed $status an optional HTTP status to send, e.g. '404 Not found'.
     */
    public function renderJSON($data, $endApplication = false, $status = null) {
        header('Content-type: application/json');
        if ($status !== null)
            header('HTTP/1.0 ' . $status);
        $this->layout = false;
        echo CJSON::encode($data);
        $this->disableLogOutput = true;
        if ($endApplication) {
            $this->afterAction($this->action);
            Yii::app()->end();
        }
    }

    /**
     * Redirects the browser to the specified URL or route (controller/action).
     * In addition, before redirecting ,it flushes the log messages so as not
     * to loose them.
     * @param mixed $url the URL to be redirected to. If the parameter is an array,
     * the first element must be a route to a controller action and the rest
     * are GET parameters in name-value pairs.
     * @param boolean $terminate whether to terminate the current application after calling this method
     * @param integer $statusCode the HTTP status code. Defaults to 302. See {@link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html}
     * for details about HTTP status code.
     */
    public function redirect($url, $terminate = true, $statusCode = 302) {
        if($terminate)
            Yii::getLogger()->flush();
        parent::redirect($url, $terminate, $statusCode);
    }

}