<?php
/**
 * ClientScriptPackageCompressor
 *
 * This class extends CClientScript and implements JS/CSS compression of packages
 * and registration of JsModule/JsPage configurations.
 *
 * If {@link $enablePackageCompression} is <tt>true</tt>, every package registered
 * through {@link registerPackage} will be compressed. The packages are defined in
 * the configuration. Js and CSS are handled separately. If a package contains both,
 * then 2 separate compressed files for Js and CSS will be created for this package.
 *
 *
 * # DETAILS ON OPERATION:
 *
 * After combining and compressing all files into one, this file will be published
 * through the asset manager. Furthermore some information about the compressed
 * package files is saved to global state.
 *
 * On subsequent registrations of the same package, if the respective file information
 * is found in global state and the file still exists, the compressed file will be served.
 *
 *
 * ## CDN feature
 *
 * To enable CDN, {@link $cdn} has to be set to a CDN base URL. If a package configuration
 * has set the 'cdn' key to `true`, the $cdn URL will get appended to that package's URL.
 * You can also configure another CDN URL with 'cdn'.
 *
 *  'somePackage' => array(
 *      'baseUrl'=>'/js',
 *      'cdn' => true,   // could also be another CDN URL
 *      'js' => array( ... ),
 *  ),
 *
 * You can't combine this with the S3 feature on the same package!
 *
 *
 * ## S3 feature
 *
 * If the {@link $s3} option is configured, then packages can be uploaded to S3. In
 * this case a package must have the 's3' key set to either true or to the name of
 * the S3 component to use for that package. In {@link $s3Bucket} the default bucket
 * of the S3 component can be overridden. In addition each package can set the
 * bucket through an 's3Bucket' key:
 *
 *  'somePackage' => array(
 *      'baseUrl'=>'/js',
 *      's3' => true,   // could also be a bucket name
 *      'js' => array( ... ),
 *  ),
 *
 * You can not combine this with the CDN feature on the same package.
 *
 * Note that we assume that the virtual hosting feature of S3 is used. This means
 * that the bucket name is also the host name of S3 URLs.
 *
 * The files will be uploaded with optimized cache control headers to use client
 * side caching as much as possible.
 *
 *
 * # MAINTENANCE:
 *
 * This component comes with a maintenace command for yiic in /commands. It can be
 * used to reset the compression status of a package:
 *
 * Reset all compressed packages:
 *
 *   ./yiic pkgComp reset
 *
 * Reset specific package:
 *
 *   ./yiic pkgComp reset --name=<packagename>
 *
 * It's important that the same application name is set in both main.php and console.php.
 */
class ClientScriptPackageCompressor extends CClientScript
{
    /**
     * @var bool wether to enable compression for package
     */
    public $enablePackageCompression=true;

    /**
     * @var string path alias to YUI compressor dir
     */
    public $compressorPath='application.lib.yui-compressor';

    /**
     * @var string name of compressor JAR file
     */
    public $compressorJar='yuicompressor-2.4.6.jar';

    /**
     * @var bool wether to only combine, not compress (useful for debugging)
     */
    public $combineOnly=false;

    /**
     * If this is enabled, during compression all other requests will wait until the compressing
     * process has completed. If disabled, the uncompressed files will be delivered for these
     * requests. This should prevent the thundering herd problem.
     *
     * @var bool wether other requests should pause during compression. Off by default.
     */
    public $blockDuringCompression=false;

    /**
     * If this is set to a CDN base URL, it will be prepended to all package URLs which have
     * 'cdn' set to `true`. The 'cdn' key in the package configuration can also contain a
     * different CDN URL.
     *
     * @var mixed CDN base URL or null to disable the CDN feature completely
     */
    public $cdn;

    /**
     * @var mixed if this is not null and contains the name of a S3Component, packages that have
     * the 's3' option set will be uploaded and served from S3.
     */
    public $s3;

    /**
     * @var mixed the S3 bucket name. If null, the default bucket name of the S3Component is used.
     */
    public $s3Bucket;

    /**
     * @var array names of packages that where registered on current page
     */
    private $_registeredPackages=array();

    /**
     * @var mixed meta information about the compressed packages
     */
    private $_pd;

    /**
     * @var mixed name of Js page, if one was registered
     */
    private $_jsp;

    /**
     * @var mixed array with Js Page configuration
     */
    private $_jsc = array();


    // Locking parameter to prevent parallel compression
    const LOCK_ID='_ClientScriptPackageCompressor';
    const LOCK_TIMEOUT=15;
    // the global conf var we'll use to pass data to JS
    const CONFIG_VAR = '__WWF_CONF_';

    /**
     * Register a Js module for this page.
     *
     * The Js Module must be available in WWF.modules.* namespace. Make sure, you
     * make it available in the current package.
     *
     * @param string $name of Js Module to register
     * @param mixed $config (optional) Array with module configuration
     */
    public function registerJsModule($name,$config=array())
    {
        $m=isset($this->_jsc['modules']) ? $this->_jsc['modules'] : array();

        $m[$name]=isset($m[$name]) ? CMap::mergeArray($m[$name],$config) : $config;

        $this->_jsc['modules']=$m;
    }

    /**
     * Get the master JS config array.
     *
     * Invoked by Controller->renderDynamicJSconfigs
     * which outputs our JS confings on CACHED pages
     *
     * @todo Need to refactor the whole logic as per discussions in #4567
     *
     * @return array
     */
    public function getJSconfigs()
    {
        return $this->_jsc;
    }

    /**
     * Register a Js page for this page
     *
     * The Js Page must be available in WWF.pages.* namespace. Make sure, you
     * make it available in the current package.
     *
     * @param mixed $name of Js Page to register
     * @param mixed $config (optional) Array with Page configuration (including modules overrides).
     */
    public function registerJsPage($name,$config=array())
    {
        if (isset($config['modules']) && isset($this->_jsc['modules']))
            $config['modules']=CMap::mergeArray($this->_jsc['modules'],$config['modules']);

        $this->_jsp=$name;
        $this->_jsc=$config;
    }

    /**
     * Register the Js Page configuration
     */
    private function registerJsConfiguration()
    {
        $page=$this->_jsp===null ? 'Page' : 'pages.'.$this->_jsp;

        $this->registerScript('prepJsPage',
            sprintf('var %s = %s;',self::CONFIG_VAR, CJavaScript::encode($this->_jsc)),
            self::POS_BEGIN
        );
        $this->registerScript('initJsPage',
            sprintf('WWF.%s(%s)',$page, self::CONFIG_VAR),
            self::POS_END
        );

        // Must be last script bound to "ready" event, to e.g. let listviews init first
        $this->registerScript('initHashChange','$(window).trigger("hashchange");',self::POS_READY);
    }

    /**
     * Override CClientScript::registerPackage() to initialize the compression algorithm
     *
     * @param string $name Name of Package to register
     * @return CClientScript the CClientScript object itself
     */
    public function registerPackage($name)
    {
        if ($this->enablePackageCompression && !in_array($name,$this->_registeredPackages))
        {
            // Ensure pages that only use registerPackage (e.g our error page) don't exit early during rendering.
            $this->hasScripts=true;

            $this->_registeredPackages[$name]=$name;

            // Create compressed package if not done so yet
            if(($info=$this->getCompressedInfo($name))===null)
            {
                // Compresssion must only be performed once, even for several parallel requests
                while(!Yii::app()->mutex->lock(self::LOCK_ID,self::LOCK_TIMEOUT))
                    if($this->blockDuringCompression)
                        sleep(1);
                    else
                        return parent::registerPackage($name);

                // We have a Mutex lock, now check if another process already compressed this package
                if ($this->getCompressedInfo($name,true)!==null) {
                    Yii::app()->mutex->unlock();
                    return;
                }

                $this->compressPackage($name);

                Yii::app()->mutex->unlock();

                return $this;
            }
        } else
            return parent::registerPackage($name);
    }

    /**
     * Override CClientScript::render() to add compressed package files if available.
     * We also register Js Module/Page configuration.
     * @param string $output the existing output that needs to be inserted with script tags
     */
    public function render(&$output)
    {
        $this->onBeforeRender();

        // If only a JsModule widget was renderd but no JsPage, we render it here
        // to let it register the default packages defined in JsPage.
        if($this->_jsp===null && $this->_jsc!==array())
            Yii::app()->controller->widget('JsPage',array(),true);

        // Register configuration for JsModule/JsPage
        if ($this->_jsp!==null || $this->_jsc!==array())
            $this->registerJsConfiguration();

        if(!$this->hasScripts)
            return;

        $packages=$this->_registeredPackages;
        if($this->enablePackageCompression)
            foreach($packages as $package)
                $this->unregisterPackagedCoreScripts($package);

        $this->renderCoreScripts();

        if(!empty($this->scriptMap))
            $this->remapScripts();

        // Register package files as *first* files always
        if($this->enablePackageCompression)
            foreach(array_reverse($this->_registeredPackages) as $package)
                $this->renderCompressedPackage($package);

        $this->unifyScripts();
        $this->renderHead($output);
        if($this->enableJavaScript)
        {
            $this->renderBodyBegin($output);
            $this->renderBodyEnd($output);
        }
    }

    /**
     * Delete cached package file
     *
     * @param string (optional) $name of package
     */
    public function resetCompressedPackage($name=null)
    {
        // Log this package reset
        YII_DEBUG && Yii::trace(sprintf("Reset package START. key name: %s", $this->getStateKey()), 'app.components.clientscriptpackagecompressor');

        if($this->_pd===null)
            $this->loadPackageData();

        if($name===null)
            $packages=$this->_pd;
        elseif(isset($this->_pd[$name]))
            $packages=array($name=>$this->_pd[$name]);
        else
            $packages=array();

        if ($packages===array()) {
            YII_DEBUG && Yii::trace(sprintf("Package reset not needed. Key name: %s", $this->getStateKey()), 'app.components.clientscriptpackagecompressor');
            return false;
        }

        foreach($packages as $package => $info)
        {
            if(isset($info['js']['file']))
                @unlink($info['js']['file']);
            if(isset($info['css']['file']))
                @unlink($info['css']['file']);
            if(isset($info['js']['s3']) && ($s3 = Yii::app()->getComponent($this->s3))!==null)
                $s3->delete($info['js']['s3'][0], $info['js']['s3'][1]);
            unset($this->_pd[$package]);
        }

        $this->savePackageData();

        // Log this package reset
        YII_DEBUG && Yii::trace(sprintf("Reset package. Key name: %s", $this->getStateKey()), 'app.components.clientscriptpackagecompressor');

        return true;
    }

    /**
     * This event is raised before this component renders the clientscripts
     *
     * @param CEvent $event the event object
     */
    public function onBeforeRender()
    {
        $event=new CEvent;
        $this->raiseEvent('onBeforeRender',$event);
    }

    /**
     * Remove any registered core scripts and packages if we have it in the package to prevent publishing
     *
     * @param string $name of package
     */
    private function unregisterPackagedCoreScripts($package)
    {
        if (($info=$this->getCompressedInfo($package))===null || !isset($info['js']))
            return;

        // Remove the package itself from the coreScripts or it would
        // still render the uncompressed script files
        unset($this->coreScripts[$package]);

        // Also remove the coreScripts contained in the package
        foreach($info['js']['coreScripts'] as $name) {
            unset($this->coreScripts[$name]);
            unset($this->_registeredPackages[$name]);
        }
    }

    /**
     * If a compressed package is available, will return an array of this format:
     *
     *  array(
     *      'js'=>array(
     *          'file'          =>'/path/to/compressed/file',
     *          'files'         => <list of original file names>
     *          'urls'          => <list of script URLs (incl. external)>
     *          'coreScripts'   => <list of core scripts contained in this package>
     *          's3'            => array( $bucketName, $name )  // wether file is hosted on S3
     *      ),
     *      'css'=>array(
     *          'file'          =>'/path/to/compressed/file',
     *          'files'         => <list of original file names>
     *          'urls'          => <list of script URLs (incl. external)>
     *      ),
     *
     * @param string name of package to load
     * @param bool wether to enforce that package data is read again from global state
     * @return mixed array with compressed package information or null if none
     */
    public function getCompressedInfo($name,$forceRefresh=false)
    {
        if ($this->_pd===null || $forceRefresh)
            $this->loadPackageData($forceRefresh);

        $i=isset($this->_pd[$name]) ? $this->_pd[$name] : null;

        // Safety check: Verify that compressed files exist
        if( isset($i['js']['file']) && !isset($i['js']['s3']) && !file_exists($i['js']['file']) ||
            isset($i['css']['file']) && !file_exists($i['css']['file']))
        {
            $this->setCompressedInfo($name,null);
            YII_DEBUG && Yii::trace(
                sprintf(
                    "Remove %s:\n%s\n%s",
                    $name,
                    isset($i['js']) ? $i['js']['file'] : '-',
                    isset($i['css']) ? $i['css']['file'] : '-'
                ),
                'app.components.clientscriptpackagecompressor'
            );
            $i=null;
        }

        return $i;
    }

    /**
     * @return array list of compressed package names
     */
    public function getCompressedPackageNames()
    {
        if ($this->_pd===null)
            $this->loadPackageData();

        return array_keys($this->_pd);
    }

    /**
     * Get a checksum of the contents of all packages.
     * Example usage: to use as a cache key in full-page caching.
     *
     * @param Boolean $debug If true print the whole package out rather than the numeric checksum.
     *
     * @return Integer Unique ID of packages contents
     */
    public function getChecksum($debug=false)
    {
        $returnArr = array();

        if ($this->_pd===null)
            $this->loadPackageData();

        foreach($this->_pd as $package => $info) {
            // For info of the package structure see comments in getCompressedInfo
            if(isset($info['js']['file']))
                $returnArr[] = $info['js']['file'];
            if(isset($info['css']['file']))
                $returnArr[] = $info['css']['file'];
        }

        // Sort the array because having a different order will change the checksum
        sort($returnArr);
        $fileStr = join("\n", $returnArr);

        return ($debug ? $fileStr : crc32($fileStr));
    }

    /**
     * Stores meta information about compressed package to cache and global state
     *
     * @param mixed Array of format array('file'=>...,'urls'=>...) or null to reset
     */
    private function setCompressedInfo($name,$value)
    {
        if($this->_pd===null)
            $this->loadPackageData();

        if($value!==null)
            $this->_pd[$name]=$value;
        elseif(isset($this->_pd[$name]))
            unset($this->_pd[$name]);

        $this->savePackageData();
    }

    /**
     * Load meta information about compressed packages from global state
     *
     * @param bool wether to enforce that global state is refreshed
     */
    private function loadPackageData($forceRefresh=false)
    {
        // Make sure, statefile is read in again. It could have been changed from
        // another request, while we were waiting for the mutex lock
        if ($forceRefresh)
            Yii::app()->loadGlobalState();

        $this->_pd=Yii::app()->getGlobalState($this->getStateKey(),array());
    }

    /**
     * Save meta information about compressed packages to global state
     */
    private function savePackageData()
    {
        Yii::app()->setGlobalState($this->getStateKey(),$this->_pd);

        // We want to be sure, global state is written immediately. Default would be onEndRequest.
        Yii::app()->saveGlobalState();
    }


    /**
     * Replace all sripts registered at $coreScriptPosition with compressed file
     */
    private function renderCompressedPackage($name)
    {
        if(($package=$this->getCompressedInfo($name))===null)
            return;

        if(isset($package['js']))
        {
            $p=$this->coreScriptPosition;

            // Keys in scriptFiles must be equal to value to make unifyScripts work:
            $packageFiles=array_combine($package['js']['urls'],$package['js']['urls']);

            $this->scriptFiles[$p]=isset($this->scriptFiles[$p]) ?
                array_merge($packageFiles,$this->scriptFiles[$p]) : $packageFiles;

            YII_DEBUG && Yii::trace(
                sprintf(
                    "Render compressed js package '%s'\nContains:\n%s\nURLs:\n%s\nCoreScripts: %s",
                    $name,
                    implode(",\n",$package['js']['files']),
                    implode(",\n",$package['js']['urls']),
                    implode(", ",$package['js']['coreScripts'])
                ),
                'app.components.clientscriptpackagecompressor'
            );
        }

        if(isset($package['css']))
        {
            $cssFiles=$this->cssFiles;
            $urls=$this->cssFiles=array();
            foreach($package['css']['urls'] as $url)
                $this->cssFiles[$url]='';
            foreach($cssFiles as $url=>$media)
                $this->cssFiles[$url]=$media;

            YII_DEBUG && Yii::trace(
                sprintf(
                    "Render compressed css package '%s'\nContains:\n%s\nURLs:\n%s",
                    $name,
                    implode(",\n",$package['css']['files']),
                    implode(",\n",$package['css']['urls'])
                ),
                'app.components.clientscriptpackagecompressor'
            );
        }
    }

    /**
     * Create a compressed version of all package files, publish the compressed file
     * through asset manager and store compressedInfo. Js and CSS will be processed
     * independently. So this can
     *
     * @param string $name of package
     */
    public function compressPackage($name)
    {
        // Backup registered scripts, css and core scripts, as we only want to
        // catch the files contained in the package, not those registered from elsewhere
        $coreScripts=$this->coreScripts;
        $scriptFiles=$this->scriptFiles;
        $cssFiles=$this->cssFiles;
        $this->coreScripts=$this->cssFiles=$this->scriptFiles=array();

        // Now we register only the package and let yii resolve dependencies
        // and expand coreScripts into scriptFiles (usually happens during rendering)
        $this->registerCoreScript($name);
        $this->renderCoreScripts();

        // Copied from CClientScript: process the scriptMap and remove duplicates
        if(!empty($this->scriptMap))
            $this->remapScripts();
        $this->unifyScripts();

        $info=array();
        $am=Yii::app()->assetManager;
        $config = $this->packages[$name];

        // Process all JS files from the package (if any)
        if (isset($this->scriptFiles[$this->coreScriptPosition]))
        {
            $scripts=array();
            $urls=array();
            $basePath=Yii::getPathOfAlias('webroot');
            foreach($this->scriptFiles[$this->coreScriptPosition] as $script)
                if (strtolower(substr($script,0,4))==='http') // Exclude external scripts
                    $urls[]=$script;
                else
                    $scripts[]=$basePath.$script;

            if ($scripts!==array())
            {
                // Remove current package name from meta data
                $core=array_keys($this->coreScripts);
                if(($key=array_search($name,$core))!==false)
                    unset($core[$key]);

                $fileName=$this->compressFiles($name,'js',$scripts);

                $info['js']=array(
                    'file'          => $am->getPublishedPath($fileName,true),  // Path to file on disk
                    'files'         => $scripts,
                    'urls'          => $urls,
                    'coreScripts'   => $core,
                );

                // Test for S3
                if($this->s3!==null && isset($config['s3']))
                {
                    $s3 = Yii::app()->getComponent($config['s3']===true ? $this->s3 : $config['s3']);
                    $bucket = isset($config['s3Bucket']) ? $config['s3Bucket'] : $this->s3Bucket;
                    if($bucket===null)
                        $bucket = $s3->bucket;
                    $uri = '/jspackages/'.basename($fileName);
                    $s3->upload($fileName, $uri, $bucket, S3::ACL_PUBLIC_READ, array(), array(
                        'Expires'       => gmdate('D, d M Y H:i:s', strtotime('+30 day')).' GMT',
                        'Cache-Control' => 'max-age=2592000',
                        'Content-Type'  => 'application/x-javascript; charset=utf8',
                    ));
                    $info['js']['urls'][] = "https://$bucket$uri"; // Always use https URL
                    $info['js']['s3'] = array($uri, $bucket);
                }
                else
                {
                    $url = $am->publish($fileName,true);         // URL to compressed file

                    // Test for CDN
                    if($this->cdn!==null && isset($config['cdn']))
                        $url =  ($config['cdn']===true ? $this->cdn : $config['cdn']) . $url;

                    $info['js']['urls'][] = $url;
                }

                unlink($fileName);
            }
        }

        // Process all CSS files from the package (if any)
        if ($this->cssFiles!==array())
        {
            $files=array();
            $urls=array();
            $basePath=Yii::getPathOfAlias('webroot');
            foreach(array_keys($this->cssFiles) as $file)
                $files[]=$basePath.$file;

            $fileName=$this->compressFiles($name,'css',$files);
            $url = $am->publish($fileName,true);         // URL to compressed file

            // Test for CDN
            if($this->cdn!==null && isset($config['cdn']))
                $url =  ($config['cdn']===true ? $this->cdn : $config['cdn']) . $url;

            $urls[] = $url;

            $info['css']=array(
                'file'          =>$am->getPublishedPath($fileName,true), // path to compressed file
                'files'         =>$files,
                'urls'          =>$urls,
            );
            unlink($fileName);
        }

        // Store package meta info
        if($info!==array())
        {
            $this->setCompressedInfo($name,$info);
        }

        // Restore original coreScripts, scriptFiles and cssFiles
        $this->coreScripts=$coreScripts;
        $this->scriptFiles=$scriptFiles;
        $this->cssFiles=$cssFiles;
    }

    /**
     * Create a compressed file using YUI compressor (requires JRE!)
     *
     * @param string $name of package
     * @param string $type of package, either js or css
     * @param array $files list of full file paths to files
     * @return string file name of compressed file
     */
    private function compressFiles($name,$type,$files)
    {
        YII_DEBUG && Yii::trace(sprintf(
            "Compressing %s package %s:\n%s",
            $type,
            $name,
            implode(",\n",$files)
        ),'app.components.clientscriptpackagecompressor');

        $inFile=$this->combineFiles($name,$files);
        $outFile=sprintf(
            '%s/%s_pkg_%s_%s.%s',
            Yii::getPathOfAlias('application.var.tmp'),
            $type,
            $name,
            md5_file($inFile),
            $type
        );
        $jar=Yii::getPathOfAlias($this->compressorPath).DIRECTORY_SEPARATOR.$this->compressorJar;
        // See http://developer.yahoo.com/yui/compressor/
        $command=sprintf("java -jar '%s' --type %s -o '%s' '%s'",$jar,$type,$outFile,$inFile);

        if($this->combineOnly)
            copy($inFile,$outFile);
        else
        {
            exec($command,$output,$result);

            if ($result!==0)
                throw new CException(sprintf(
                    "Could not create compressed $type file. Maybe missing a JRE?\nCommand was:\n%s",
                    $command
                ));
        }

        unlink($inFile);
        return $outFile;
    }

    /**
     * Combine the set of given text files into one file
     *
     * @param string $name of package
     * @param array $files list of files to combine (full path)
     * @return string full path name of combined file
     */
    private function combineFiles($name,$files)
    {
        $fileName=tempnam(Yii::getPathOfAlias('application.var.tmp'),'combined_'.$name);
        foreach($files as $f)
            if(!file_put_contents($fileName, file_get_contents($f)."\n", FILE_APPEND))
                throw new CException(sprintf(
                    'Could not combine combine file "%s" into "%s"',
                    $f,
                    $fileName
                ));

        return $fileName;
    }

    /**
     * @return string unique key per application
     */
    public function getStateKey()
    {
        return '__packageCompressor:'.Yii::app()->getId();
    }
}
