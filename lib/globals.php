<?php
/**
 * This file contains global helpers.
 *
 * Some methods from the legacy system/functions.php file can be found here.
 */

/**
 * This is a shortcut to CHtml::link() with the addition that it ensures HTTP/HTTPS links.
 * Default is to use same schema as current request. See url() for more details. See sl()
 * for a HTTPS wrapper.
 *
 * NOTE: Absolute URLs remain unchanged!
 *
 * @param  mixed $text content of link anchor
 * @param  mixed $url to link to, either array() with createUrl() params or relative URL as string
 * @param  array $options for the link tag (class, ...)
 * @param  mixed $ssl Use SSL(true), no SSL (false) or same as current request(null, default)
 * @param  bool  $forceAbsolute  Wether to enforce creation of absolute URL
 * @return string the generated link tag
 */
function l($text,$url='#',$options=array(),$ssl=null,$forceAbsolute=false)
{
    return CHtml::link($text,url($url,$ssl,$forceAbsolute),$options);
}

/**
 * Load content from given URL through cUrl
 *
 * @param string $url to the external page
 * @param string $file full path to output file
 * @return mixed the response body or the final URL (when $file is set) for success, false otherwhise
 */
function loadurl($url,$file=null)
{
    $ch=curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_HEADER,0);
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);  // follow redirects
    curl_setopt($ch,CURLOPT_MAXREDIRS, 3);      // but not more than 3

    if($file===null)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    elseif(($fp=fopen($file,'wb'))!==false)
        curl_setopt($ch,CURLOPT_FILE,$fp);
    else
        throw new CException("Could not write to file '$file'");

    $res=curl_exec($ch);


    // Get the HTTP status code of the request http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
    $http_status = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // Allow only HTTP status 200
    if ($http_status !== 200) {
        Yii::log("loadurl exited with http status code {$http_status} while loading url '{$url}'", 'warning', 'app.component.loadurl');
        return false;
    }

    if($file!==null) {
        fclose($fp);
        $res=curl_getinfo($ch,CURLINFO_EFFECTIVE_URL);
    }
    curl_close($ch);
    return $res;
}

/**
 * This is the shortcut to Yii::app()->params[$name]
 *
 * @param string $name of parameter
 * @return mixed parameter value
 */
function param($name, $default=null)
{
    static $params;
    if ($params===null)
        $params=Yii::app()->params;
    return $params->contains($name) ? $params->itemAt($name) : $default;
}

/**
 * Create a URL
 *
 * This method can create almost any kind of URL.
 *
 *    * $url should usually be an array with a Yii route as first element and extra
 *      parameters as additional key/value pairs:
 *
 *          url( array('some/route', 'id'=>5) );
 *
 *      It can also be a string with a relative URL. This is mainly useful for static
 *      content like CSS files or images (or legacy URLs, but this will soon be deprecated):
 *
 *          url( '/images/someimage.png', true, true, 'web');
 *
 *      In the special case, that an external URL with the same schema should be created
 *      $url can also include the hostname. $hostAlias must be set to `true` in that case:
 *
 *          url( 'www.external.com/somepage.html', null, true, true);
 *
 *      NOTE: If $url is already a full URL that starts with 'http(s)://', then there's
 *      no processing at all.
 *
 *
 *    * $ssl is null by default, which means, that the same schema (http/https) as for the
 *      current request is used for the URL. If it's true/false then either a HTTPS or a
 *      HTTP URL is created. If the schema stays the same as the current request, then
 *      a relative URL is created whenever possible.
 *
 *
 *    * $forceAbsolute will enforce the creation of an absolute URL.
 *
 *
 *    * $hostAlias defines the host name to be used for absolute URLs. By default this
 *      will be "local" which stands for the hostname of the current request. The aliases
 *      must be configured as parameter in url.hostnames. The special value `true` must
 *      be used for external URLs, in which case the hostname is not touched at all.
 *
 * @param mixed $url        Either an array with createUrl() arguments, or a relative URL string
 * @param mixed $ssl        Use SSL(true), no SSL (false) or same as current request(null, default)
 * @param bool  $forceAbsolute  Wether to enforce creation of absolute URL
 * @param mixed $hostAlias  Which hostname to use for absolute URLs. Default is "local".
 *
 * @return string the created URL
 */
function url($url, $ssl=null, $forceAbsolute=false, $hostAlias='notused')
{
    if($hostAlias!='notused')
        ylog('Wrong use of $hostAlias: '.$hostAlias);

    // Poor man's "Cache":
    static $isSsl;
    static $request;
    static $http;
    static $https;
    static $component;  // component used to create URLs
    static $aliases;

    if ($isSsl===null) {    // Set "cache" variables
        $request    = Yii::app()->request;
        $isSsl      = $request->isSecureConnection;
        $http       = $request->getHostInfo('http');
        $https      = $request->getHostInfo('https');
        // #4047 - We could be in legacy where the controller isn't defined:
        $component  = Yii::app()->controller===null ? Yii::app() : Yii::app()->controller;
        $aliases    = param('url.aliases');
    }

    if ($ssl===null)
        $ssl=$isSsl;

    // Check for absolute URLs starting with 'http': They are not processed
    if (is_string($url) && strpos($url,'http')===0)
        return $url;

    // External URLs only need the right schema
    if($hostAlias===true)
        return ($ssl ? 'https://' : 'http://').$url;

    // Temporarily change hostInfo if another hostname should be used
    if($hostAlias!=='local' && isset($aliases[$hostAlias]))
    {
        $oldHostInfo    = $request->getHostInfo();
        $oldHttp        = $http;
        $oldHttps       = $https;
        $request->setHostInfo( ($isSsl ? 'https://' : 'http://').$aliases[$hostAlias]);
        $http   = $request->getHostInfo('http');
        $https  = $request->getHostInfo('https');
    }

    // Find out if we need to create an absolute URL
    if ($forceAbsolute || $hostAlias!=='local' || !$ssl && $isSsl || $ssl && !$isSsl) {
        if (is_array($url))
            $value = $component->createAbsoluteUrl($url[0],array_splice($url,1),$ssl ? 'https':'http');
        else
            $value = ($ssl ? $https : $http).$url;
    } else {
        if (is_array($url))
            $value = $component->createUrl($url[0],array_splice($url,1));
        else
            $value = $url;
    }

    return $value;
}

/**
 * Super easy wrapper for CVarDumper::dump
 * @param mixed $var
 * @param bool $die
 * @param bool $formatted
 * @return html
 */
function ydump($var, $title = false, $die = false, $formatted = true){
    if($title)
        echo "<br/>$title<br/>";;
    $formatted?CVarDumper::dump($var, 10, true):CVarDumper::dump($var);
    if($die)
        Yii::app()->end();
}

/**
 * Wrapper for Yii::app()->end()
 * @param mixed $var
 * @param bool $formatted
 * @return html
 */
function yend(){
    Yii::app()->end();
}

/**
 * Wrapper for Yii::log()
 *
 * @param type $msg
 * @param type $level
 * @param type $category
 */
function ylog($msg, $level=  CLogger::LEVEL_ERROR, $category='application'){
    Yii::log($msg, $level, $category);
}