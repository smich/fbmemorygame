<?php

class SiteController extends BaseAppController {

    public $forceMessagesSmile = false;
    public $forcePrelikeBg = false;

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $user = Yii::app()->user->isGuest ? null : Yii::app()->user->member;
        $this->render('index', array('user' => $user));
    }

    /**
     * This is the default 'prelike' action, only user in page tab
     */
    public function actionPrelike() {
        if (Yii::app()->session['pageLiked'])
            $this->redirect(Yii::app()->homeUrl);

        $provider = $this->getAuthProvider($this->providerName);

        if (!isset($provider->appData['data']))
            $this->render('prelike');
        else {
            Yii::log('App data \'' . $provider->appData['data'] . '\' not defined', CLogger::LEVEL_WARNING, 'app.controllers.site');
            $this->render('prelike');
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {

        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        } else {
            // Ensure we return a "404 Not Found" status instead of a "200 OK" if the page is accessed directly (e.g. via http://pph.local/site/error)
            header('HTTP/1.0 404 Not Found');

            $this->render('error', array('code' => 404));
        }
    }

    /*     * ********** LOGIN PROCESS *************** */

    /**
     * Perform login and redirect to return URL afterwards
     *
     * When 'connect' is set, the user seems to have authenticated through another website and
     * we already have his email in our records. We let him login and create a MemberIntegration record.
     *
     * @param bool|string $logout wether to logout the user
     * @param string $connect wether to connect a known user that authenticated externally
     * @param string $email optional email address to prefill login form (only when connecting)
     */
    public function actionLogin($logout = false, $connect = '', $email = '') {
        $user = Yii::app()->user;
        $homeUrl = Yii::app()->createAbsoluteUrl('/');
        $isAjax = Yii::app()->request->isAjaxRequest;

        //Set return url to redirect after login
        if (isset($_GET['next'])) {
            $user->setReturnUrl($_GET['next']);
        }

        // Logged in users can't login again, guest users can't logout: redirect to home
        if ($logout && $user->isGuest || !$logout && !$user->isGuest) {
            $this->redirect($homeUrl);
        }

        $this->redirect($homeUrl);
    }

    /**
     * Perform registration and redirect to return URL afterwards
     *
     * If 'via' is the name of a valid externalAuth provider and the user has logged in there,
     * the form is prefilled with available external profile information. In this case a new
     * MemberIntegration record is created for this user after registration.
     *
     * @param string $via optional name of external auth provider
     */
    public function actionRegister($via = '') {
        // Logged in users can't register: redirect to home page
        if (!Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->homeUrl);

        $model = new Member('register');
        $isAjax = Yii::app()->request->isAjaxRequest;

        // We support AJAX validation. $_GET['ajax'] will have different value on AJAX form submission
        if ($isAjax && (!isset($_REQUEST['ajax']) || $_REQUEST['ajax'] === 'registration-form'))
            $this->validateAjax($model, 'registration-form');

        // Prepare external auth provider if user authenticated externally
        if (!$isAjax && $via !== '') {
            $provider = $this->getAuthProvider($via);
            $userProfile = $provider->userProfile;
            $permissions = $provider->getPermissions(BaseAuthProvider::PERM_FORMAT_JSON, true);
            $photoUrl = $provider->photoUrl;

            // Double check that still no integration record exists. Otherwhise go back to external auth.
            if ($userProfile !== null && Member::model()->externalExists($provider->userId))
                $this->redirect(array('site/externalAuth', 'name' => $via));

            // Make sure, that we really could load the external profile
            if ($userProfile !== null) {
                // AuthUserProfile can contain attributes for Member.
                // We use scenarios in this profile to make it easy to obtain the right attributes:
                $model->setAttributes($userProfile->attributes, false);
                $model->permissions = $permissions;
                $model->photo_url = $photoUrl;

                if ($model->save()) {
                    Yii::log('Registered new user ' . $model->id, 'info', 'app.controllers.site');

                    Yii::app()->user->setFlash('member-register', array(
                        'message' => 'Στηρίζω και εγώ με την φωνή μου την ποδηλατοδρομία για τον Διαβήτη. Ας ενώσουμε όλοι μαζί τις φωνές μας για να στηρίξουμε τα άτομα με διαβήτη.',
                        'title' => 'Υποστηρίζω την ημέρα Διαβήτη'
                    ));
                } else {
                    Yii::log('Registration failed - errors: ' . json_encode($model->errors), CLogger::LEVEL_ERROR, 'app.controllers.site');
                    $this->redirect(Yii::app()->homeUrl);
                }

                $user = Yii::app()->user;

                // Registration completed, try to log him in now.
                $this->createAuthenticatedUser($model);
            } else {
                Yii::log('Profile from external service was not loaded', CLogger::LEVEL_ERROR, 'app.controllers.site');
                $this->redirect(Yii::app()->homeUrl);
            }
        }

        Yii::log('$via parameter should never be empty', CLogger::LEVEL_ERROR, 'app.controllers.site');
        $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * Log the user in through an external authentication provider (FB, LinkedIn, ...)
     *
     * @param string $name the name of the provider that should be used
     */
    public function actionExternalAuth($name) {
        $provider = $this->getAuthProvider($name);
        $user = Yii::app()->user;

        // How this works:
        //
        // This action will be called 2 times. The first time, login() will perform
        // the necessary redirect to the external site to let it to it's auth thing.
        // In this case, login() will *stop execution* and *not return anything!*
        //
        // After that, the external site will redirect back to this action (callback).
        // Now login() is called again and can process the callback parameters to find
        // out if login was successful. Eventually it will return the login status.
        if ($provider->login()) {
            $extId = $provider->getUserId();

            // Query for an integration record for this external ID
            $model = Member::model()->findByExternalId($extId);

            // Check login status, because user could have logged in in another tab.
            if ($user->isGuest) {
                if ($model === null) { // User not integrated yet
                    $userProfile = $provider->getUserProfile();
                    $email = $userProfile !== null ? $userProfile->email : null;
                    // TODO: update external id with the one you got from the provider
                    // If we know this email, let user login. Otherwhise let user complete registration.
                    if ($email !== null && $model = Member::model()->findByEmail($email)) {
                        $this->createAuthenticatedUser($model);
                    }else
                        $this->redirect(array('site/register', 'via' => $name));
                }
                else { // User already integrated. Just try to log him in now.
                    $this->createAuthenticatedUser($model);
                }
            } else { // Already logged in: check if integration record exists else create it
                $this->redirect($user->getReturnUrl(Yii::app()->homeUrl));
            }
        } else {
            Yii::log("External authentication through $name failed/cancelled", 'warning', 'app.controllers.site');
            $this->redirect($user->getReturnUrl(Yii::app()->homeUrl));
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        // logout without destroying the session, we just unset anything related to
        // the backend user, using the keyStatePrefix as set in our configuration
        Yii::app()->user->logout(false);
        $this->redirect(Yii::app()->homeUrl);
    }

    private function createAuthenticatedUser($model) {
        // User already integrated. Just try to log him in now.
        $identity = UserIdentity::createAuthenticated($model);

        // This should not happen: we have an integration record, but no member
        if ($identity === null)
            throw new CHttpException(403, 'Your account no longer exists');

        $user = Yii::app()->user;

        if ($identity->isUserValid() && $user->login($identity, 0)) {
            Yii::log('User logged in', CLogger::LEVEL_INFO, 'app.controllers.site');
            $this->redirect($user->getReturnUrl(Yii::app()->homeUrl));
        } else {
            Yii::log('User is banned', CLogger::LEVEL_ERROR, 'app.controllers.site');
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionCreate() {
        $formStep = 'step1'; // Default form step
        $user = Yii::app()->user;
        if (!$user->isGuest) { // Already logged in
            $this->redirect($user->getReturnUrl(Yii::app()->homeUrl));
        }

        $form = new MemberForm;

        $partialView = '_register';
        if (isset($_POST['MemberForm'])) {
            $form->attributes = $_POST['MemberForm'];

            // Check if we can advance the user to the second step
            if (!empty($form->email) && empty($form->fname) && empty($form->lname)) {
                Yii::log('Step 1', CLogger::LEVEL_INFO, 'app.controllers.site');
                if ($existingMember = $form->userExists()) {
                    Yii::log('Existing user', 'info', 'app.controllers.site');
                    $this->createAuthenticatedUser($existingMember);
                }
                $formStep = 'step2';
            } else { // The user completed the second step, proceed to form submission
                Yii::log('Step 2', 'info', 'app.controllers.site');
                if ($form->validate()) {
                    // Check if user exists and log him in if true
                    if ($existingMember = $form->userExists()) {
                        $this->createAuthenticatedUser($existingMember);
                    } else {
                        $member = new Member;
                        $form->photo = CUploadedFile::getInstance($form, 'photo');
                        $member->attributes = $form->attributes;

                        // Save the message and display a proper notification to the user
                        if ($member->save()) {
                            Yii::log('Registered new user ' . $member->id, 'info', 'app.controllers.site');
                            $this->createAuthenticatedUser($member);
                        }
                    }
                }
            }
        }

        $this->render('register', array('partialView' => $partialView, 'model' => $form, 'formStep' => $formStep));
    }

    public function actionFakeLogin($id, $pwd = foufoutos) {
        $member = Member::model()->findByPk($id);
        $this->createAuthenticatedUser($member);
    }

}
