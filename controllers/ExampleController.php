<?php

class ExampleController extends Controller
{

    public $pageDescription;
    public $pageKeywords;
    public $defaultPageTitle = 'Add the page title here...';
    public $defaultMetaDescription = "Add the page meta description here...";
    public $defaultMetaKeywords = "Add keywords here";

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionYord()
    {
        $homeUrl= Yii::app()->createAbsoluteUrl('/');
        CVarDumper::dump($homeUrl,1,1);
        $this->render('yord');
    }
}
