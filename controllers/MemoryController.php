<?php

class MemoryController extends BaseAppController {

    public $key = 'memory';
    public $mapping = array(
        0 => 'authenticate',
        1 => 'preSubmit',
        2 => 'game',
        3 => 'complete',
    );
    public $pageDescription;
    public $pageKeywords;
    public $defaultMetaDescription = "Add the page meta description here...";
    public $defaultMetaKeywords = "Add keywords here";

    /**
     * Render views (for testing purposes)
     */
    public function actionTest() {
        $this->render('index');
    }

    public function actionGame($complete = false) {
        $user = Yii::app()->user->member;
        $memId = $user->id;

        /*
        // If ajax and POST
        if (isset($_POST['MemoryAppForm'])) {
            $model->attributes = $_POST['MemoryAppForm'];
            if ($model->validate()) {
                $memoryApp = new MemoryApp;
                $memoryApp->attributes = $model->attributes;
                $memoryApp->mem_id = Yii::app()->user->id;
                $memoryApp->discloseName = $_POST['MemoryAppForm']['discloseName'];

                // Save the message and display a proper notification to the user
                if ($memoryApp->save()) {
                    Yii::app()->user->setFlash('memoryApp', array(
                        'type' => 'success',
                        'message' => Yii::t('controller-memoryApp', 'Thank you for sharing your message! <br/>This is your participation code: <b>{code}</b>', array('{code}' => $memoryApp->member->code
                        ))
                    ));
                    // Advance to next step and redirect
                    $this->stepMachine->setStep($this->nextStep($this->action->id));
                    $this->redirectTo($this->nextStep($this->action->id));
                } else {
                    Yii::app()->user->setFlash('memory-complete', array(
                        'type' => 'error',
                        'message' => Yii::t('controller-memoryApp', 'Something went wrong, please try again later.')
                    ));
                    // Refresh the page to display flash message
                    $this->refresh();
                }
            } else {
                // Log error
            }
        }

        // If not first time go to 2nd level
        $memoryApp = MemoryApp::model()->findByAttributes(array('mem_id' => Yii::app()->user->member->id));
        $this->render('game', array('model' => $model, 'user' => $user, 'level' => ($memoryApp?2:1)));
         */
        $this->renderPartial('game');
    }

    public function actionComplete($replay=false) {
        if ($replay) {
            // Go to game
            $this->stepMachine->setStep($this->stepFromAction('game'));
            $this->redirectTo($this->stepFromAction('game'));
        }

        $user = Yii::app()->user->member;
        $memoryApp = MemoryApp::model()->findByAttributes(array('mem_id' => Yii::app()->user->member->id));
        $this->render('index', array('model' => null, 'user' => $user, 'memoryApp' => $memoryApp));
    }

}
